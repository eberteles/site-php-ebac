<?php

class Acesso extends AppModel {
    public $name = 'Acesso';
    public $validate = array();
    
    public $belongsTo = array(
        'Rotina' => array(
            'className' => 'Rotina',
            'foreignKey' => 'rotina_id'
        ),
        'Unidade' => array(
            'className' => 'Unidade',
            'foreignKey' => 'unidade_id',
            'fields' => array('unidade')
        )
    );
}