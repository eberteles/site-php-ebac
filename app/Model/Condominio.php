<?php

class Condominio extends AppModel {
    public $name = 'Condominio';
    public $validate = array(
    );
    
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->virtualFields['exibe'] = 'CONCAT( lpad(' . $this->alias . '.id, 3, "0") , " - ", ' . $this->alias . '.nome)';
    }
    
    public $belongsTo = array(
        'Sindico' => array(
            'className' => 'Sindico',
            'foreignKey' => 'sindico_id'
        )
    );
}