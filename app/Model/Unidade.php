<?php

class Unidade extends AppModel {
    public $name = 'Unidade';
    public $validate = array(
    );
    
    public $belongsTo = array(
        'Condominio' => array(
            'className' => 'Condominio',
            'foreignKey' => 'condominio_id'
        ),
        'Ocupante' => array(
            'className' => 'Cliente',
            'foreignKey' => 'ocupante_id'
        ),
        'Proprietario' => array(
            'className' => 'Cliente',
            'foreignKey' => 'proprietario_id'
        ),
        'Imobiliaria' => array(
            'className' => 'Imobiliaria',
            'foreignKey' => 'imobiliaria_id'
        )
    );
    
}