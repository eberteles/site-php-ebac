<?php

class Chave extends AppModel {
    public $name = 'Chave';
    public $validate = array(
        'id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o código de validação.'
            )
        )
    );

}