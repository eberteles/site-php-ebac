<?php

class Cliente extends AppModel {
    public $name = 'Cliente';
    var $actsAs  = array('CakePtbr.AjusteData');
    public $validate = array(
    );
    
    public $hasMany = array(
        'SindicoCondominio' => array(
            'className' => 'Condominio',
            'foreignKey' => 'sindico_id'
        )
    );
}