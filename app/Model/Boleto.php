<?php

class Boleto extends AppModel {
    public $name = 'Boleto';
    public $validate = array(
    );
    
    public $hasMany = array(
        'Lancamento' => array(
            'className' => 'Lancamento',
            'foreignKey' => 'boleto_id'
        )
    );
    
}