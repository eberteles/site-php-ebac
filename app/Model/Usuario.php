<?php

App::uses('AuthComponent', 'Controller/Component');

class Usuario extends AppModel {
    public $name = 'Usuario';
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar um usuário de acesso.'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A senha é obrigatória.'
            )
        )
    );
    
    public $belongsTo = array(
        'Cliente' => array(
            'className' => 'Cliente',
            'foreignKey' => false,
            'conditions' => array("`Cliente`.`documento` = `Usuario`.`username`")
        )
    );

    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password(strtolower($this->data[$this->alias]['password']));
        }
        return parent::beforeSave($options);
    }
}