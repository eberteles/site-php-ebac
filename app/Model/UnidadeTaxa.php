<?php

class UnidadeTaxa extends AppModel {
    public $name = 'UnidadeTaxa';
    public $validate = array(
    );
    
    public $belongsTo = array(
        'Taxa' => array(
            'className' => 'Taxa',
            'foreignKey' => 'taxa_id'
        ),
        'Unidade' => array(
            'className' => 'Unidade',
            'foreignKey' => 'unidade_id'
        )
    );
    
}