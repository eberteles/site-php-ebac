<?php

class Votacao extends AppModel {
    public $name = 'Votacao';
    public $displayField = 'assunto';
    public $actsAs = array('CakePtbr.AjusteData');
    public $validate = array(
        'data' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Data.'
            )
        ),
        'assunto' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o Assunto.'
            )
        ),
        'quantidade' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Quantidade de Propostas.'
            )
        )
    );
    
    public $hasMany = array(
        'Proposta' => array(
            'className' => 'VotacaoProposta',
            'foreignKey' => 'votacao_id'
        )
    );
    
}