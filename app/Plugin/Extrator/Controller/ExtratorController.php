<?php

set_time_limit(7200);

App::uses('File', 'Utility');
App::uses('Folder', 'Utility');

define('DADOS', ROOT . DS . 'dados' . DS);

class ExtratorController extends ExtratorAppController {
    
    public $autoRender = false;
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow('index');
        $this->Auth->allow('zerarTabelas');
        $this->Auth->allow('taxas'); //1s
        $this->Auth->allow('condiminios'); //1s
        $this->Auth->allow('unidades');
        //$this->Auth->allow('taxas_condominio');
        //$this->Auth->allow('fluxos_condominio');
        //$this->Auth->allow('boletos_condominio');
        $this->Auth->allow('saldos');
        $this->Auth->allow('usuarios');
        $this->Auth->allow('acessos');
   }

    public function index() {
        echo date('d/m/Y - H:i:s') . ' Começando...<br>';
        echo '-------------------------------------<br><br>';
        
        $this->zerarTabelas();
        
        //$this->imobiliarias();
        $this->taxas();
        $this->condiminios();
        $this->saldos();
        $this->usuarios();
        
        echo '-------------------------------------<br><br>';
        echo date('d/m/Y - H:i:s') . ' Fim da execução...<br>';
    }
    
    public function usuarios() {
        echo date('H:i:s') . ' - Importando Usuários...<br>';
    
        $file = new File(DADOS . 'DadosEbacBackup.txt');

        $contents = $file->read();
        $convert = explode("\n", utf8_encode($contents));
        //$convert = explode("\n", $contents);
      
        $configuracao   = array(
            'Usuario.nome'          => array(17, 12),
            'Usuario.password'      => array(29, 10),
            'Usuario.username'      => array(39, 11),
            'Usuario.role'          => array(50, 15),
            'Usuario.externo'       => array(65, 1),
            'Usuario.condominio_id' => array(66, 3, 'CONDOMINIO')
        );
        $usuarios   = array();
        
        $padrao1 = "$!&)=]:<_%*+.;>\#^(-[}'?/@|{myhqsbiwgvxoldpnfrutc7036284,";
        $padrao2 = str_split("ABCDEFGHIJKLMNOPQRSTUVXWYZ0123456789#$%&*()+-=[],.:;<>?/ ");
        
        $this->loadModel('Usuario');
        foreach ($convert as $linha) {            
            if(strlen($linha) > 69) {
            
                $newLinha   = '';

                foreach (str_split($linha) as $str_linha) {
                    $posicao    = strpos($padrao1, $str_linha);
                    if ($posicao === false) {
                        $newLinha .=$str_linha;
                    } else {
                        $newLinha .=$padrao2[$posicao];
                    }
                }
                
                $novoUsuario = $this->lineToObject($newLinha, $configuracao);
                $usuarioDb   = $this->Usuario->find('first', array(
                        'fields' => array('Usuario.id'),
                        'conditions' => array(
                            'Usuario.username'=>$novoUsuario['Usuario']['username'],
                            'Usuario.role'=>$novoUsuario['Usuario']['role']
                            )
                    ));
                if(isset($usuarioDb['Usuario']['id']) && $usuarioDb['Usuario']['id'] > 0) {
                    $novoUsuario['Usuario']['id']   = $usuarioDb['Usuario']['id'];
                }
                $usuarios[] = $novoUsuario;
            }            
        }        
        
        $this->Usuario->saveAll($usuarios);
        
        debug($usuarios);
        echo count($usuarios) . ' Usuários.<br>';
        echo date('H:i:s') . ' - Importação de Usuários concluída.<br>';
        echo '-------------------------------------<br><br>';

        $file->close();
        
    }
    
//    private function imobiliarias() {
//        echo date('H:i:s') . ' - Importando Imobiliárias...<br>';
//    
//        $file = new File(DADOS . 'DadosImobilia.txt');
//
//        $contents = $file->read();
//        $convert = explode("\n", utf8_encode($contents));
//        //$convert = explode("\n", $contents);
//      
//        $configuracao   = array(
//            'Imobiliaria.id'        => array(0, 3),
//            'Imobiliaria.nome'      => array(3, 35),
//            'Imobiliaria.telefone'  => array(38, 11),
//            'Imobiliaria.endereco'  => array(85, 35),
//            'Imobiliaria.bairro'    => array(49, 15),
//            'Imobiliaria.cidade'    => array(60, 15),
//            'Imobiliaria.uf'        => array(75, 2),
//            'Imobiliaria.cep'       => array(77, 8),
//            'Imobiliaria.documento' => array(128, 14),
//            'Imobiliaria.email'     => array(158, 50)
//        );
//        $imobiliarias   = array();
//        foreach ($convert as $imobiliaria) {
//            if(strlen($imobiliaria) > 100) {
//                $imobiliarias[] = $this->lineToObject($imobiliaria, $configuracao);
//            }
//        }
//        
//        $this->loadModel('Imobiliaria');
//        $this->Imobiliaria->saveAll($imobiliarias);
//        
//        echo count($imobiliarias) . ' Imobiliárias.<br>';
//        echo date('H:i:s') . ' - Importação de Imobiliárias concluída.<br>';
//        echo '-------------------------------------<br><br>';
//
//        $file->close();
//        
//    }
    
    public function taxas() {
        echo date('H:i:s') . ' - Importando Taxas...<br>';
    
        $file = new File(DADOS . 'DadosNomTaxa.txt');

        $contents = $file->read();
        $convert = explode("\n", utf8_encode($contents));
        //$convert = explode("\n", $contents);
      
        $configuracao   = array(
            'Taxa.id'   => array(0, 3),
            'Taxa.nome' => array(3, 15)
        );
        $taxas  = array();
        foreach ($convert as $taxa) {
            if(strlen($taxa) > 2) {
                $taxas[] = $this->lineToObject($taxa, $configuracao);
            }
        }
        
        $this->loadModel('Taxa');
        $this->Taxa->saveAll($taxas);
        
        echo count($taxas) . ' Taxas.<br>';
        echo date('H:i:s') . ' - Importação de Taxas concluída.<br>';
        echo '-------------------------------------<br><br>';

        $file->close();
        
    }
    
    public function condiminios() {
        echo date('H:i:s') . ' - Importando Condomínios...<br>';
    
        $file = new File(DADOS . 'DadosDadGera.txt');

        $contents = $file->read();
        $convert = explode("\n", utf8_encode($contents));
        //$convert = explode("\n", $contents);
      
        $configuracao   = array(
            'Condominio.id'             => array(0, 3),
            'Condominio.nome'           => array(3, 35),
            'Condominio.endereco'       => array(38, 35),
            'Condominio.bairro'         => array(73, 15),
            'Condominio.cidade'         => array(88, 15),
            'Condominio.uf'             => array(103, 2),
            'Condominio.cep'            => array(105, 8),
            'Condominio.telefone'       => array(113, 9),
            'Condominio.cnpj'           => array(122, 14),
            'Sindico.nome'              => array(136, 31),
            'Sindico.unidade'           => array(167, 5),
            'Condominio.previsao_guia'  => array(172, 10, 'DATE'),
            'Condominio.tp_doc'         => array(182, 4),
            'Sindico.documento'         => array(186, 14),
            'Sindico.sexo'              => array(200, 1),
            'Condominio.nada_consta'    => array(201, 1),
            'Condominio.email'          => array(202, 50),
            'Condominio.site '          => array(270, 40),
            'Condominio.reducao'        => array(252, 9, 'DECIMAL_PT')
        );
        $condominios = array();
        foreach ($convert as $condominio) {
            if(strlen($condominio) > 100) {
                $condominios[] = $this->lineToObject($condominio, $configuracao);
            }
        }
        
        $this->loadModel('Condominio');
        $this->Condominio->saveAll($condominios, array('deep' => true));
        //debug($condominios);
        
        echo count($condominios) . ' Condomínios.<br>';
        echo date('H:i:s') . ' - Importação de Condomínios concluída.<br>';
        echo '-------------------------------------<br><br>';

        $file->close();
        
    }
    
    public function saldos() {
        echo date('H:i:s') . ' - Importando Saldos...<br>';
    
        $file = new File(DADOS . 'DadosSaldos.txt');

        $contents = $file->read();
        $convert = explode("\n", utf8_encode($contents));
        //$convert = explode("\n", $contents);
      
        $configuracao   = array(
            'Saldo.id'              => array(0, 9),
            'Saldo.condominio_id'   => array(0, 3),
            'Saldo.ordinario'       => array(9, 12, 'DECIMAL'),
            'Saldo.extraordinario'  => array(21, 12, 'DECIMAL'),
            'Saldo.reserva'         => array(33, 12, 'DECIMAL')
        );
        $saldos = array();
        foreach ($convert as $saldo) {
            if(strlen($saldo) > 2) {
                $saldos[] = $this->lineToObject($saldo, $configuracao);
            }
        }
        
        $this->loadModel('Saldo');
        $this->Saldo->saveAll($saldos);
        
        echo count($saldos) . ' Saldos.<br>';
        echo date('H:i:s') . ' - Importação de Saldos concluída.<br>';
        echo '-------------------------------------<br><br>';

        $file->close();
        
    }
    
    public function unidades() {
        $this->loadModel('Condominio');
        
        $condominios = $this->Condominio->find('list', array(
                'fields' => array('Condominio.id')
            ));
        foreach ($condominios as $condominio => $value) {
            $this->unidades_condominio( sprintf("%03d", $condominio) );
        }
    }

    private function unidades_condominio($condominio) {
        echo date('H:i:s') . ' - Importando Unidades Condomínio: ' . $condominio . '...<br>';
    
        $file = new File(DADOS . 'Dados' . $condominio . '.txt');

        $contents = $file->read();
        $convert = explode("\n", utf8_encode($contents));
        //$convert = explode("\n", $contents);
        
        $unidades   = array();
        $carga      = null;
        foreach ($convert as $linha) {
            if(strlen($linha) > 30) {
                switch ($linha{0}) {
                    case 0: // CABECALHO COM DATA
                        $carga  = $this->lineToObject($linha, array('Unidade.carga' => array(1, 10, 'DATE')) );
                        break;
                    case 1: // DADOS BÁSICOS DA UNIDADE
                        $unidades[] = $this->lineToObject($linha, $this->getConfiguracaoUnidade($condominio, $carga));
                        break;
                    case 2: // TAXAS DA UNIDADE
                    case 3: // RECEITAS / DESPESAS DO CONDOMÍNIO
                    case 4: // BOLETO
                        break;
                }
            }
        }
        
        $this->loadModel('Unidade');
        echo date('H:i:s') . ' - Iniciando Importação de Unidades...<br>';
        $this->Unidade->saveAll($unidades, array('deep' => true));
        echo count($unidades) . ' Unidades.<br>';
        echo date('H:i:s') . ' - Importação de Unidades concluída.<br>';
        echo '-------------------------------------<br><br>';
        
        $file->close();
    }
    
    public function taxas_upload() {
        $this->loadModel('Condominio');
        
        $condominios = $this->Condominio->find('list', array(
                'fields' => array('Condominio.id')
            ));
        foreach ($condominios as $condominio => $value) {
            $this->taxas_unidades( sprintf("%03d", $condominio) );
        }
    }
    
    private function taxas_unidades($condominio) {
        echo date('H:i:s') . ' - Importando Taxas Unidades Condomínio: ' . $condominio . '...<br>';
    
        $file = new File(DADOS . 'Dados' . $condominio . '.txt');

        $contents = $file->read();
        $convert = explode("\n", utf8_encode($contents));
        //$convert = explode("\n", $contents);
        
        $this->loadModel('UnidadeTaxa');
        $taxas      = array();
        foreach ($convert as $linha) {
            if(strlen($linha) > 30) {
                switch ($linha{0}) {
                    case 0: // CABECALHO COM DATA
                    case 1: // DADOS BÁSICOS DA UNIDADE
                        break;
                    case 2: // TAXAS DA UNIDADE
                        $taxa   = $this->lineToObject($linha, $this->getConfiguracaoTaxaUnidade($condominio));
                        $taxaDb = $this->UnidadeTaxa->find('first', array(
                                'fields' => array('UnidadeTaxa.id'),
                                'conditions' => array(
                                    'UnidadeTaxa.unidade_id'=>$taxa,
                                    'UnidadeTaxa.role'=>null
                                    )
                            ));
                        $taxas[] = $taxa;
                        break;
                    case 3: // RECEITAS / DESPESAS DO CONDOMÍNIO
                    case 4: // BOLETO
                        break;
                }
            }
        }
        
        echo date('H:i:s') . ' - Iniciando Importação de Taxas da Unidade...<br>';
        $this->UnidadeTaxa->saveAll($taxas);
        echo count($taxas) . ' Taxas da Unidade.<br>';
        echo date('H:i:s') . ' - Importação de Taxas da Unidade concluída.<br>';
        echo '-------------------------------------<br><br>';
        
        $file->close();
    }
    
    public function fluxos_upload() {
        $this->loadModel('Condominio');
        
        $condominios = $this->Condominio->find('list', array(
                'fields' => array('Condominio.id')
            ));
        foreach ($condominios as $condominio => $value) {
            $this->fluxos( sprintf("%03d", $condominio) );
        }
    }
    
    private function fluxos($condominio) {
        echo date('H:i:s') . ' - Importando Fluxos Condomínio: ' . $condominio . '...<br>';
    
        $file = new File(DADOS . 'Dados' . $condominio . '.txt');

        $contents = $file->read();
        $convert = explode("\n", utf8_encode($contents));
        //$convert = explode("\n", $contents);
        
        $fluxoCondominio    = array();
        foreach ($convert as $linha) {
            if(strlen($linha) > 30) {
                switch ($linha{0}) {
                    case 0: // CABECALHO COM DATA
                    case 1: // DADOS BÁSICOS DA UNIDADE
                    case 2: // TAXAS DA UNIDADE
                        break;
                    case 3: // RECEITAS / DESPESAS DO CONDOMÍNIO
                        $fluxoCondominio[]  = $this->lineToObject($linha, $this->getConfiguracaoFluxoCondominio($condominio));
                        break;
                    case 4: // BOLETO
                        break;
                }
            }
        }
                
        $this->loadModel('Fluxo');
        echo date('H:i:s') . ' - Iniciando Importação de Entradas/Saídas do Condomínio...<br>';
        $this->Fluxo->saveAll($fluxoCondominio);
        echo count($fluxoCondominio) . ' Entradas/Saídas do Condomínio.<br>';
        echo date('H:i:s') . ' - Importação de Entradas/Saídas do Condomínio concluída.<br>';
        echo '-------------------------------------<br><br>';

        $file->close();
    }
    
    public function boletos_upload() {
        $this->loadModel('Condominio');
        
        $condominios = $this->Condominio->find('list', array(
                'fields' => array('Condominio.id')
            ));
        foreach ($condominios as $condominio => $value) {
            $this->boletos( sprintf("%03d", $condominio) );
        }
    }
    
    private function boletos($condominio) {
        echo date('H:i:s') . ' - Importando Boletos Condomínio: ' . $condominio . '...<br>';
    
        $file = new File(DADOS . 'Dados' . $condominio . '.txt');

        $contents = $file->read();
        $convert = explode("\n", utf8_encode($contents));
        //$convert = explode("\n", $contents);
        
        $boletos    = array();
        $mensagemBoleto = array();
        $indiceBoleto   = -1;
        foreach ($convert as $linha) {
            if(strlen($linha) > 30) {
                switch ($linha{0}) {
                    case 0: // CABECALHO COM DATA
                    case 1: // DADOS BÁSICOS DA UNIDADE
                    case 2: // TAXAS DA UNIDADE
                    case 3: // RECEITAS / DESPESAS DO CONDOMÍNIO
                        break;
                    case 4: // BOLETO
                        switch ($linha{7}) {
                            case 0: // Primeira Mensagem Padrão
                                $mensagemBoleto[0]  = trim(mb_substr($linha, 8, 80));
                                break;
                            case 1: // Segunda Mensagem Padrão
                                $mensagemBoleto[1]  = trim(mb_substr($linha, 8, 80));
                                break;
                            case 2: // Tipo 2 do Boleto
                                $indiceBoleto++;
                                
                                $cfgBoleto2    = array(
                                    'Boleto.unidade_id'      => array($condominio, array(1, 6), 'CONCATENAR'),
                                    'Boleto.mensagem'        => ( isset($mensagemBoleto[0]) ? $mensagemBoleto[0] : '' ),
                                    'Boleto.mensagem_2'      => ( isset($mensagemBoleto[1]) ? $mensagemBoleto[1] : '' ),
                                    'Boleto.vencimento'      => array(8, 10, 'DATE'),
                                    'Boleto.limite_desconto' => array(18, 10, 'DATE'),
                                    'Boleto.referencia'      => array(28, 8),
                                    'Boleto.validade'        => array(36, 10, 'DATE'),
                                    'Boleto.situacao'        => array(46, 1),
                                    'Boleto.cateira'         => array(47, 6),
                                    'Boleto.moeda'           => array(53, 3),
                                    'Boleto.banco'           => array(56, 15),
                                    'Boleto.cedente'         => array(71, 22),
                                    'Boleto.atraso'          => array(93, 3)
                                );
                                
                                $boletos[$indiceBoleto]  = $this->lineToObject($linha, $cfgBoleto2);
                                break;
                            case 3: // Tipo 3 do Boleto
                                
                                $cfgBoleto3    = array(
                                    'Boleto.nosso_numero'   => array(8, 18),
                                    'Boleto.digito_nn'      => array(26, 1),
                                    'Boleto.total'          => array(27, 12, 'DECIMAL_PT'),
                                    'Boleto.honorario'      => array(39, 12, 'DECIMAL_PT'),
                                    'Boleto.desconto'       => array(51, 12, 'DECIMAL_PT'),
                                    'Boleto.vencer'         => array(63, 12, 'DECIMAL_PT'),
                                    'Boleto.vencido'        => array(75, 12, 'DECIMAL_PT'),
                                    'Boleto.emissao'        => array(87, 10, 'DATE')
                                );
                                
                                $boleto3    = $this->lineToObject($linha, $cfgBoleto3);
                                
                                $boletos[$indiceBoleto] = array_merge_recursive($boletos[$indiceBoleto], $boleto3);
                                break;
                            case 4: // Tipo 4 do Boleto Linha Digitável
                                
                                $cfgBoleto4    = array(
                                    'Boleto.linha_digitavel'    => array(8, 54)
                                );
                                
                                $boleto4    = $this->lineToObject($linha, $cfgBoleto4);
                                
                                $boletos[$indiceBoleto] = array_merge_recursive($boletos[$indiceBoleto], $boleto4);
                                break;
                            case 5: // Tipo 5 do Boleto Lançamentos
                                
                                $cfgBoleto5    = array(
                                    'Lancamento.codigo'     => array(8, 4),
                                    'Lancamento.lancamento' => array(12, 15),
                                    'Lancamento.referencia' => array(27, 8),
                                    'Lancamento.moeda'      => array(35, 4),
                                    'Lancamento.vencimento' => array(39, 10, 'DATE'),
                                    'Lancamento.principal'  => array(49, 12, 'DECIMAL_PT'),
                                    'Lancamento.atraso'     => array(61, 4),
                                    'Lancamento.corrigido'  => array(65, 12, 'DECIMAL_PT'),
                                    'Lancamento.acrescimo'  => array(77, 12, 'DECIMAL_PT'),
                                    'Lancamento.total'      => array(89, 13, 'DECIMAL_PT'),
                                );
                                
                                $cfgBoleto5_2   = array(
                                    'Lancamento.codigo'     => '****',
                                    'Lancamento.lancamento' => '****Anteriores -->',
                                    'Lancamento.total'      => array(18, 13, 'DECIMAL_PT'),
                                );
                                
                                if( trim(mb_substr($linha, 8, 10)) == 'Anteriores' ) { $cfgBoleto5 = $cfgBoleto5_2; }
                                
                                $boleto5    = $this->lineToObject($linha, $cfgBoleto5);
                                
                                $boletos[$indiceBoleto]['Lancamento'][] = $boleto5['Lancamento'];
                                break;
                        }
                        break;
                }
            }
        }
                
        $this->loadModel('Boleto');
        echo date('H:i:s') . ' - Iniciando Importação de Boletos das Unidades...<br>';
        $this->Boleto->saveAll($boletos, array('deep' => true));
        echo count($boletos) . ' Boletos.<br>';
        echo date('H:i:s') . ' - Importação de Boletos do Condomínio concluída.<br>';
        echo '-------------------------------------<br><br>';

        $file->close();
    }
    
    public function acessos() {
        echo date('H:i:s') . ' - Importando Acessos...<br>';
        
        $this->loadModel('Condominio');
        $condominios    = $this->Condominio->find('list');
        foreach ($condominios as $codigo => $descricao) {
            
            $diretorio  = ROOT . DS . 'acessos' . DS;
            $dir      = new Folder( $diretorio );
            $arquivos = $dir->find('Acessos' . sprintf("%03d", $codigo) . '.*\.txt');
            foreach ($arquivos as $nomeArquivo) {
                $file = new File($diretorio . $nomeArquivo);

                $contents = $file->read();
                $convert = explode("\n", utf8_encode($contents));
                //$convert = explode("\n", $contents);

                $configuracao   = array(
                    'Acesso.condominio_id'   => $codigo,
                    'Acesso.unidade_id' => array($codigo, array(33, 6), 'CONCATENAR'),
                    'Acesso.rotina_id' => array(79, 23, 'ROTINA'),
                    'Acesso.responsavel' => array(39, 40),
                    'Acesso.usuario' => array(19, 14),
                    'Acesso.created' => array(0, 19, 'DATE_TIME')
                );
                $acessos    = array();
                foreach ($convert as $acesso) {
                    if(strlen($acesso) > 80) {
                        $acessos[] = $this->lineToObject($acesso, $configuracao);
                    }
                }

                $this->loadModel('Acesso');
                $this->Acesso->saveAll($acessos);

                echo count($acessos) . ' Acesso.<br>';

                $file->close();
            }
        }
        echo date('H:i:s') . ' - Importação de Acessos concluída.<br>';
        echo '-------------------------------------<br><br>';
    }
    
    private function getConfiguracaoUnidade($condominio, $carga) {
        return array(
            'Unidade.id'                    => array($condominio, array(1, 6), 'CONCATENAR'),
            'Unidade.unidade'               => array(1, 6),
            'Unidade.condominio_id'         => $condominio,
            'Unidade.carga'                 => $carga['Unidade']['carga'],
            'Unidade.cobranca'              => array(120, 1),
            //'Unidade.imobiliaria_id'        => array(120, 3),
            'Unidade.taxas_abertas'         => array(266, 4),
            'Unidade.divida'                => array(256, 10, 'DECIMAL'),
            'Unidade.acordo_administrativo' => array(237, 4),
            'Unidade.acordor_judicial'      => array(241, 4),
            'Unidade.ajuizada'              => array(232, 1),
            'Unidade.ultimo_pgto'           => array(245, 10, 'DATE'),
            'Unidade.cheque_aberto'         => array(255, 1),
            'Unidade.taxas_nada_consta'     => array(233, 4),
            'Unidade.fracao'                => array(370, 9, 'DECIMAL_PT'),
            'Unidade.percentual_ordinario'  => array(379, 3),
            
            'Ocupante.nome'         => array(7, 40),
            'Ocupante.documento'    => array(200, 14),
            'Ocupante.nascimento'   => array(382, 10, 'DATE'),
            'Ocupante.email'        => array(270, 50),
            'Ocupante.telefone'     => array(47, 11),
            
            'Proprietario.nome'         => array(58, 40),
            'Proprietario.documento'    => array(218, 14),
            'Proprietario.nascimento'   => array(392, 10, 'DATE'),
            'Proprietario.email'        => array(320, 50),
            'Proprietario.telefone'     => array(98, 11),
            'Proprietario.celular'      => array(109, 11),
            'Proprietario.endereco'     => array(161, 35),
            'Proprietario.bairro'       => array(121, 15),
            'Proprietario.cidade'       => array(136, 15),
            'Proprietario.uf'           => array(151, 2),
            'Proprietario.cep'          => array(153, 8)
        );
    }
    
    private function getConfiguracaoTaxaUnidade($condominio) {
        return array(
            'UnidadeTaxa.unidade_id'     => array($condominio, array(1, 6), 'CONCATENAR'),
            'UnidadeTaxa.condominio_id'  => $condominio,
            'UnidadeTaxa.taxa_id'        => array(7, 3),
            'UnidadeTaxa.codigo'         => array(56, 4),
            'UnidadeTaxa.mes'            => array(10, 2),
            'UnidadeTaxa.ano'            => array(12, 4),
            'UnidadeTaxa.vencimento'     => array(16, 10, 'DATE'),
            'UnidadeTaxa.valor'          => array(26, 10, 'DECIMAL'),
            'UnidadeTaxa.pagamento'      => array(36, 10, 'DATE'),
            'UnidadeTaxa.pago'           => array(46, 10, 'DECIMAL')
        );
    }
    
    private function getConfiguracaoFluxoCondominio($condominio) {
        return array(
            'Fluxo.condominio_id'   => $condominio,
            'Fluxo.data'            => array(1, 10, 'DATE'),
            'Fluxo.valor'           => array(11, 10, 'DECIMAL'),
            'Fluxo.conta'           => array(21, 30),
            'Fluxo.historico'       => array(64, 40),
            'Fluxo.tipo'            => array(62, 2),
            'Fluxo.documento'       => array(51, 11),
        );
    }
  
    /**
    * @param string $line Linha com os dados
    * @param Array $configuracao Formato dos dados da linha array( 'condominio' => array(0, 10)  )
    */
    private function lineToObject($line, $configuracao) {
        $retorno = array();
        foreach( $configuracao as $colunaDb => $intervalo ) {
            
            list($dominio, $coluna) = explode(".", $colunaDb);
            
            if(is_array($intervalo)) {
                switch ( ( isset($intervalo[2]) ? $intervalo[2] : '' ) ) {
                    case 'DECIMAL':
                        $retorno[$dominio][$coluna] = trim(mb_substr($line, $intervalo[0], $intervalo[1])) / 100;
                        break;
                    case 'DECIMAL_PT':
                        $retorno[$dominio][$coluna] = str_replace(',', '.', str_replace('.', '', trim(mb_substr($line, $intervalo[0], $intervalo[1])) ) );
                        break;
                    case 'DATE':
                        $retorno[$dominio][$coluna] = $this->dateDb( trim(mb_substr($line, $intervalo[0], $intervalo[1])) );
                        break;
                    case 'DATE_TIME':
                        $retorno[$dominio][$coluna] = $this->dateTimeDb( trim(mb_substr($line, $intervalo[0], $intervalo[1])) );
                        break;
                    case 'ROTINA':
                        $retorno[$dominio][$coluna] = $this->getRotina( trim(mb_substr($line, $intervalo[0], $intervalo[1])) );
                        break;
                    case 'CONCATENAR':
                        $retorno[$dominio][$coluna] = $this->concatenar($line, $intervalo);
                        break;
                    case 'ACORDO':
                        $retorno[$dominio][$coluna] = $this->acordo($line, $intervalo);
                        break;
                    case 'CONDOMINIO':
                        $retorno[$dominio][$coluna] = $this->condominioDb( trim(mb_substr($line, $intervalo[0], $intervalo[1])) );
                        break;
                    default :
                        $retorno[$dominio][$coluna] = trim(mb_substr($line, $intervalo[0], $intervalo[1]));
                        break;
                }
            } else { // Campo com valor fixo
                $retorno[$dominio][$coluna] = $intervalo;
            }
        }
//        debug(strlen($line));
//        debug($retorno);
        return $retorno;
    }
    
    public function zerarTabelas() {
        $this->loadModel('Condominio');
        $this->Condominio->query("TRUNCATE TABLE boletos");
        $this->Condominio->query("TRUNCATE TABLE clientes");
        $this->Condominio->query("TRUNCATE TABLE fluxos");
        $this->Condominio->query("TRUNCATE TABLE lancamentos");
        $this->Condominio->query("TRUNCATE TABLE sindicos");
        $this->Condominio->query("TRUNCATE TABLE taxas");
        $this->Condominio->query("TRUNCATE TABLE unidades");
        $this->Condominio->query("TRUNCATE TABLE unidade_taxas");
        $this->Condominio->query("UPDATE usuarios SET `password` = null WHERE `role` is not null");
        $this->Condominio->query("UPDATE condominios SET `importado` = 0");
    }
    
    private function concatenar($line, $intervalo) {
        return ( is_array($intervalo[0]) ? trim(mb_substr($line, $intervalo[0][0], $intervalo[0][1])) : $intervalo[0] ) . 
               ( is_array($intervalo[1]) ? trim(mb_substr($line, $intervalo[1][0], $intervalo[1][1])) : $intervalo[1] );
    }
    
    private function acordo($line, $intervalo) {
        return ( trim(mb_substr($line, $intervalo[0][0], $intervalo[0][1])) > 0 ) ? 'A' : (  (trim(mb_substr($line, $intervalo[1][0], $intervalo[1][1])) > 0) ? 'J' : ''  );
    }
    
    private function dateDb($date) {
        if($date != '' && strpos($date, '/') > 0) {
            list($dia, $mes, $ano) = explode('/', $date);
            if (strlen($ano) == 2) {
                    if ($ano > 50) $ano += 1900;
                    else $ano += 2000;
            }
            return "$ano-$mes-$dia";
        } else {
            return '';
        }
    }
    
    private function dateTimeDb($date) {
        if($date != '' && strpos($date, '/') > 0) {
            list($data, $hora) = explode(' ', $date);
            list($dia, $mes, $ano) = explode('/', $data);
            if (strlen($ano) == 2) {
                    if ($ano > 50) $ano += 1900;
                    else $ano += 2000;
            }
            return "$ano-$mes-$dia $hora";
        } else {
            return '';
        }
    }
    
    private function getRotina($nome) {
        $this->loadModel('Rotina');
        $rotinaDb   = $this->Rotina->find('first', array(
                'fields' => array('Rotina.id'),
                'conditions' => array(
                    'Rotina.descricao'=>$nome
                    )
            ));
        if(isset($rotinaDb['Rotina']['id']) && $rotinaDb['Rotina']['id'] > 0) {
            return $rotinaDb['Rotina']['id'];
        }
//        elseif ($nome = 'Taxas Aberto Unidade') {
//            return 15;
//        }
        else {
            debug($nome);
            return null;
        }
    }
    
    private function condominioDb($condominio) {
        if($condominio == '000' || $condominio == '') {
            return null;
        } else {
            return $condominio;
        }
    }

}