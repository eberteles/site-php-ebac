<?php
	if(count($breadcrumbs[$scope]) > 1) {
		$titlePage	= "";
		$breadcrumbsPage	= "";
		foreach($breadcrumbs[$scope] as $title => $link):
			$breadcrumbsPage .= "<li>";
			if(!empty($link)) {
				$breadcrumbsPage .= $this->Html->link($title, $link);
			} else {
				$breadcrumbsPage .= $title;
			}
			$titlePage	= $title;

			$breadcrumbsPage .= "</li>";
		endforeach
?>
		<section class="section-40 section-lg-40 bg-gray-lighter">
			<div class="breadcrumbs-wrap">
				<div class="shell text-center">
					<div class="wrap-sm-justify-horizontal">
						<div class="text-sm-left">
							<h4><?php echo $titlePage; ?></h4>
						</div>
						<div class="offset-top-22 offset-sm-top-0 text-sm-right">
							<ul id="breadcrumbs" class="breadcrumbs-custom">
								<?php echo $breadcrumbsPage; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php
	}
?>