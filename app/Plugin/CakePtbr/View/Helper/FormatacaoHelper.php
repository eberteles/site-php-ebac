<?php
/**
 * Helper para formatação de dados no padrão brasileiro
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @author        Juan Basso <jrbasso@gmail.com>
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Formatação Helper
 *
 * @link http://wiki.github.com/jrbasso/cake_ptbr/helper-formatao
 */
class FormatacaoHelper extends AppHelper {

/**
 * Helpers auxiliares
 *
 * @var array
 * @access public
 */
	public $helpers = array('Time', 'Number', 'Html');
        
        public function string($valor) {
            if($valor != null && $valor != '') {
                return $valor;
            } else {
                return 'Não Consta';
            }
        }
        
        public function localCobranca($valor) {
            switch ($valor) {
                case '1' :
                    return 'Proprietário';
                case '2' :
                    return 'Ocupante';
//                case '3' :
//                    return 'Endereço da Imobiliária';
                default:
                    return 'Não Consta';
            }
        }
        
        public function situacaoUnidade($acordoAdm, $acordoJud, $ajuizada) {
            $situacao = ($acordoAdm > 0) ? 'Acordo Administrativo' : ( ($acordoJud > 0) ? 'Acordo Judicial' : '' );
            if($ajuizada == 'S') {
                $situacao .= ($situacao == '') ? 'Ajuizadas' : ' / Ajuizadas';
            }
            return $situacao;
        }
        
        
        public function mensagemBoleto($mensagem, $mensagem2) {
            return ( $mensagem == '' && $mensagem2 == '' ) ? 'Mensagem do mês: Nenhuma.' : $mensagem;
        }
        
        public function codigoBanco($linhaDigitavel) {
            $codigo = substr($linhaDigitavel, 0, 3);
            if($codigo == '001') { $codigo .= '-9'; }
            if($codigo == '104') { $codigo .= '-0'; }
            return $codigo;
        }
        
        public function instrucaoBoleto($boleto) {
            
            $mensagens = array();
            $totalComDesconto   = ($boleto['total'] + $boleto['honorario']) - $boleto['desconto'];
            
            if( $boleto['desconto'] > 0 ) {
                $mensagens['wDescAba'] = '';
                $mensagens['wValCobr'] = '';
                $mensagens['wInstr11'] = '1/4-Conceder desconto de ' . $this->moeda($boleto['desconto']) . ' até o dia ' . $this->data($boleto['limite_desconto']) . ', COBRAR: ' . $this->moeda($totalComDesconto);
                $mensagens['wInstr12'] = 'Pagamentos entre o desconto e Vencimento poderão gerar diferenças a receber.';
            } else {
                $mensagens['wDescAba'] = $this->moeda($boleto['desconto']);
                $mensagens['wValCobr'] = $this->moeda($totalComDesconto);
                $mensagens['wInstr11'] = '1/4-Não conceder qualquer tipo de desconto. O valor já está calculado.';
                $mensagens['wInstr12'] = 'Os cálculos desta guia são válidos até a data do vencimento.';
            }
            
            if($totalComDesconto <= 0) {
                $mensagens['wDescAba'] = '';
                $mensagens['wValCobr'] = '*** Não Receber ***';
                $mensagens['wInstr11'] = '1/1 - ' . ( $totalComDesconto < 0 ) ? '*** NÃO RECEBER. GUIA COMPENSADA ***' : '*** NÃO RECEBER. GUIA QUITADA ***';
                $mensagens['wInstr12'] = '';
                
                $mensagens['wInstr21'] = '';
                $mensagens['wInstr22'] = '';
                $mensagens['wInstr31'] = '';
                $mensagens['wInstr41'] = '';
            } else {
                $mensagens['wInstr21'] = '2/4-Após o vencimento, receber o VALOR DO DOCUMENTO (sem desconto).  Os acréscimos';
                $mensagens['wInstr22'] = 'devidos (Multa,Juros,Correção,etc) serão lançados e cobrados na próxima guia.';
                $mensagens['wInstr31'] = '3/4-NÃO RECEBER APÓS O DIA ' . $this->data($boleto['validade']) . '.';
                $mensagens['wInstr41'] = '4/4-Nenhuma.';
            }
            
            return $mensagens;            
        }
        
        public function getSacado($unidade) {
            $sacado = ($unidade['Unidade']['cobranca'] == 'P') ? 'Proprietario' : ( ($unidade['Unidade']['cobranca'] == 'I') ? 'Imobiliaria' : 'Ocupante' );
            return $unidade[$sacado]['nome'];
        }

        /**
 * Formata a data
 *
 * @param integer $data Data em timestamp ou null para atual
 * @param array $opcoes É possível definir o valor de 'invalid' e 'userOffset' que serão usados pelo helper Time
 * @return string Data no formato dd/mm/aaaa
 * @access public
 */
	public function data($data = null, $opcoes = array()) {
                if($data == null || $data == '') return '';
		$padrao = array(
			'invalid' => '31/12/1969',
			'userOffset' => null
		);
		$config = array_merge($padrao, $opcoes);

		$data = $this->_ajustaDataHora($data);
		return $this->Time->format('d/m/Y', $data, $config['invalid'], $config['userOffset']);
	}

/**
 * Formata a data e hora
 *
 * @param integer $dataHora Data e hora em timestamp ou null para atual
 * @param boolean $segundos Mostrar os segundos
 * @param array $opcoes É possível definir o valor de 'invalid' e 'userOffset' que serão usados pelo helper Time
 * @return string Data no formato dd/mm/aaaa hh:mm:ss
 * @access public
 */
	public function dataHora($dataHora = null, $segundos = true, $opcoes = array()) {
		$padrao = array(
			'invalid' => '31/12/1969',
			'userOffset' => null
		);
		$config = array_merge($padrao, $opcoes);

		$dataHora = $this->_ajustaDataHora($dataHora);
		if ($segundos) {
			return $this->Time->format('d/m/Y H:i:s', $dataHora, $config['invalid'], $config['userOffset']);
		}
		return $this->Time->format('d/m/Y H:i', $dataHora, $config['invalid'], $config['userOffset']);
	}

/**
 * Mostrar a data completa
 *
 * @param integer $dataHora Data e hora em timestamp ou null para atual
 * @return string Descrição da data no estilo "Sexta-feira", 01 de Janeiro de 2010, 00:00:00"
 * @access public
 */
	public function dataCompleta($dataHora = null) {
		$_diasDaSemana = array('Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado');
		$_meses = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');

		$dataHora = $this->_ajustaDataHora($dataHora);
		$w = date('w', $dataHora);
		$n = date('n', $dataHora) - 1;

		return sprintf('%s, %02d de %s de %04d, %s', $_diasDaSemana[$w], date('d', $dataHora), $_meses[$n], date('Y', $dataHora), date('H:i:s', $dataHora));
	}

/**
 * Se a data for nula, usa data atual
 *
 * @param mixed $data
 * @return integer Se null, retorna a data/hora atual
 * @access protected
 */
	protected function _ajustaDataHora($data) {
		if (is_null($data)) {
			return time();
		}
		if (is_integer($data) || ctype_digit($data)) {
			return (int)$data;
		}
		return strtotime((string)$data);
	}

/**
 * Mostrar uma data em tempo
 *
 * @param integer $dataHora Data e hora em timestamp, dd/mm/YYYY ou null para atual
 * @param string $limite null, caso não haja expiração ou então, forneça um tempo usando o formato inglês para strtotime: Ex: 1 year
 * @return string Descrição da data em tempo ex.: a 1 minuto, a 1 semana
 * @access public
 */
	public function tempo($dataHora = null, $limite = '30 days'){
		if (!$dataHora) {
			$dataHora = time();
		}

		if (strpos($dataHora, '/') !== false) {
			$_dataHora = str_replace('/', '-', $dataHora);
			$_dataHora = date('ymdHi', strtotime($_dataHora));
		} elseif (is_string($dataHora)) {
			$_dataHora = date('ymdHi', strtotime($dataHora));
		} else {
			$_dataHora = date('ymdHi', $dataHora);
		}

		if ($limite !== null) {
			if ($_dataHora > date('ymdHi', strtotime('+ ' . $limite))) {
				return $this->dataHora($dataHora);
			}
		}

		$_dataHora = date('ymdHi') - $_dataHora;
		if ($_dataHora > 88697640 && $_dataHora < 100000000) {
			$_dataHora -= 88697640;
		}

		switch ($_dataHora) {
			case 0:
				return 'menos de 1 minuto';
			case ($_dataHora < 99):
				if ($_dataHora === 1) {
					return '1 minuto';
				} elseif ($_dataHora > 59) {
					return ($_dataHora - 40) . ' minutos';
				}
				return $_dataHora . ' minutos';
			case ($_dataHora > 99 && $_dataHora < 2359):
				$flr = floor($_dataHora * 0.01);
				return $flr == 1 ? '1 hora' : $flr . ' horas';

			case ($_dataHora > 2359 && $_dataHora < 310000):
				$flr = floor($_dataHora * 0.0001);
				return $flr == 1 ? '1 dia' : $flr . ' dias';

			case ($_dataHora > 310001 && $_dataHora < 12320000):
				$flr = floor($_dataHora * 0.000001);
				return $flr == 1 ? '1 mes' : $flr . ' meses';

			case ($_dataHora > 100000000):
			default:
				$flr = floor($_dataHora * 0.00000001);
				return $flr == 1 ? '1 ano' : $flr . ' anos';

		}
	}


/**
 * Número float com ponto ao invés de vírgula
 *
 * @param float $numero Número
 * @param integer $casasDecimais Número de casas decimais
 * @return string Número formatado
 * @access public
 */
	public function precisao($numero, $casasDecimais = 3) {
		return number_format($numero, $casasDecimais, ',', '.');
	}

/**
 * Valor formatado com símbolo de %
 *
 * @param float $numero Número
 * @param integer $casasDecimais Número de casas decimais
 * @return string Número formatado com %
 * @access public
 */
	public function porcentagem($numero, $casasDecimais = 2) {
		return $this->precisao($numero, $casasDecimais) . '%';
	}
                
        
        public function getFloat($valorReal, $precisao = -1) {
            $valor  = preg_replace("/[^0-9]/", "", $valorReal) / 100;
            
            return ($precisao >=0) ? $this->precisao($valor, $precisao) : $valor;
        }

/**
 * Formata um valor para reais
 *
 * @param float $valor Valor
 * @param array $opcoes Mesmas opções de Number::currency()
 * @return string Valor formatado em reais
 * @access public
 */
	public function moeda($valor, $opcoes = array()) {
		$padrao = array(
			'before'=> 'R$ ',
			'after' => '',
			'zero' => 'R$ 0,00',
			'places' => 2,
			'thousands' => '.',
			'decimals' => ',',
			'negative' => '()',
			'escape' => true
		);
		$config = array_merge($padrao, $opcoes);
		if ($valor > -1 && $valor < 1) {
			$before = $config['before'];
			$config['before'] = '';
			$formatado = $this->Number->format(abs($valor), $config);
			if ($valor < 0 ) {
				if ($config['negative'] == '()') {
					return '(' . $before . $formatado .')';
				} else {
					return $before . $config['negative'] . $formatado;
				}
			}
			return $before . $formatado;
		}
		return $this->Number->currency($valor, null, $config);
	}

/**
 * Valor por extenso em reais
 *
 * @param float $numero
 * @return string Valor em reais por extenso
 * @access public
 * @link http://forum.imasters.uol.com.br/index.php?showtopic=125375
 */
	public function moedaPorExtenso($numero) {
		$singular = array('centavo', 'real', 'mil', 'milhão', 'bilhão', 'trilhão', 'quatrilhão');
		$plural = array('centavos', 'reais', 'mil', 'milhões', 'bilhões', 'trilhões', 'quatrilhões');

		$c = array('', 'cem', 'duzentos', 'trezentos', 'quatrocentos', 'quinhentos', 'seiscentos', 'setecentos', 'oitocentos', 'novecentos');
		$d = array('', 'dez', 'vinte', 'trinta', 'quarenta', 'cinquenta', 'sessenta', 'setenta', 'oitenta', 'noventa');
		$d10 = array('dez', 'onze', 'doze', 'treze', 'quatorze', 'quinze', 'dezesseis', 'dezesete', 'dezoito', 'dezenove');
		$u = array('', 'um', 'dois', 'três', 'quatro', 'cinco', 'seis', 'sete', 'oito', 'nove');

		$z = 0;
		$rt = '';

		$valor = number_format($numero, 2, '.', '.');
		$inteiro = explode('.', $valor);
		$tamInteiro = count($inteiro);

		// Normalizandos os valores para ficarem com 3 digitos
		$inteiro[0] = sprintf('%03d', $inteiro[0]);
		$inteiro[$tamInteiro - 1] = sprintf('%03d', $inteiro[$tamInteiro - 1]);

		$fim = $tamInteiro - 1;
		if ($inteiro[$tamInteiro - 1] <= 0) {
			$fim--;
		}
		foreach ($inteiro as $i => $valor) {
			$rc = $c[$valor{0}];
			if ($valor > 100 && $valor < 200) {
				$rc = 'cento';
			}
			$rd = '';
			if ($valor{1} > 1) {
				$rd = $d[$valor{1}];
			}
			$ru = '';
			if ($valor > 0) {
				if ($valor{1} == 1) {
					$ru = $d10[$valor{2}];
				} else {
					$ru = $u[$valor{2}];
				}
			}

			$r = $rc;
			if ($rc && ($rd || $ru)) {
				$r .= ' e ';
			}
			$r .= $rd;
			if ($rd && $ru) {
				$r .= ' e ';
			}
			$r .= $ru;
			$t = $tamInteiro - 1 - $i;
			if (!empty($r)) {
				$r .= ' ';
				if ($valor > 1) {
					$r .= $plural[$t];
				} else {
					$r .= $singular[$t];
				}
			}
			if ($valor == '000') {
				$z++;
			} elseif ($z > 0) {
				$z--;
			}
			if ($t == 1 && $z > 0 && $inteiro[0] > 0) {
				if ($z > 1) {
					$r .= ' de ';
				}
				$r .= $plural[$t];
			}
			if (!empty($r)) {
				if ($i > 0 && $i < $fim  && $inteiro[0] > 0 && $z < 1) {
					if ($i < $fim) {
						$rt .= ', ';
					} else {
						$rt .= ' e ';
					}
				} elseif ($t == 0 && $inteiro[0] > 0) {
					$rt .= ' e ';
				} else {
					$rt .= ' ';
				}
				$rt .= $r;
			}
		}

		if (empty($rt)) {
			return 'zero';
		}
		return trim(str_replace('  ', ' ', $rt));
	}

        public function barcode($valor){
            $valor  = $this->trasformaCnab($valor);
            $fino = 1 ;
            $largo = 3 ;
            $altura = 50 ;
            
            $barcodes[0] = "00110" ;
            $barcodes[1] = "10001" ;
            $barcodes[2] = "01001" ;
            $barcodes[3] = "11000" ;
            $barcodes[4] = "00101" ;
            $barcodes[5] = "10100" ;
            $barcodes[6] = "01100" ;
            $barcodes[7] = "00011" ;
            $barcodes[8] = "10010" ;
            $barcodes[9] = "01010" ;
            
            for($f1=9;$f1>=0;$f1--){ 
              for($f2=9;$f2>=0;$f2--){  
                $f = ($f1 * 10) + $f2 ;
                $texto = "" ;
                for($i=1;$i<6;$i++){ 
                  $texto .=  substr($barcodes[$f1],($i-1),1) . substr($barcodes[$f2],($i-1),1);
                }
                $barcodes[$f] = $texto;
              }
            }
          //Desenho da barra
          //Guarda inicial
          ?><img src=<?php echo $this->Html->url('/', true); ?>images/p.png width=<?php echo $fino?> height=<?php echo $altura?> border=0><img 
          src=<?php echo $this->Html->url('/', true); ?>images/b.png width=<?php echo $fino?> height=<?php echo $altura?> border=0><img 
          src=<?php echo $this->Html->url('/', true); ?>images/p.png width=<?php echo $fino?> height=<?php echo $altura?> border=0><img 
          src=<?php echo $this->Html->url('/', true); ?>images/b.png width=<?php echo $fino?> height=<?php echo $altura?> border=0><img 
          <?php
          $texto = $valor ;
          if((strlen($texto) % 2) <> 0){
                  $texto = "0" . $texto;
          }

          // Draw dos dados
          while (strlen($texto) > 0) {
            $i = round($this->esquerda($texto,2));
            $texto = $this->direita($texto,strlen($texto)-2);
            $f = $barcodes[$i];
            for($i=1;$i<11;$i+=2){
              if (substr($f,($i-1),1) == "0") {
                $f1 = $fino ;
              }else{
                $f1 = $largo ;
              }
          ?>
              src=<?php echo $this->Html->url('/', true); ?>images/p.png width=<?php echo $f1?> height=<?php echo $altura?> border=0><img 
          <?php
              if (substr($f,$i,1) == "0") {
                $f2 = $fino ;
              }else{
                $f2 = $largo ;
              }
          ?>
              src=<?php echo $this->Html->url('/', true); ?>images/b.png width=<?php echo $f2?> height=<?php echo $altura?> border=0><img 
          <?php
            }
          }

          // Draw guarda final
          ?>
          src=<?php echo $this->Html->url('/', true); ?>images/p.png width=<?php echo $largo?> height=<?php echo $altura?> border=0><img 
          src=<?php echo $this->Html->url('/', true); ?>images/b.png width=<?php echo $fino?> height=<?php echo $altura?> border=0><img 
          src=<?php echo $this->Html->url('/', true); ?>images/p.png width=<?php echo 1?> height=<?php echo $altura?> border=0> 
            <?php
        }
        
        private function trasformaCnab($linha) {
            return  substr($linha, 0, 4) . substr($linha, 38, 1) . substr($linha, 40, 14) . substr($linha, 4, 1) . 
                    substr($linha, 6, 4) . substr($linha, 12, 5) . substr($linha, 18, 5) . substr($linha, 25, 5) . 
                    substr($linha, 31, 5);
        }
        
        private function esquerda($entra,$comp){
            return substr($entra,0,$comp);
        }
        
        private function direita($entra,$comp){
            return substr($entra,strlen($entra)-$comp,$comp);
        }
        
        /**
         * 
         * @param type $val
         * @param type $mask = '##.###.###/####-##'
         * @return type
         */
        public function mask($val, $mask){
            $maskared = '';
            $k = 0;
            for($i = 0; $i<=strlen($mask)-1; $i++) {
                if($mask[$i] == '#') {
                    if(isset($val[$k]))
                        $maskared .= $val[$k++];
                }
                else {
                    if(isset($mask[$i]))
                        $maskared .= $mask[$i];
                }
            }
            return $maskared;
        }
        
        public function documento($documento) {
            switch (strlen($documento)) {
                case 11:
                    return $this->mask($documento, '###.###.###-##');
                case 14:
                    return $this->mask($documento, '##.###.###/####-##');
                default:
                    return $documento;
            }
        }
        
        public function cep($cep) {
            if(strlen($cep) == 8) {
                return $this->mask($cep, '#####-###');
            } else {
                return $cep;
            }
        }
        
        public function telefone($telefone, $ddd = true) {
            if(strlen($telefone) > 7) {
                if($ddd) {
                    return '(' . substr($telefone, 0, 2) . ') ' . $this->mask(trim(substr($telefone, 2)), $this->getMaskTelefone(trim(substr($telefone, 2))));
                } else {
                    return $this->mask( $telefone, $this->getMaskTelefone($telefone) );
                }
            }
            return $telefone;
        }
        
        private function getMaskTelefone($nuTelefone) {
            switch (strlen($nuTelefone)) {
                case 8: 
                    return '####-####';
                case 9:
                    return '#####-####';
            }
        }
        
}
