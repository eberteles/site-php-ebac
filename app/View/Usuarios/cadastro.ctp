<?php echo $this->Form->create('Usuario', array('data-form-base'=>$this->base, 'data-form-output'=>"form-output-global", 'data-form-type'=>"acesso", 'url'=> "/envia/acesso", 'class'=>"rd-mailform rd-mailform-mod-1" )); ?>

    <section>
      <div class="shell" id="div-form">
        <div class="range range-sm-center">
          <div class="cell-sm-6 cell-md-5 cell-lg-4">
            <div class="responsive-tabs responsive-tabs-default responsive-tabs-horizontal responsive-tabs-silver-chalice">
              <div class="resp-tabs-container">
                <div class="animated fadeIn">
                  <!-- RD Mailform-->
                  
                    <!--p align="center"><font face="Verdana" size="2" color="#FF0000">< % = wMensagemErro %></font></p-->
                    <div class="form-group">
                      <?php echo $this->Form->input('email', array('id'=>'login-email', 'data-constraints'=>'@Required @Email', 'class'=>'form-control', 'label'=>'E-mail constante no nosso Cadastro')); ?>
                    </div>
                    <div class="form-group">
                      <?php echo $this->Form->input('nascimento', array('id'=>'login-nascimento', 'data-constraints'=>'@Required', 'class'=>'form-control data-pt', 'label'=>'Data de Nascimento / Abertura')); ?>
                    </div>
                    <div class="form-group">
                      <?php echo $this->Form->input('username', array('id'=>'login-documento', 'data-constraints'=>'@Required @Numeric', 'class'=>'form-control cpf-cnpj', 'label'=>'CPF/CNPJ')); ?>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="cell-sm-6 cell-md-5 cell-lg-4">
            <div class="responsive-tabs responsive-tabs-default responsive-tabs-horizontal responsive-tabs-silver-chalice">
              <div class="resp-tabs-container">
                <div class="animated fadeIn">
                  <!-- RD Mailform-->
                    <!--p align="center"><font face="Verdana" size="2" color="#FF0000">< % = wMensagemErro %></font></p-->
                    <div class="form-group">
                      <?php echo $this->Form->input('password', array('id'=>'login-password', 'data-constraints'=>'@Required', 'class'=>'form-control', 'label'=>'Senha')); ?>
                    </div>
                    <div class="form-group">
                      <?php echo $this->Form->input('psword', array('id'=>'login-password-confirm', 'type'>="password", 'data-constraints'=>'@Required', 'class'=>'form-control', 'label'=>'Confirmação da Senha')); ?>
                    </div>

                </div>
              </div>
            </div>
          </div>
          <div class="cell-sm-6 cell-md-5 cell-lg-8">

              <div class="resp-tabs-container">
                <div class="animated fadeIn">
                  <!-- RD Mailform-->
                  
                    <!--p align="center"><font face="Verdana" size="2" color="#FF0000">< % = wMensagemErro %></font></p-->
                    <div class="group group-middle text-center text-xs-left">
                      <button type="submit" name="cmdConcluir" value="Concluir" class="btn btn-primary btn-sm">Concluir
                          <i class="fa fa-sign-in"></i></button><br>
                        <a href="<?php echo $this->base; ?>/unidades/inclusao">Inclusão Cadastral</a>
                    </div>
                </div>
              </div>

          </div>
        </div>
      </div>
    </section>
</form>
