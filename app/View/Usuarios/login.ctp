
        <section>
          <div class="shell">
            <div class="range range-sm-center">
              <div class="cell-sm-6 cell-md-5 cell-lg-4">
                <div class="responsive-tabs responsive-tabs-default responsive-tabs-horizontal responsive-tabs-silver-chalice">
                  <div class="resp-tabs-container">
                    <div class="animated fadeIn">
                      <!-- RD Mailform-->
                      <?php echo $this->Form->create('Usuario'); ?>
                        <!--p align="center"><font face="Verdana" size="2" color="#FF0000">< % = wMensagemErro %></font></p-->
                        <div class="form-group">
                          <?php echo $this->Form->input('username', array('id'=>'login-documento', 'data-constraints'=>'@Required @Numeric', 'class'=>'form-control cpf-cnpj', 'label'=>'CPF/CNPJ')); ?>
                        </div>
                        <div class="form-group">
                          <?php echo $this->Form->input('password', array('id'=>'login-password', 'data-constraints'=>'@Required', 'class'=>'form-control', 'label'=>'Senha')); ?>
                        </div>
                        <div class="group group-middle offset-top-30 text-center text-xs-left">
                          <button type="button" class="btn btn-warning btn-sm" onclick="location.href='<?php echo $this->base; ?>/usuarios/cadastro'">
                            <i class="fa fa-user-plus"></i> <b>CADASTRE-SE</b>
                          </button>&nbsp;&nbsp;
                          <button type="submit" name="cmdAcessar" value="Acessar" class="btn btn-primary btn-sm">Acessar
                            <i class="fa fa-sign-in"></i></button>
                            <a href="<?php echo $this->base; ?>/usuarios/cadastro">Esqueceu sua Senha?</a>&nbsp;&nbsp;  &nbsp;&nbsp;
                            <a href="<?php echo $this->base; ?>/unidades/inclusao">Inclusão Cadastral</a>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>