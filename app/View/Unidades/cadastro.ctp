
    <table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th colspan="2">Ocupante: <?php echo $this->Formatacao->string($this->Session->read('UnidadeAtual.Ocupante.nome')); ?></th>
        </tr>
      </thead>
      <?php if($this->Session->read('UnidadeAtual.Ocupante.nome') != '') { ?>
      <tbody>
        <tr>
            <td><b>Telefone:</b> <?php echo $this->Formatacao->telefone($this->Session->read('UnidadeAtual.Ocupante.telefone')); ?></td>
            <td><b>Data Nascimento:</b> <?php echo $this->Formatacao->data($this->Session->read('UnidadeAtual.Ocupante.nascimento')); ?></td>
        </tr>
        <tr>
            <td><b>Cpf/Cnpj:</b> <?php echo $this->Formatacao->documento($this->Session->read('UnidadeAtual.Ocupante.documento')); ?></td>
            <td><b>Email:</b> <?php echo $this->Session->read('UnidadeAtual.Ocupante.email'); ?></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
    <table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th colspan="2">Proprietário: <?php echo $this->Formatacao->string($this->Session->read('UnidadeAtual.Proprietario.nome')); ?></th>
        </tr>
      </thead>
      <?php if($this->Session->read('UnidadeAtual.Proprietario.nome') != '') { ?>
      <tbody>
        <tr>
            <td><b>Telefones:</b> <?php echo $this->Formatacao->telefone($this->Session->read('UnidadeAtual.Proprietario.telefone')) . ' / ' . $this->Formatacao->telefone($this->Session->read('UnidadeAtual.Proprietario.celular')); ?></td>
            <td><b>Data Nascimento:</b> <?php echo $this->Formatacao->data($this->Session->read('UnidadeAtual.Proprietario.nascimento')); ?></td>
        </tr>
        <tr>
            <td><b>Cpf/Cnpj:</b> <?php echo $this->Formatacao->documento($this->Session->read('UnidadeAtual.Proprietario.documento')); ?></td>
            <td><b>Email:</b> <?php echo $this->Session->read('UnidadeAtual.Proprietario.email'); ?></td>
        </tr>
        <tr>
            <td colspan="2"><b>Endereço:</b></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
<!--    <table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th colspan="2">Imobiliária: <?php echo $this->Formatacao->string($this->Session->read('UnidadeAtual.Imobiliaria.nome')); ?></th>
        </tr>
      </thead>
      <?php if($this->Session->read('UnidadeAtual.Imobiliaria.nome') != '') { ?>
      <tbody>
        <tr>
            <td colspan="2"><b>Telefones:</b> <?php echo $this->Formatacao->telefone($this->Session->read('UnidadeAtual.Imobiliaria.telefone')) . ' / ' . $this->Formatacao->telefone($this->Session->read('UnidadeAtual.Imobiliaria.celular')); ?></td>
        </tr>
        <tr>
            <td><b>Cpf/Cnpj:</b> <?php echo $this->Formatacao->documento($this->Session->read('UnidadeAtual.Imobiliaria.documento')); ?></td>
            <td><b>Email:</b> <?php echo $this->Session->read('UnidadeAtual.Imobiliaria.email'); ?></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>-->
    <table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th>Responsável pelo Pagamento: <?php echo $this->Formatacao->localCobranca($this->Session->read('UnidadeAtual.Unidade.cobranca')); ?></th>
        </tr>
      </thead>
    </table>

<br>
<center><a href="<?php echo $this->base; ?>/unidades/atualizacao"><div class="btn btn-sm btn-primary">alterar</div></a></center>
