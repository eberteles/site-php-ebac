    <section class="section-top-40 section-sm-top-40">
      <div class="shell">
        <div class="range">
          <div class="cell-lg-12">
            <!-- Responsive-tabs-->
            <div tab-type="<?php if(count($condominios) > 1) { echo 'condominios';} ?>" data-type="accordion" class="responsive-tabs responsive-tabs-horizontal responsive-tabs-collapsed">
              <ul class="resp-tabs-list">
              <?php foreach ($condominios as $condominio) { ?>
                <li><?php echo $condominio; ?></li>
              <?php } ?>
              </ul>

              <div class="resp-tabs-container">
              <?php foreach ($condominios as $codigo => $condominio) { ?>
                <div id="<?php echo $codigo; ?>"><?php if(count($condominios) == 1) { echo $this->element('unidades', array("unidades" => $this->Session->read('unidades')));} ?></div>
              <?php } ?>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>

<?php
    if(count($condominios) > 1) {
?>
<script>
    $("div.resp-tabs-container > div").hide();
</script>
<?php
    }
?>
