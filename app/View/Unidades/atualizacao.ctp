    <section>
      <div class="shell">
        <div class="range">
          <div class="col-sm-12 col-md-12" id="div-form">
            <div class="offset-top-50">
              <!-- RD Mailform-->
              <form data-form-output="form-output-global" data-form-type="alteracao-cadastral" method="post" action="<?php echo $this->base; ?>/envia/alteracao_cadastral" class="rd-mailform rd-mailform-mod-1">
                <div class="range">

                  <div class="cell-sm-12">
                      <a href="#" id="instrucoes-importantes-2" data-toggle="tooltip" title="Instruções Importantes"><span class="icon icon-sm-variant-2 icon-primary fa-question-circle"></span></a>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <b><?php echo $this->Session->read('UnidadeAtual.Condominio.id') . " - " . $this->Session->read('UnidadeAtual.Condominio.nome'); ?></b>
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <b>Unidade: <?php echo $this->Session->read('UnidadeAtual.Unidade.unidade'); ?></b>
                    </div>
                  </div>

                  <div class="cell-sm-12 offset-top-18">
                      <h6>Dados do Ocupante</h6>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="contact-ocupante" class="form-label-outside">Nome do Ocupante</label>
                      <input id="contact-ocupante" type="text" name="txtOcupante" class="form-control" value="<?php echo $this->Session->read('UnidadeAtual.Ocupante.nome'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="contact-email-ocupante" class="form-label-outside">E-mail do Ocupante</label>
                      <input id="contact-email-ocupante" type="email" name="txtEmailOcup" class="form-control" value="<?php echo $this->Session->read('UnidadeAtual.Ocupante.email'); ?>">
                    </div>
                  </div>

                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-cpf-ocupante" class="form-label-outside">Cpf/Cnpj</label>
                      <input id="contact-cpf-ocupante" type="text" name="txtCpfCnpjOcup" class="form-control cpf-cnpj" value="<?php echo $this->Session->read('UnidadeAtual.Ocupante.documento'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-dt-ocupante" class="form-label-outside">Data de Nascimento</label>
                      <input id="contact-dt-ocupante" type="text" name="txtNascOcup" class="form-control data-pt" value="<?php echo $this->Formatacao->data( $this->Session->read('UnidadeAtual.Ocupante.nascimento') ); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-tel-ocupante" class="form-label-outside">Telefone</label>
                      <input id="contact-tel-ocupante" type="text" name="txtFoneOcup" class="form-control telefone" value="<?php echo $this->Session->read('UnidadeAtual.Ocupante.telefone'); ?>">
                    </div>
                  </div>

                  <div class="cell-sm-12 offset-top-18">
                      <h6>Dados do Proprietário</h6>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="contact-proprietario" class="form-label-outside">Nome do Proprietário</label>
                      <input id="contact-proprietario" type="text" name="txtProprietario" data-constraints="@Required" class="form-control" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.nome'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="contact-email-proprietario" class="form-label-outside">E-mail do Proprietário</label>
                      <input id="contact-email-proprietario" type="email" name="txtEmailProp" class="form-control" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.email'); ?>">
                    </div>
                  </div>

                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-cpf-proprietario" class="form-label-outside">Cpf/Cnpj</label>
                      <input id="contact-cpf-proprietario" type="text" name="txtCpfCnpjProp" data-constraints="@Required" class="form-control cpf-cnpj" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.documento'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-dt-proprietario" class="form-label-outside">Data de Nascimento</label>
                      <input id="contact-dt-proprietario" type="text" name="txtNascProp" data-constraints="@Required" class="form-control data-pt" value="<?php echo $this->Formatacao->data( $this->Session->read('UnidadeAtual.Proprietario.nascimento') ); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-tel1-proprietario" class="form-label-outside">Telefone 1</label>
                      <input id="contact-tel1-proprietario" type="text" name="txtFoneProp1" data-constraints="@Required" class="form-control telefone" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.telefone'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-tel2-proprietario" class="form-label-outside">Telefone 2</label>
                      <input id="contact-tel2-proprietario" type="text" name="txtFoneProp2" data-constraints="@Required" class="form-control telefone" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.celular'); ?>">
                    </div>
                  </div>

                  <div class="cell-sm-12 offset-top-18">
                      <h6>Informar o endereço do Proprietário somente se for diferente do endereço do imóvel</h6>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="atualiza-endereco" class="form-label-outside">Endereço</label>
                      <input id="atualiza-endereco" type="text" name="txtEndeProp" class="form-control" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.endereco'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="atualiza-bairro" class="form-label-outside">Bairro</label>
                      <input id="atualiza-bairro" type="text" name="txtBairProp" class="form-control" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.bairro'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="atualiza-cidade" class="form-label-outside">Cidade</label>
                      <input id="atualiza-cidade" type="text" name="txtCidaProp" class="form-control" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.cidade'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-2 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="atualiza-uf" class="form-label-outside">UF</label>
                      <input id="atualiza-uf" type="text" name="txtUfProp" class="form-control uf" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.uf'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="atualiza-cep" class="form-label-outside">CEP</label>
                      <input id="atualiza-cep" type="text" name="txtCepProp1" class="form-control cep" value="<?php echo $this->Session->read('UnidadeAtual.Proprietario.cep'); ?>">
                    </div>
                  </div>

<!--                  <div class="cell-sm-12 offset-top-18">
                      <h6>Dados da Imobiliária</h6>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="contact-imobiliaria" class="form-label-outside">Nome da Imobiliária</label>
                      <input id="contact-imobiliaria" type="text" name="txtImobiliaria" class="form-control" value="<?php echo $this->Session->read('UnidadeAtual.Imobiliaria.nome'); ?>">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="contact-tel-imobiliaria" class="form-label-outside">Telefone</label>
                      <input id="contact-tel-imobiliaria" type="text" name="txtFoneImob" class="form-control telefone" value="<?php echo $this->Session->read('UnidadeAtual.Imobiliaria.telefone'); ?>">
                    </div>
                  </div>-->

                  <div class="cell-sm-12 offset-top-18">
                      <hr>
                  </div>
                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="contact-cobranca" class="form-label-outside">Responsável Pagamento</label>
                      <select id="contact-cobranca" name="cmbTipoCobr" class="form-control">
                        <option>Selecione</option>
                        <option<?php if($this->Session->read('UnidadeAtual.Unidade.cobranca') == '1') echo ' selected'; ?>>Proprietário</option>
                        <option<?php if($this->Session->read('UnidadeAtual.Unidade.cobranca') == '2') echo ' selected'; ?>>Ocupante</option>
                      </select>
                    </div>
                  </div>

                  <div class="cell-sm-12 offset-top-18">
                      <h6>Responsável pela Alteração</h6>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="email-remetente" class="form-label-outside">E-Mail do Remetente</label>
                      <input id="email-remetente" type="text" name="txtEmail" data-constraints="@Email" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="email-remetente-confirm" class="form-label-outside">Confirmação do Email</label>
                      <input id="email-remetente-confirm" type="text" name="txtConfirmaEmail" data-constraints="@Email" class="form-control">
                    </div>
                  </div>

                  <div class="cell-xs-12 offset-top-30">
                    <button type="submit" class="btn btn-primary">Enviar Alteração Cadastral</button>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <div class="col-sm-12 col-md-9 hidden" id="div-form-sucesso">
<!--            <h5>Alteração Cadastral</h5>
            <hr>-->
            <div class="offset-top-50">
                <div class="grid-element">
                    <h4 class="text-center">Confirmação</h4><br><br>
                    <p class="text-center">Os dados foram enviados para a Ebac.</p><br>
                    <p class="text-center">Aguarde a resposta em até 2 dias úteis</p>
                </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-9 hidden" id="div-form-error">
<!--            <h5>Alteração Cadastral</h5>
            <hr>-->
            <div class="offset-top-50">
                <div class="grid-element">
                    <h4 class="text-center">Não foi possível enviar sua Mensagem</h4><br><br>
                    <p class="text-center">Publicando Dados ou Instabilidade Temporária no Provedor.</p><br>
                    <p class="text-center">Tente mais tarde!</p>
                </div>
            </div>
          </div>

        </div>
      </div>
    </section>