<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740" height="161">
    <tr>
      <td width="181" valign="middle" align="center" rowspan="8">
        <p align="center"><img border="0" src="<?php echo $this->base; ?>/images/ebaclogo.jpg" width="137" height="106">
        <p align="center"><font face="Arial" size="1">Administração de Condomínios</font></p>
        <p align="center"><font face="Arial" size="1"><b>www.ebac.com.br</b></font></td>
      <td width="398" height="24"><font face="Angsana New" size="3">Cedente</font></td>
      <td width="161" colspan="2" height="24"><font face="Angsana New" size="3">Data do Vencimento</font></td>
    </tr>
    <tr>
      <td width="398" height="21"><font face="Arial" size="2"><b><?php echo $this->Session->read('UnidadeAtual.Condominio.nome') . ' - ' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id') ); ?></b></font></td>
      <td width="161" colspan="2" bgcolor="#FFFF00" height="21" align="center"><p align="center"><b><font face="Arial" size="2"><?php echo $this->Formatacao->data( $boleto['Boleto']['vencimento'] ); ?></font></b></td>
    </tr>
    <tr>
      <td width="398" height="24"><font face="Angsana New" size="3">Ocupante</font></td>
      <td width="81" height="24"><font face="Angsana New" size="3">Entrega</font></td>
      <td width="80" height="24"><font face="Angsana New" size="3">Desconto</font></td>
    </tr>
    <tr>
      <td width="398" align="left" height="21"><font size="2" face="Arial"><?php echo $this->Session->read('UnidadeAtual.Ocupante.nome'); ?></font></td>
      <td width="81" align="left" height="21">
        <p align="center"><font face="Arial" size="2">Internet</font></p>
      </td>
      <td width="80" align="left" height="21">
        <p align="center"><font face="Arial" size="2"><?php echo $this->Formatacao->data( $boleto['Boleto']['limite_desconto'] ); ?></font></p>
      </td>
    </tr>
    <tr>
      <td width="398" height="24"><font size="3" face="Angsana New">Proprietário</font></td>
      <td width="81" height="24"><font face="Angsana New" size="3">Referência</font></td>
      <td width="80" height="24"><font face="Angsana New" size="3">Unidade</font></td>
    </tr>
    <tr>
      <td width="398" align="left" height="21"><font face="Arial" size="2"><?php echo $this->Session->read('UnidadeAtual.Proprietario.nome'); ?></font></td>
      <td width="81" valign="middle" height="21">
        <p><font face="Arial" size="2"><?php echo $boleto['Boleto']['referencia']; ?></font></p>
      </td>
     <td width="80" valign="middle" align="center" bgcolor="#FFFF00" height="21">
        <p><b><font face="Arial" size="2"><?php echo $this->Session->read('UnidadeAtual.Unidade.unidade'); ?></font></b></p>
      </td>
    </tr>
    <tr>
      <td width="398" height="24"><font face="Angsana New" size="3">Procurador
        / Imobiliária</font></td>
      <td width="161" colspan="2" height="24"><font face="Angsana New" size="3">Código da Guia
        / Folha</font></td>
    </tr>
    <tr>
      <td width="398" align="left" height="2"><font face="Arial" size="2"><?php echo $this->Session->read('UnidadeAtual.Imobiliaria.nome'); ?></font></td>
      <td width="161" valign="middle" align="left" colspan="2" height="2"><font face="Arial" size="2"><?php echo $boleto['Boleto']['nosso_numero'] . '-' . $boleto['Boleto']['digito_nn']; ?>&nbsp;&nbsp;&nbsp;1/1</font></td>
    </tr>
  </table>
</div>

  <table border="0" cellpadding="0" cellspacing="0" width="740">
    <tr>
      <td width="33" align="center">
        <p align="center"><font face="Arial" size="1">Código</font></p></td>
      <td width="129" align="center">
        <p align="center"><font face="Arial" size="1">Tipo de Lançamento</font></p></td>
      <td width="53" align="center">
        <p align="center"><font face="Arial" size="1">Referência</font></p></td>
      <td width="34" align="center">
        <p align="center"><font face="Arial" size="1">Moeda</font></p></td>
      <td width="60" align="center">
        <p align="center"><font face="Arial" size="1">Vencimento</font></p></td>
      <td width="88" align="center">
        <p align="center"><font face="Arial" size="1">Valor Principal</font></p></td>
      <td width="24" align="center">
        <p align="center"><font face="Arial" size="1">Dias</font></p></td>
      <td width="90" align="center">
        <p align="center"><font face="Arial" size="1">Principal Corrigido</font></p></td>
      <td width="100" align="center">
        <p align="center"><font face="Arial" size="1">Multa/Juros</font></p></td>
      <td width="109" align="center">
        <p align="center"><font face="Arial" size="1">Total do Lançamento</font></p></td>
    </tr>
<?php
    foreach ($boleto['Lancamento'] as $lancamento) {
?>
    <tr>
      <td width="33" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $lancamento['codigo']; ?></font></p></td>
      <td width="129" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $lancamento['lancamento']; ?></font></p></td>
      <td width="53" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $lancamento['referencia']; ?></font></p></td>
      <td width="34" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $lancamento['moeda']; ?></font></p></td>
      <td width="60" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $this->Formatacao->data($lancamento['vencimento']); ?></font></p></td>
      <td width="88" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $this->Formatacao->moeda($lancamento['principal']); ?></font></p></td>
      <td width="24" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $lancamento['atraso']; ?></font></p></td>
      <td width="90" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $this->Formatacao->moeda($lancamento['corrigido']); ?></font></p></td>
      <td width="100" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $this->Formatacao->moeda($lancamento['acrescimo']); ?></font></p></td>
      <td width="109" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"><?php echo $this->Formatacao->moeda($lancamento['total']); ?></font></p></td>
    </tr>
<?php
    }
?>
<?php
    for ($i = count($boleto['Lancamento']); $i < 24; $i++) {
?>
    <tr>
      <td width="33" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
      <td width="129" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
      <td width="53" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
      <td width="34" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
      <td width="60" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
      <td width="88" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
      <td width="24" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
      <td width="90" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
      <td width="100" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
      <td width="109" align="center" height="5">
        <p align="center" style="word-spacing: 0; line-height: 100%; margin-top: 0; margin-bottom: 0"><font face="Arial" size="1"></font></p></td>
    </tr>
<?php
    }
?>
  </table>
<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740">
    <tr>
      <td width="148" valign="bottom" align="center" height="14"><font face="Angsana New" size="2">Total dos Lançamentos</font></td>
      <td width="148" valign="bottom" align="center" height="14"><font face="Angsana New" size="2">Honorários</font></td>
      <td width="148" valign="bottom" align="center" height="14"><font face="Angsana New" size="2">Valor a pagar sem Desconto</font></td>
      <td width="148" valign="bottom" align="center" height="14"><font face="Angsana New" size="2">Desconto</font></td>
      <td width="148" valign="bottom" align="center" height="14"><font face="Angsana New" size="2">Valor a pagar com Desconto</font></td>
    </tr>
    <tr>
      <td width="148" align="center" ><font face="Arial"><font size="2"><b><?php echo $this->Formatacao->moeda( $boleto['Boleto']['total'] ); ?></b></font></font></td>
      <td width="148" align="center" ><font face="Arial"><font size="2"><b><?php echo $this->Formatacao->moeda( $boleto['Boleto']['honorario'] ); ?></b></font></font></td>
      <td width="148" align="center" ><font face="Arial"><font size="2"><b><?php echo $this->Formatacao->moeda( $boleto['Boleto']['total'] + $boleto['Boleto']['honorario'] ); ?></b></font></font></td>
      <td width="148" align="center" ><font face="Arial"><font size="2"><b><?php echo $this->Formatacao->moeda( $boleto['Boleto']['desconto'] ); ?></b></font></font></td>
      <td width="148" align="center" ><font face="Arial"><font size="2"><b><?php echo $this->Formatacao->moeda( ($boleto['Boleto']['total'] + $boleto['Boleto']['honorario']) - $boleto['Boleto']['desconto'] ); ?></b></font></font></td>    </tr>
  </table>
</div>
<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740" height="31">
    <tr>
      <td width="592" height="10" align="left">
        <p align="Left"><font face="Arial" size="1"><?php echo $this->Formatacao->mensagemBoleto( $boleto['Boleto']['mensagem'], $boleto['Boleto']['mensagem_2'] ); ?></font></p>
      </td>
      <td width="148" height="10">
        <p align="Left"><font color="#808080" size="1" face="Arial"></font></td>
    </tr>
    <tr>
      <td width="592" height="15" align="left">
        <p align="Left"><font face="Arial" size="1"><?php echo $boleto['Boleto']['mensagem_2']; ?></font></p>
      </td>
      <td width="148" height="15">
        <p align="Left"><font color="#808080" size="1" face="Arial"><?php echo date('d/m/Y - H:i:s'); ?></font></td>
    </tr>
  </table>
</div>
<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740">
    <tr>
      <td width="180">
        <font face="Arial" size="1">
        Lançamentos Vencidos</font></td>
      <td width="180">
        <font face="Arial" size="1">
        Lançamentos a vencer</font></td>
      <td width="560" rowspan="3" align="center">
        <p align="rigth"><font face="Arial" size="1">Autenticação Mecânica</font></td>
    </tr>
    <tr>
      <td width="180" align="Left"><font face="Arial" size="1"><?php echo $this->Formatacao->moeda( $boleto['Boleto']['vencido'] ); ?></font></td>
      <td width="180" align="Left"><font face="Arial" size="1"><?php echo $this->Formatacao->moeda( $boleto['Boleto']['vencer'] ); ?></font></td>
    </tr>
    <tr>
      <td width="516" height="15" colspan="2" rowspan="2">
        <p align="Left"><b><font face="Arial" size="1">Mensagem do Departamento Jurídico</font></b></td>
    </tr>
    <tr>
      <td width="560" valign="middle" rowspan="3">
        <p align="Left">&nbsp;</td>
    </tr>
    <tr>
      <td width="520" height="15" colspan="2">
        <p align="Left"><font face="Arial" size="1"><?php echo ($boleto['Boleto']['situacao']=='A') ? 'P R O C E S S O' : ( ($boleto['Boleto']['situacao']=='P') ? 'E M  P R O C E S S O' : 'Com ' . $boleto['Boleto']['atraso'] . ' dias de atraso, os lançamentos não pagos serão enviados'); ?></font></td>
    </tr>
    <tr>
      <td width="516" height="14" colspan="2">
        <p align="Left"><font face="Arial" size="1"><?php echo ($boleto['Boleto']['situacao']=='A') ? 'A J U I Z A D O' : ( ($boleto['Boleto']['situacao']=='P') ? 'D E  A J U I Z A M E N T O' : 'ao Departamento Jurídico, sendo acrescidos de honorários advocatícios.'); ?></font></td>
    </tr>
  </table>
</div>
<hr width="740" color="#FF0000" size="1" align="left">
<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740">
    <tr>
      <td width="289" height="25">
        <p align="left"><font face="Arial" size="2"><?php echo $boleto['Boleto']['banco']; ?></font></td>
      <td width="48" >
        <p align="Left"><font face="Arial" size="3"><?php echo $this->Formatacao->codigoBanco($boleto['Boleto']['linha_digitavel']); ?></font></td>
      <td width="402" align="left">
        <p align="Center"><font face="Arial" size="3"><?php echo $boleto['Boleto']['linha_digitavel']; ?></font></td>
    </tr>
  </table>
</div>
<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740" height="56">
    <tr>
      <td width="398" valign="middle" align="left" height="13"><font face="Arial" size="1">Local
        de Pagamento</font></td>
      <td width="182" valign="middle" align="left" height="13"><font face="Arial" size="1">Vencimento</font></td>
    </tr>
    <tr>
      <td width="560" height="15"><font face="Arial" size="1">Qualquer
        Banco até o vencimento. Após vencimento, nas agências <?php echo $boleto['Boleto']['banco']; ?></font></td>
      <td width="182" height="15">
        <p align="center"><font face="Arial" size="1"><?php echo $this->Formatacao->data( $boleto['Boleto']['vencimento'] ); ?></font></td>
    </tr>
    <tr>
      <td width="398" valign="middle" align="left" height="15"><font face="Arial" size="1">Cedente</font></td>
      <td width="182" valign="middle" align="left" height="15"><font face="Arial" size="1">Agência
        / Código do Cedente</font></td>
    </tr>
    <tr>
      <td width="560" height="13"><font face="Arial" size="1"><?php echo $this->Session->read('UnidadeAtual.Condominio.nome') . ' - ' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id') ); ?></font></td>
      <td width="182" align="right" height="13">
        <p align="center"><font face="Arial" size="1"><?php echo $boleto['Boleto']['cedente']; ?></font></td>
    </tr>
  <center>
  </table>
  </center>
</div>
<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740">
    <tr>
      <td width="150" valign="middle" align="left" height="10"><font face="Arial" size="1">Data
        do documento</font></td>
      <td width="85" valign="middle" align="left" height="10"><font face="Arial" size="1">Nº
        do documento</font></td>
      <td width="85" valign="middle" align="left" height="10"><font face="Arial" size="1">Espécie
        Docum.</font></td>
      <td width="85" valign="middle" align="left" height="10"><font face="Arial" size="1">Aceite</font></td>
      <td width="147" valign="middle" align="left" height="10"><font face="Arial" size="1">Data
        do processamento</font></td>
      <td width="183" valign="middle" align="left" height="10"><font face="Arial" size="1">Nosso
        Número</font></td>
    </tr>
    <tr>
      <td width="150" height="6">
        <p align="center"><font face="Arial" size="1"><?php echo date('d/m/Y'); ?></font></td>
      <td width="85" height="6">&nbsp;</td>
      <td width="85" height="6">&nbsp;</td>
      <td width="85" height="6">&nbsp;</td>
      <td width="147" height="6">
        <p align="center"><font face="Arial" size="1"><?php echo $this->Formatacao->data( $boleto['Boleto']['emissao'] ); ?></font></td>
      <td width="183" height="6">
        <p align="center"><font face="Arial" size="1"><?php echo $boleto['Boleto']['nosso_numero'] . '-' . $boleto['Boleto']['digito_nn']; ?></font></td>
    </tr>
    <tr>
      <td width="150" height="17"><font face="Arial" size="1">Uso
        do Banco</font></td>
      <td width="85" height="17"><font face="Arial" size="1">Carteira</font></td>
      <td width="85" height="17"><font face="Arial" size="1">Moeda</font></td>
      <td width="85" height="17"><font face="Arial" size="1">Quantidade</font></td>
      <td width="147" height="17"><font face="Arial" size="1">Valor</font></td>
      <td width="183" valign="middle" align="left" height="17"><font face="Arial" size="1">(=)
        Valor do Documento</font></td>
    </tr>
    <tr>
      <td width="150" height="3">
        <p align="center"><font face="Arial" size="1"><?php echo date('d/m/Y'); ?></font></td>
      <td width="85" height="3">
        <p align="center"><font face="Arial" size="1"><?php echo $boleto['Boleto']['carteira']; ?></font></td>
      <td width="85" height="3">
        <p align="center"><font face="Arial" size="1"><?php echo $boleto['Boleto']['moeda'] . "$"; ?></font></td>
      <td width="85" height="3">&nbsp;</td>
      <td width="147" height="3">
        <p align="center">&nbsp;</td>
      <td width="183" height="3">
        <p align="center"><font face="Arial" size="1"><?php echo $this->Formatacao->moeda( $boleto['Boleto']['total'] + $boleto['Boleto']['honorario'] ); ?></font></td>
    </tr>
  </table>
</div>

<?php $instrucoes   = $this->Formatacao->instrucaoBoleto($boleto['Boleto']); ?>

<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740">
    <tr>
      <td width="560" height="10">
        <p align="center"><font face="Arial" size="1">Instruções para Pagamento</font></td>
      <td width="180" valign="middle" align="left" height="10"><font face="Arial" size="1">(-)
        Desconto / Abatimento</font></td>
    </tr>
    <tr>
      <td width="560" height="10"><font face="Arial" size="1"><?php echo $instrucoes['wInstr11']; ?></font></td>
      <td width="180" height="10">
        <p align="center"><font face="Arial" size="1"><?php echo $instrucoes['wDescAba']; ?></font></td>
    </tr>
    <tr>
      <td width="560" height="10"><font face="Arial" size="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $instrucoes['wInstr12']; ?></font></td>
      <td width="180" valign="middle" align="left" height="10"><font face="Arial" size="1">(-)
        Outras Deduções</font></td>
    </tr>
    <tr>
      <td width="560" height="10"><font face="Arial" size="1"><?php echo $instrucoes['wInstr21']; ?></font></td>
      <td width="180" valign="middle" align="left" height="10"><font face="Arial" size="1">(+)
        Juros Mora / Multa</font></td>
    </tr>
    <tr>
      <td width="560" height="10"><font face="Arial" size="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $instrucoes['wInstr22']; ?></font></td>
      <td width="180" valign="middle" align="left" height="10"><font face="Arial" size="1">(+)
        Outros Acréscimos</font></td>
    </tr>
    <tr>
      <td width="560" height="14"><font face="Arial" size="1"><?php echo $instrucoes['wInstr31']; ?></font></td>
      <td width="180" valign="middle" align="left" height="14"><font face="Arial" size="1">(=)
        Valor Cobrado</font></td>
    </tr>
    <tr>
      <td width="560" height="10"><font face="Arial" size="1"><?php echo $instrucoes['wInstr41']; ?></font></td>
      <td width="180" height="10">
        <p align="center"><font face="Arial" size="1"><?php echo $instrucoes['wValCobr']; ?></font></td>
    </tr>
  </table>
</div>

<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740">
    <tr>
      <td width="69" align="center" height="10"><font face="Arial" size="1">Unidade</font></td>
      <td width="177" align="center" height="10">
        <p align="left"><font face="Arial" size="1">Sacado</font> </td>
      <td width="179" rowspan="2" align="center" valign="middle" height="28"><font face="Arial" size="1">Ficha
        de Compensação</font></td>
    </tr>
    <tr>
      <td width="69" align="center" bgcolor="#FFFF00" height="15"><b><font face="Arial" size="2"><?php echo $this->Session->read('UnidadeAtual.Unidade.unidade'); ?></font></b></td>
      <td width="484" height="15"><font face="Arial" size="2"><?php echo $this->Formatacao->getSacado($this->Session->read('UnidadeAtual')); ?></font></td>
    </tr>
  </table>
</div>
<div align="left">
  <table border="0" cellpadding="0" cellspacing="0" width="740">
    <tr>
      <td width="558">
        <p align="right"></td>
      <td width="180" height="6">
        <p align="center"><font face="Arial" size="1">Autenticação Mecânica</font></td>
    </tr>
  </table>
</div>

<?php $this->Formatacao->barcode($boleto['Boleto']['linha_digitavel']); ?>