<section class="section-top-20 section-sm-top-20">
  <div class="shell text-center text-sm-left">
    <ul class="range list-icons-variant-1 offset-top-40">
      <li class="cell-sm-12 cell-lg-12">
        <div class="unit unit-sm-horizontal unit-spacing-sm">
          <div class="unit-left"><span class="icon icon-sm-variant-2 icon-circle-md icon-white icon-primary-filled mdi fa-check-square-o"></span></div>
          <div class="unit-body">
            <h6>Emitir Nada-Consta</h6>
            <p>A declaração de pagamentos (Nada-Consta) estará disponível nas seguintes condições:</p>
            <ul class="list-numbered">
                <li>Não existem taxas em aberto até a data ;</li>
                <li>Não existem Acordos Administrativos / Judiciais a vencer ; e</li>
                <li>Não existem Ações Judiciais de Cobrança vinculadas às taxas.</li>
            </ul>
            <br>
            <em>Haverá ressalvas de cheques a compensar para pagamentos realizados nos 2 últimos dias.</em>
            <br><br>
            <?php if($exibirNadaConsta) { ?>
            <center><a target="_blank" href="<?php echo $this->base; ?>/unidades/declaracao"><div class="btn btn-sm btn-primary">Gerar Declaração</div></a></center>
            <?php } ?>
          </div>
        </div>
      </li>
    </ul>
  </div>
</section>