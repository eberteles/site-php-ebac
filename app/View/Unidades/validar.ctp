<?php echo $this->Form->create('Chave'); ?>
<section class="section-top-20 section-sm-top-20">
  <div class="shell text-center text-sm-left">
    <ul class="range list-icons-variant-1 offset-top-40">
      <li class="cell-sm-12 cell-lg-12">
        <div class="unit unit-sm-horizontal unit-spacing-sm">
          <div class="unit-left"><span class="icon icon-sm-variant-2 icon-circle-md icon-white icon-primary-filled mdi fa-check-square-o"></span></div>
          <div class="unit-body">
            <h6>Validar Nada-Consta</h6><br>
            <div class="form-group">
              <?php echo $this->Form->input('id', array('type'=>'text','id'=>'chave-validar', 'data-constraints'=>'@Required', 'class'=>'form-control chave', 'label'=>'Informe o Código de Validação')); ?>
            </div>
            <div class="group group-middle offset-top-30 text-center text-xs-left">
              <button type="submit" name="cmdAcessar" value="Acessar" class="btn btn-primary">Validar a Declaração</button>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</section>