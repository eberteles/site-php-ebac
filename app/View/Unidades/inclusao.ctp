    <section>
      <div class="shell">
        <div class="range">
          <div class="col-sm-12 col-md-12" id="div-form">
            <div class="offset-top-50">
              <!-- RD Mailform-->
              <form data-form-output="form-output-global" data-form-type="inclusao-cadastral" method="post" action="<?php echo $this->base; ?>/envia/inclusao_cadastral" class="rd-mailform rd-mailform-mod-1">
                <div class="range">

                  <div class="cell-sm-12">
                      <a href="#" id="instrucoes-importantes" data-toggle="tooltip" title="Instruções Importantes"><span class="icon icon-sm-variant-2 icon-primary fa-question-circle"></span></a>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="contact-condominio" class="form-label-outside">Condomínio</label>
                      <input id="contact-condominio" type="text" name="txtNomeCon" data-constraints="@Required" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="contact-unidade" class="form-label-outside">Unidade</label>
                      <input id="contact-unidade" type="text" name="txtUnidade" data-constraints="@Required" class="form-control">
                    </div>
                  </div>

                  <div class="cell-sm-12 offset-top-18">
                      <h6>Dados do Ocupante</h6>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="contact-ocupante" class="form-label-outside">Nome do Ocupante</label>
                      <input id="contact-ocupante" type="text" name="txtOcupante" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="contact-email-ocupante" class="form-label-outside">E-mail do Ocupante</label>
                      <input id="contact-email-ocupante" type="email" name="txtEmailOcup" class="form-control">
                    </div>
                  </div>

                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-cpf-ocupante" class="form-label-outside">Cpf/Cnpj</label>
                      <input id="contact-cpf-ocupante" type="text" name="txtCpfCnpjOcup" class="form-control cpf-cnpj">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-dt-ocupante" class="form-label-outside">Data de Nascimento</label>
                      <input id="contact-dt-ocupante" type="text" name="txtNascOcup" class="form-control data-pt">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-tel-ocupante" class="form-label-outside">Telefone</label>
                      <input id="contact-tel-ocupante" type="text" name="txtFoneOcup" class="form-control telefone">
                    </div>
                  </div>

                  <div class="cell-sm-12 offset-top-18">
                      <h6>Dados do Proprietário</h6>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="contact-proprietario" class="form-label-outside">Nome do Proprietário</label>
                      <input id="contact-proprietario" type="text" name="txtProprietario" data-constraints="@Required" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="contact-email-proprietario" class="form-label-outside">E-mail do Proprietário</label>
                      <input id="contact-email-proprietario" type="email" name="txtEmailProp" class="form-control">
                    </div>
                  </div>

                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-cpf-proprietario" class="form-label-outside">Cpf/Cnpj</label>
                      <input id="contact-cpf-proprietario" type="text" name="txtCpfCnpjProp" data-constraints="@Required" class="form-control cpf-cnpj">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-dt-proprietario" class="form-label-outside">Data de Nascimento</label>
                      <input id="contact-dt-proprietario" type="text" name="txtNascProp" data-constraints="@Required" class="form-control data-pt">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-tel1-proprietario" class="form-label-outside">Telefone 1</label>
                      <input id="contact-tel1-proprietario" type="text" name="txtFoneProp1" data-constraints="@Required" class="form-control telefone">
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18">
                    <div class="form-group">
                      <label for="contact-tel2-proprietario" class="form-label-outside">Telefone 2</label>
                      <input id="contact-tel2-proprietario" type="text" name="txtFoneProp2" data-constraints="@Required" class="form-control telefone">
                    </div>
                  </div>

<!--                  <div class="cell-sm-12 offset-top-18">
                      <h6>Dados da Imobiliária</h6>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="contact-imobiliaria" class="form-label-outside">Nome da Imobiliária</label>
                      <input id="contact-imobiliaria" type="text" name="txtImobiliaria" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="contact-tel-imobiliaria" class="form-label-outside">Telefone</label>
                      <input id="contact-tel-imobiliaria" type="text" name="txtFoneImob" class="form-control telefone">
                    </div>
                  </div>-->

                  <div class="cell-sm-12 offset-top-18">
                      <h6>Responsável pela Inclusão</h6>
                      <hr>
                  </div>

                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="email-remetente" class="form-label-outside">E-Mail do Remetente</label>
                      <input id="email-remetente" type="text" name="txtEmail" data-constraints="@Email" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="email-remetente-confirm" class="form-label-outside">Confirmação do Email</label>
                      <input id="email-remetente-confirm" type="text" name="txtConfirmaEmail" data-constraints="@Email" class="form-control">
                    </div>
                  </div>

                  <div class="cell-xs-12 offset-top-30">
                    <button type="submit" class="btn btn-primary">Enviar Cadastral</button>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <div class="col-sm-12 col-md-9 hidden" id="div-form-sucesso">
<!--            <h5>Alteração Cadastral</h5>
            <hr>-->
            <div class="offset-top-50">
                <div class="grid-element">
                    <h4 class="text-center">Confirmação</h4><br><br>
                    <p class="text-center">Os dados foram enviados para a Ebac.</p><br>
                    <p class="text-center">Aguarde a resposta em até 2 dias úteis</p>
                </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-9 hidden" id="div-form-error">
<!--            <h5>Alteração Cadastral</h5>
            <hr>-->
            <div class="offset-top-50">
                <div class="grid-element">
                    <h4 class="text-center">Não foi possível enviar sua Mensagem</h4><br><br>
                    <p class="text-center">Publicando Dados ou Instabilidade Temporária no Provedor.</p><br>
                    <p class="text-center">Tente mais tarde!</p>
                </div>
            </div>
          </div>

        </div>
      </div>
    </section>


<script>

      bootbox.alert({
          title: '<h5><center>Instruções Importantes</center></h5>',
          message: '1.Esta ficha deve ser utilizada para Inclusão de Dados Cadastrais (por exemplo, CPF ou CNPJ).<br>2.	Para Alteração de Dados já cadastrados, deve-se efetuar o Login, acessando Minha Unidade / Cadastro.<br>3.	Esta ficha deve ser preenchida, preferencialmente, pelo proprietário ou por quem detém todos os dados, seja por procuração, contrato de aluguel ou documento de propriedade.<br>4. Caso haja alteração do proprietário (titularidade do imóvel) um comprovante deve ser enviado para o Departamento Administrativo da Ebac, através do malote,  pelo correio no endereço QRSW 05 CL 01 Loja 03 - Sudoeste - Brasília - DF - Cep: 70675-550 ou por email para: sistemas@ebac.com.br.<br>&nbsp;&nbsp;&nbsp;São comprovantes de Propriedade:<br>&nbsp;&nbsp;&nbsp;. Escritura do Imóvel<br>&nbsp;&nbsp;&nbsp;. Promessa de Compra e Venda do Imóvel<br>&nbsp;&nbsp;&nbsp;. Certidão de Ônus ou Ficha Dominial do Cartório de Registro de Imóveis<br>5.	Preencha todos os campos obrigatórios para enviar o cadastramento.<br>6.	Os dados serão confirmados antes do cadastramento na base de dados da Ebac. <br>7.	Um e-mail será enviado confirmando o cadastramento em até 2 dias úteis.<br>',
          closeButton: false
      });

</script>