<table border="1" cellpadding="4" cellspacing="1" width="100%" bordercolor="#C0C0C0">
  <tr>
    <td height="121" align="center">
        <img border="0" src="<?php echo $this->base; ?>/images/ebaclogo.jpg" width="131" height="111"></td>
    <td height="121" align="center">
        <font face="Verdana" size="5"><b>Administração de Condomínios</b></font><br><br>
        <b><font face="Verdana" size="2">www.ebac.com.br</font></b>
    </td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10">
  <tr>
     <td align="center" height="1"></td>
  </tr>
</table>

<table border="1" cellpadding="8" cellspacing="1" width="100%" height="50" bordercolor="#C0C0C0">
  <tr>
    <td align="center"><b><font face="Verdana" size="4" color="#000000">Declaração de Pagamentos</font></b>
    </td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10">
  <tr>
     <td align="center" height="1"></td>
  </tr>
</table>

<table border="1" cellpadding="8" cellspacing="1" width="100%" height="50" bordercolor="#C0C0C0">
  <tr>
    <td align="center"><b><font face="Verdana" size="3" color="#000000"><?php echo Set::classicExtract($unidade, 'Condominio.nome') . ' (' . sprintf("%03d", Set::classicExtract($unidade, 'Condominio.id') ) . ')'; ?></font></b>
    </td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10">
  <tr>
     <td align="center" height="1"></td>
  </tr>
</table>

<table border="1" cellpadding="8" cellspacing="1" width="100%" height="50" bordercolor="#C0C0C0">
    <tr>
    <td colspan="2" align="center">
      <p><b><font face="Verdana" color="#000000" size="4">Unidade: <?php echo Set::classicExtract($unidade, 'Unidade.unidade'); ?></font></b></p>
    </td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10">
  <tr>
     <td align="center" height="1"></td>
  </tr>
</table>
  
<table width="100%" bordercolor="#C0C0C0" border="1" cellspacing="1" cellpadding="8">
  <tr>
    <td height="21" width="250">
      <p align="left"><font face="Verdana" size="2">00.<b> CNPJ do Condomínio:</b></font></td>
    <td height="21"><font face="Verdana" size="2">&nbsp;<?php echo $this->Formatacao->documento(Set::classicExtract($unidade, 'Condominio.cnpj')); ?></font></td>
  </tr>
  <tr>
    <td height="21" width="250">
      <p align="left"><font face="Verdana" size="2">01.<b> Endereço:</b></font></td>
    <td height="21"><font face="Verdana" size="2">&nbsp;<?php echo Set::classicExtract($unidade, 'Condominio.endereco') . ' - ' . Set::classicExtract($unidade, 'Condominio.bairro'); ?></font></td>
  </tr>
  <tr>
    <td height="21" width="250">
      <p align="left"><font face="Verdana" size="2">02.<b> Complemento:</b></font></td>
    <td height="21"><font face="Verdana" size="2">&nbsp;<?php echo Set::classicExtract($unidade, 'Condominio.cidade') . '-' . Set::classicExtract($unidade, 'Condominio.uf') . ' Cep: ' . $this->Formatacao->cep(Set::classicExtract($unidade, 'Condominio.cep')); ?> </font></td>
  </tr>
  <tr>
    <td height="21" width="250">
      <p align="left"><font face="Verdana" size="2">03.<b> Síndic<?php echo (Set::classicExtract($unidade, 'Condominio.Sindico.sexo') == 'M') ? 'o' : 'a'; ?>:</b></font></td>
    <td height="21"><font face="Verdana" size="2">&nbsp;<?php echo Set::classicExtract($unidade, 'Condominio.Sindico.nome') . ' (' . Set::classicExtract($unidade, 'Condominio.Sindico.unidade') . ')'; ?></font></td>
  </tr>
  <tr>
    <td height="21" width="250">
      <p align="left"><font face="Verdana" size="2">04.<b>  CPF d<?php echo (Set::classicExtract($unidade, 'Condominio.Sindico.sexo') == 'M') ? 'o' : 'a'; ?> Síndic<?php echo (Set::classicExtract($unidade, 'Condominio.Sindico.sexo') == 'M') ? 'o' : 'a'; ?>:</b></font></td>
    <td height="21"><font face="Verdana" size="2">&nbsp;<?php echo $this->Formatacao->documento(Set::classicExtract($unidade, 'Condominio.Sindico.documento')); ?> </font></td>
  </tr>
  <tr>
    <td height="21" width="250"><font face="Verdana" size="2">05.<b>
      Último Pagamento:</b></font></td>
    <td height="21"><font face="Verdana" size="2">&nbsp;
        <?php echo $this->Formatacao->data(Set::classicExtract($unidade, 'Unidade.ultimo_pgto')) . ( ( Set::classicExtract($unidade, 'Unidade.cheque_aberto') == 'S' ) ? '  (Com ressalvas de possíveis cheques a compensar)' : '' ); ?> </font>
    </td>
  </tr>
  <tr>
    <td height="21" width="250"><font face="Verdana" size="2">06.<b>
      Taxas em Aberto:</b></font></td>
    <td height="21">
        <font face="Verdana" size="2">&nbsp;<?php echo (Set::classicExtract($unidade, 'Unidade.taxas_nada_consta') > 0) ? Set::classicExtract($unidade, 'Unidade.taxas_nada_consta') : 'Nada-Consta'; ?> </font>
    </td>
  </tr>
  <tr>
    <td height="21" width="250"><font face="Verdana" size="2">07.<b>
      Processos Judiciais:</b></font></td>
    <td height="21">
        <font face="Verdana" size="2">&nbsp;<?php echo (Set::classicExtract($unidade, 'Unidade.ajuizada') == 'S') ? 'Existem Processos Judiciais para Cobrança de Taxas.' : 'Nada-Consta'; ?> </font>
    </td>
  </tr>
  <tr>
    <td height="21" width="250"><font face="Verdana" size="2">08.<b>
      Acordo Administrativo:</b></font></td>
    <td height="21">
        <font face="Verdana" size="2">&nbsp;<?php echo (Set::classicExtract($unidade, 'Unidade.acordo_administrativo') > 0) ? Set::classicExtract($unidade, 'Unidade.acordo_administrativo') : 'Nada-Consta'; ?> </font>
    </td>
  </tr>
  <tr>
    <td height="21" width="250"><font face="Verdana" size="2">09.<b> Acordo Judicial:</b></font></td>
    <td height="21">
        <font face="Verdana" size="2">&nbsp;<?php echo (Set::classicExtract($unidade, 'Unidade.acordor_judicial') > 0) ? Set::classicExtract($unidade, 'Unidade.acordor_judicial') : 'Nada-Consta'; ?> </font>
    </td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10">
  <tr>
     <td align="center" height="1"></td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10" bordercolor="#C0C0C0" border="1" cellpadding="8">
  <tr>
    <td height="40" colspan="2" align="center">
        <font face="Verdana" size="3"><b class="<?php echo $corSituacao; ?>">Posição em <?php echo $this->Formatacao->data(Set::classicExtract($unidade, 'Unidade.carga')); ?>:&nbsp; <?php echo $situacao; ?></b></font></td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10">
  <tr>
     <td align="center" height="1"></td>
  </tr>
</table>
 
<table width="100%" height="300">
 <tr>
    <td height="21"></td>
  </tr>
  <tr>
    <td height="21" align="center">
        <font face="Verdana" size="2">Brasília-DF, <?php echo strftime("%d de %B de %Y (%A)", strtotime(Set::classicExtract($unidade, 'Unidade.carga'))); ?></font>
    </td>
  </tr>
  <tr>
      <td height="21">&nbsp;</td>
  </tr>
  <tr>
      <td height="21">&nbsp;</td>
  </tr>
  <tr>
      <td height="21">&nbsp;</td>
  </tr>
  <tr>
      <td height="21">&nbsp;</td>
  </tr>
  <tr>
    <td height="21" align="center">___________________________________________</td>
  </tr>
  <tr>
    <td height="21" align="center"><font face="Verdana" size="2"><?php echo Set::classicExtract($unidade, 'Condominio.Sindico.nome'); ?></font></td>
  </tr>
  <tr>
    <td height="21"></td>
  </tr>
  <tr>
    <td height="21"></td>
  </tr>
  <tr>
    <td height="21" align="center">
        <font face="Verdana" size="2">Dispensados a assinatura e reconhecimento de firma d<?php echo (Set::classicExtract($unidade, 'Condominio.Sindico.sexo') == 'M') ? 'o' : 'a'; ?> Síndic<?php echo (Set::classicExtract($unidade, 'Condominio.Sindico.sexo') == 'M') ? 'o' : 'a'; ?> se autenticada pela</font>
    </td>
  </tr>
  <tr>
    <td height="23" align="center">
        <font face="Verdana" size="2">Internet no endereço: <i><font color="#000000"><b>www.ebac.com.br.</b></font></i> Código: <b><?php echo $chave; ?></b></font>
    </td>
  </tr>
  <tr>
    <td height="1">
    </td>
  </tr>
  <tr>
    <td height="1" align="center">
        <font face="Verdana" size="1" color="#808080"><?php echo $emissor; ?> em <?php echo date('d/m/Y - H:i:s'); ?></font>
    </td>
  </tr>
</table>