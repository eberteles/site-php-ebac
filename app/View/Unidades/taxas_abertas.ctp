<br><br>

<table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th>Quantidade de Taxas</th>
            <th>Situação</th>
            <th>Valor em Aberto</th>
        </tr>
      </thead>
      <?php if( $this->Session->read('UnidadeAtual.Unidade.taxas_abertas') > 0 || $this->Session->read('UnidadeAtual.Unidade.acordo_administrativo') > 0 || $this->Session->read('UnidadeAtual.Unidade.acordor_judicial') > 0 || $this->Session->read('UnidadeAtual.Unidade.ajuizada') == 'S' ) { ?>
      <tbody>
        <?php
            echo '<tr>';
            echo '<td>' . $this->Session->read('UnidadeAtual.Unidade.taxas_abertas') . '</td>';
            echo '<td>' . $this->Formatacao->situacaoUnidade($this->Session->read('UnidadeAtual.Unidade.acordo_administrativo'), $this->Session->read('UnidadeAtual.Unidade.acordor_judicial'), $this->Session->read('UnidadeAtual.Unidade.ajuizada') ) . '</td>';
            echo '<td>' . $this->Formatacao->moeda($this->Session->read('UnidadeAtual.Unidade.divida')) . '</td>';
            echo '</tr>';
        ?>
      </tbody>
      <?php } else { ?>
      <tbody>
        <tr>
            <td colspan="3">Nenhuma Taxa em Aberto</td>
        </tr>
      </tbody>
      <?php } ?>
    </table>

