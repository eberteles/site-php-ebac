<section class="section-top-20 section-sm-top-20">
  <div class="shell text-center text-sm-left">
    <ul class="range list-icons-variant-1 offset-top-40">
      <li class="cell-sm-12 cell-lg-12">
        <div class="unit unit-sm-horizontal unit-spacing-sm">
          <div class="unit-left"><span class="icon icon-sm icon-circle-md icon-white icon-primary-filled mdi fa-barcode"></span></div>
          <div class="unit-body">
            <h6>Boleto de Cobrança</h6>
            <p>As guias de Cobrança são geradas mensalmente e disponibilizadas na Internet. <br>
                Com isso, é possível emitir a guia pelo site da Ebac antes mesmo de recebê-la via correio.</p>
            <p><b>Próxima Emissão Prevista para: <?php echo $this->Formatacao->data($this->Session->read('UnidadeAtual.Condominio.previsao_guia')); ?></b></p>
            <br><br>
            <?php if($exibirGuia) { ?>
            <center><a target="_blank" href="<?php echo $this->base; ?>/unidades/guia"><div class="btn btn-sm btn-primary">Gerar Guia</div></a></center>
            <?php } ?>
          </div>
        </div>
      </li>
    </ul>
  </div>
</section>