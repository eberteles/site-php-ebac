        <section class="section-top-40 section-sm-top-40">
          <div class="shell">
            <div class="range">
              <div class="cell-lg-12">
                <h5>Assuntos Cadastrados</h5>
                <hr>
                <p class="offset-top-18">As dúvidas que nos são enviadas são pesquisadas, respondidas e transformadas em assuntos, formando um banco de dados para consulta gratuita dos interessados. <br>Estão disponíveis também, vários modelos de formulários, termos de ocupação de área comum, contratos, editais, etc.</p>
                <br>
                <!-- Responsive-tabs-->
                <div data-type="accordion" class="responsive-tabs responsive-tabs-horizontal">
                  <ul class="resp-tabs-list">
                    <li>Administração / Administradora</li>
                    <li>Animais Domésticos</li>
                    <li>Áreas Comuns do Condomínio</li>
                    <li>Assembléia Geral Ordinária / Extraordinária</li>
                    <li>Conselho Consultivo e Fiscal</li>
                    <li>Convenção do Condomínio</li>
                    <li>Depósito em conta para pagamento de taxa</li>
                    <li>Desconto na Taxa de Condomínio</li>
                    <li>Edital de Convocação</li>
                    <li>Elevador Social e Serviço</li>
                    <li>Empregados Diretos e Indiretos</li>
                    <li>Equipamentos contra incêndio</li>
                    <li>Fração Ideal</li>
                    <li>Multas / Juros / Correção Monetária</li>
                    <li>Regimento / Regulamento Interno</li>
                    <li>Seguros Obrigatórios</li>
                    <li>Sindicatos / Contribuição</li>
                    <li>Síndico (a) / Subsíndico (a)</li>
                    <li>Taxas de Condomínio / Taxa Extra</li>
                    <li>Vagas de Garagem</li>
                  </ul>
                  <div class="resp-tabs-container">
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Por definição legal, a administração de um condomínio é de exclusiva responsabilidade do Síndico, o qual será assessorado pelo Conselho Consultivo  e fiscalizado pelo Conselho Fiscal, podendo ter um Subsíndico que irá lhe substituir na sua ausência.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dependendo da complexidade de cada Condomínio, cabe ao Síndico dividir suas tarefas ou mesmo, delegá-las à pessoas físicas e/ou jurídicas capazes de assessorá-lo nas diversas áreas: administrativa, financeira, trabalhista, jurídica, contábil, etc.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As administradoras de Condomínio têm prestado grandes serviços à comunidade condominial, contudo, a escolha de uma administradora merece necessária atenção e criteriosa seleção dos serviços que ela pode oferecer. Para uma boa escolha, alguns itens valem ser verificados, dentre outros:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>empresa registrada no Conselho Regional de Administração (C.R.A.);</li>
                            <li>profissionais registrados nos respectivos Conselhos Regionais na área contábil(CRC), administrativa (CRA), e jurídica (OAB);</li>
                            <li>estrutura de sistemas para movimentação financeira somente na rede bancária, evitando qualquer tipo de manuseio de dinheiro na Administradora ou Condomínio;</li>
                            <li>estrutura física operacional adequada para o número de clientes;</li>
                            <li>quantidade e qualidade de equipamentos para a realização dos serviços;</li>
                            <li>pessoal qualificado e treinado para a realização dos diversos serviços;</li>
                            <li>nível profissional de atuação perante aos bancos e fornecedores dos condomínios;</li>
                            <li>qualidade e pontualidade na apresentação dos serviços ao condomínio;</li>
                            <li>responsabilidade total sobre os serviços executados, principalmente sobre os passivos trabalhistas de empregados com contratação direta e pagamento das contas fixas do condomínio;</li>
                            <li>tempo de experiência no mercado e referência dos condomínios administrados.</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="animated fadeIn">
                        <p>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Os animais domésticos têm sido uma grande preocupação dos condomínios, visto que, o número muito grande de animais domésticos e a falta de cuidado dos proprietários têm causado acidentes e prejudicado a higiene da comunidade.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Escritura de Convenção do Condomínio e/ou o Regimento Interno, podem estabelecer as normas de aceitação de animais domésticos, ou mesmo, proibi-los nas dependências comuns, bem como, nos elevadores (mesmo que seja o elevador de serviço).<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Na maioria dos Condomínios, os animais domésticos são tolerados desde que, não prejudiquem a saúde, higiene e tranqüilidade dos demais moradores.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vários condomínios estão estabelecendo horário para o trânsito de cachorros nas dependências comuns, como forma de evitar o contato físico com os moradores.
                        </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Áreas comuns são espaços físicos de propriedade de todos os Condôminos, definidas na Escritura de Convenção do Condomínio, conforme a fração ideal de cada unidade autônoma.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;São exemplos de áreas comuns, dentre outras:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>halls de entrada e de elevadores;</li>
                            <li>terraço ou cobertura coletiva;</li>
                            <li>áreas de circulação coletiva (andares, garagens etc.);</li>
                            <li>escadas, sala de máquinas, pc's , banheiros coletivos;</li>
                            <li>playground, áreas de lazer coletivo;</li>
                            <li>áreas verdes de propriedade do condomínio;</li>
                            <li>vagas de garagem de propriedade do condomínio.</li>
                          </ul>
                        </div>
                      </div>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Qualquer utilização exclusiva de áreas comuns deve ser disciplinada pela Assembléia Geral dos Condôminos, regularmente convocada para esse fim específico ou conforme estabelecido na Escritura de Convenção do Condomínio e/ou Regimento Interno.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Caso o Condomínio optar em ceder áreas comuns (garagens, halls para colocação de mesas e cadeiras, espaço para cartazes, etc.) deverá firmar um Termo de Ocupação de Área Comum, onde constarão normas e definições. Não é aconselhada a utilização da expressão "Aluguel", visto que, não se está alugando e sim cedendo a ocupação de uma coisa comum sob as normas e leis do Condomínio.<br>
                      </p>
                      <div class="group-xl">
                          <a class="btn btn-icon btn-icon-left btn-primary btn-no-shadow" href="<?php echo $this->base; ?>/files/modelos/modelo_termo_ocupacao.doc"><span class="icon icon-xs fa-arrow-circle-o-down"></span>MODELO DE TERMO DE OCUPAÇÃO</a>
                      </div>
                    </div>
                    <div class="animated fadeIn">
                      <h6>&nbsp;&nbsp;&nbsp;&nbsp;Assembléia Geral</h6>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Assembléia Geral de um Condomínio é realizada para a discussão e deliberação de assuntos não previstos na legislação condominial ou para normatizar o cumprimento do que já está previsto, sempre precedida de um Edital de Convocação.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Das decisões da Assembléia será lavrada uma ata que discorrerá sobre os principais assuntos discutidos e todos aqueles votados, assinada pelo Secretário e Presidente. O livro de atas ficará em poder do Síndico do Condomínio durante todo o seu mandato.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As atas poderão ser impressas e coladas no livro evitando sua transcrição manual. Contudo, as atas de maior importância (eleição de síndico e membros da administração, definição de obras de grande vulto, multas impostas a moradores e condôminos) deverão ser registradas no Cartório de Registro de Títulos e Documentos para que possam estar públicas e disponíveis no caso de extravio do livro de atas. As assembléias podem ser:<br>
                      </p>
                      <h6>&nbsp;&nbsp;&nbsp;&nbsp;Assembléia Ordinária</h6>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Conforme definição legal, todo Condomínio deverá estabelecer uma data para a Assembléia Geral Ordinária (convocada pelo Síndico), a qual ocorrerá uma vez por ano e tem como objetivos principais:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>prestação de contas do exercício anterior (mandato atual);</li>
                            <li>eleição de Síndico, Subsíndico e membros dos Conselhos Consultivo e Fiscal;</li>
                            <li>definição do valor do rateio de despesas ordinárias do Condomínio ou previsão para o ano seguinte;</li>
                            <li>deliberação sobre os recursos interpostos contra ações do Síndico;</li>
                            <li>ratificação ou retificação de multas impostas pelo Síndico à Condôminos que descumpriram as normas do Condomínio.</li>
                          </ul>
                        </div>
                      </div>
                      <h6>&nbsp;&nbsp;&nbsp;&nbsp;Assembléia Extraordinária</h6>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Assembléia Geral Extraordinária, como o nome diz, é convocada para tratar de assuntos de ordem emergencial ou aqueles que necessitam de decisão da coletividade e não foram previstos anteriormente. A AGE poderá ser convocada pelo Síndico ou pelos Condôminos que representam um percentual definido na Escritura de Convenção (geralmente 1/4) e tratará sobre os seguintes assuntos, dentre outros:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>destituição do Síndico, Subsíndico ou Membros dos Conselhos;</li>
                            <li>renúncia do Síndico, Subsíndico ou Membros dos Conselhos;</li>
                            <li>eleição de Síndico, Subsíndico ou Membro dos Conselhos para assumirem "mandato tampão"  (até a próxima Assembléia Geral Ordinária);</li>
                            <li>fixação da taxa de condomínio mensal, caso não haja previsão anual;</li>
                            <li>definição de taxa complementar para suprimento de caixa;</li>
                            <li>definição de obras a serem realizadas no Condomínio e respectiva taxa extraordinária;</li>
                            <li>discussão e deliberação sobre assuntos diversos de caráter comum.</li>
                          </ul>
                        </div>
                      </div>
                      <div class="group-xl">
                          <a class="btn btn-icon btn-icon-left btn-primary btn-no-shadow" href="<?php echo $this->base; ?>/files/modelos/modelo_ata_assembleia.doc"><span class="icon icon-xs fa-arrow-circle-o-down"></span>MODELO DE ATA DA ASSEMBLÉIA</a>
                      </div>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O artigo 23 da Lei 4.591/64 - Será eleito, na forma prevista na Convenção, um Conselho Consultivo, constituído de três condôminos, com mandatos que não poderão exceder de 2 anos, permitida a reeleição.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Funcionará o Conselho como órgão consultivo do Síndico, para assessorá-lo na solução de problemas que digam respeito ao Condomínio, podendo a Convenção definir suas atribuições específicas.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A legislação não define o Conselho Fiscal, contudo não proíbe sua definição pela Escritura de Convenção. Neste caso, as Convenções poderão criar Conselhos Consultivo e Fiscal e definir atribuições para eles.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Se a Convenção somente definir o Conselho Consultivo, o mesmo poderá assumir papel fiscalizador do Síndico, como por exemplo, conferência mensal das contas do Condomínio para posterior encaminhamento de parecer à Assembléia Geral.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;É totalmente equivocada a atribuição de aprovação de contas ao Conselho Consultivo e/ou Fiscal. Pela legislação em vigor, quem aprova contas é a Assembléia Geral dos Condôminos. É praxe, contudo, que o Conselho avalie as contas e emita um parecer para a Assembléia.<br>
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A escritura de convenção de um condomínio é constituída de normas e regras com o objetivo de regular o bem comum e a convivência em coletividade. A convenção deve ser registrada no Cartório de Registro de Imóveis para que produza todos os efeitos necessários definidos na Lei.<br>
                      </p>
                      <div class="group-xl">
                          <a class="btn btn-icon btn-icon-left btn-primary btn-no-shadow" href="<?php echo $this->base; ?>/files/modelos/modelo_escritura_convencao.doc"><span class="icon icon-xs fa-arrow-circle-o-down"></span>MODELO DE ESCRITURA CONVENÇÃO</a>
                      </div>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O depósito em conta corrente ou transferência eletrônica não têm caráter de pagamento, podendo ser estornado, a qualquer momento, por simples solicitação do autor ao banco mediante a apresentação da via original (já ocorreu emissão de nada-consta e posterior estorno do pagamento).<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Quando é realizado o depósito, o banco não fornece nenhuma informação do autor, e a única forma de identificar é pela manifestação do mesmo. Isto torna necessário  fechar o demonstrativo mensal com lançamentos de Crédito não identificado.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A maioria das taxas de Condomínio tem o mesmo valor e, quando existem muitos depósitos no mesmo dia, corre-se o risco de estar baixando taxas com documentos incorretos, já que nunca se tem em mãos todos os documentos ao mesmo tempo.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Portanto, o objetivo é conscientizar o Condômino a realizar o pagamento através da guia de cobrança que é mais segura para ele (comprovante de pagamento) e para o Condomínio (crédito identificado). O pagamento através da guia de cobrança somente poderá ser estornado pelo autor no mesmo dia; após o dia, o estorno só poderá ser feito com autorização do Condomínio.<br>
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A cada dia, mais Condomínios implantam o desconto na taxa visando garantir um rateio mais justo entre os moradores e a pontualidade do pagamento das despesas.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ao invés de se criar um fundo de caixa para a inadimplência, aumentando o valor do rateio e o conseqüente aumento da taxa de condomínio, o desconto permite o rateio das despesas mantendo uma taxa individual mais baixa.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Com a redução da multa de vinte para dois por cento (novo código civil) alguns moradores (aqueles que não pagam em dia) votam contra o desconto com o argumento de se tratar de uma "multa embutida". Contudo, a vasta jurisprudência dos Tribunais Superiores tratam o assunto como "desconto pontualidade". Ainda, não existe Lei que regule a matéria. Com isso, a vontade dos condôminos sempre prevalece nos Tribunais.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deve-se contudo, ter alguns cuidados na definição de desconto para a Taxa de Condomínio, a saber:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>convocar uma Assembléia Geral com pauta específica: discussão e deliberação sobre a implantação do desconto de 20% na taxa de condomínio;</li>
                            <li>O percentual já deve ser informado no edital para que todos possam ter conhecimento e aqueles que não comparecerem estejam concordando com a maioria, conforme definido na Lei (as decisões da Assembléia Geral obrigam os presentes e ausentes);</li>
                            <li>Não é necessário a presença de 2/3 dos condôminos na Assembléia, visto que, a Lei não fala sobre a matéria. Contudo, novas convenções podem estar tratando da matéria e, neste caso, deve-se obedecer o que está escrito;</li>
                            <li>se houver decisão favorável ao desconto, a ata da Assembléia Geral deverá conter um texto claro e específico e registrada no Cartório de Registro de Títulos e Documentos para se tornar pública e fácil acesso no caso de extravio.</li>
                            <li>O percentual do desconto é livre e não estará vinculado a qualquer motivo financeiro do Condomínio. Quanto maior o intervalo entre a data do vencimento e desconto, maior pode ser o desconto. Deve-se evitar contudo, descontos para a própria data do vencimento e em dias não úteis. Isso dificulta a interpretação do caixa do banco e pode gerar recusas na concessão do desconto.</li>
                          </ul>
                        </div>
                      </div>
                      <h6>Exemplos de texto sobre desconto para a Ata</h6><br>
                      <blockquote class="quote-variant-1">
                        <div class="quote-body">
                          <p>
                            <q>... passando ao item da pauta, o Sr. Síndico informou que o desconto na Taxa de Condomínio se faz necessário para que não se aumente o rateio das despesas com percentual para inadimplência, única forma de cumprir com as obrigações condominiais sem multa e juros e empréstimos bancários, onerando assim os que pagam em dia ...</q>
                          </p>
                        </div>
                      </blockquote>
                      <blockquote class="quote-variant-1">
                        <div class="quote-body">
                          <p>
                            <q>... após votação, foi aprovado por unanimidade/maioria absoluta que a Taxa de Condomínio terá vencimento no dia 10 do mês de referência e será concedido um desconto de 20% (vinte por cento) para o pagamento realizado até o terceiro dia útil do mês de referência.</q>
                          </p>
                        </div>
                      </blockquote>
                      <br><h6>JURISPRUDÊNCIA SOBRE DESCONTO</h6>
                      <hr>
                      <p>
                          <b>AÇÃO DE COBRANÇA. CONDOMÍNIO. REDUÇÃO DA TAXA CONDOMINIAL. DELIBERAÇÃO EM ASSEMBLÉIA. PRESUNÇÃO DE LEGITIMIDADE.</b><br>
                          A ata da assembléia que deliberou sobre o desconto de 40% da taxa associativa para os lotes que menciona, não contendo ressalvas, goza de presunção de legitimidade, não podendo o MM. Juiz, substituindo a manifestação de vontade dos condôminos, alterar as decisões ali deliberadas para conceder o desconto à períodos pretéritos, sendo forçoso, portanto, que prevaleça o quanto pactuado. Recurso parcialmente provido.<br>
                          <b>(20040710228178APC, Relator LÉCIO RESENDE, 3ª Turma Cível, julgado em 08/08/2005, DJ 22/09/2005 p. 95)</b>
                      </p>
                      <hr>
                      <p>
                          <b>AÇÃO DE COBRANÇA. CONDOMÍNIO. PRESTAÇÕES EM ATRASO. PAGAMENTO ADIANTADO. DESCONTO PONTUALIDADE. BENEFÍCIO DA COLETIVIDADE. MULTA POR ATRASO. FIXAÇÃO DE ACORDO COM A CONVENÇÃO DO CONDOMÍNIO.</b><br>
                          1. A Convenção do Condomínio tem força formal obrigatória de direito, porque constitui a lei própria do Condomínio. Sendo assim, o chamado "desconto pontualidade" instituído pelos condôminos é legítimo, visto que, especialmente em período de inflação, tem sido adotado no gerenciamento de cobranças, seja de taxas de condomínio, seja de mensalidades de clubes, seja de mensalidades escolares. Até mesmo o Poder Público assim procede no recolhimento de impostos, sem qualquer mácula de ilegalidade. O adiantamento incentivado, pois, não pode ser confundido com penalidade, devendo ser interpretado como prêmio oferecido a quem antecipa o pagamento.<br>
                          2. A multa de mora estabelecida pela Convenção de Condomínio, em consonância com o Código Civil Brasileiro e Lei nº 4.591, de 16/12/1964, pode ser exigida do condômino inadimplente.<br>
                          3. A despesa que o Condomínio realiza, devidamente autorizada pela Convenção do Condomínio e por Assembléia, para contratar advogado, para ajuizar ação de cobrança contra condômino inadimplente, pode ser cobrada deste, porque não é justo que os demais condôminos paguem essa despesa extra. A verba honorária decorrente da sucumbência é destinada exclusivamente ao patrono da causa e não tem o condão de cobrir a despesa extra que o Condomínio realiza com a contratação de advogado.<br>
                          4. Tendo sido a verba honorária fixada em valor compatível com a natureza e importância da causa, considerando o grau de zelo do profissional, o lugar da prestação do serviço, o trabalho realizado e o tempo exigido para o serviço, não há motivo para que seja majorada ou reduzida.  
                      </p>
                      <hr>
                      <p>
                          <b>DESPESAS DE CONDOMÍNIO. DESCONTO PONTUALIDADE. JUROS DE MORA E MULTA.</b>
                          O chamado desconto pontualidade, a ser concedido aos que pagam as despesas de condomínio antes do vencimento, não tem caráter de multa contratual.<br>
                          Apelação não provida.(20030710090326APC, Relator JAIR SOARES, 6ª Turma Cível, julgado em 27/09/2004, DJ 28/10/2004 p. 78)
                      </p>
                      <hr>
                      <p>
                          <b>CIVIL - AÇÃO DE COBRANÇA - CONDOMÍNIO - PRESTAÇÕES EM ATRASO - DESCONTO PONTUALIDADE - MULTA E JUROS DE MORA - INCIDÊNCIA - CÓDIGO DE DEFESA DO CONSUMIDOR - INAPLICABILIDADE.</b><br>
                          1. O "desconto pontualidade" não pode ser confundido com a imposição de multa aos que atrasam no resgate das respectivas contribuições. Configura-se em um prêmio para aqueles que cumprem as obrigações antecipadamente.<br>
                          2. A multa prevista na Convenção Condominial pode ser cobrada licitamente em relação às prestações vencidas antes da vigência do Novo Código Civil. Assim, sua redução será possível tão-somente para as prestações vencidas após a vigência do referido diploma legal.<br>
                          3. O valor cobrado a título de juros de mora, quando estipulado na Convenção, dentro dos limites previstos na Lei 4.591/64, sobrepuja-se, em relação à matéria, ao Código Civil.<br>
                          4. O Código do Consumidor não se aplica às relações entre condomínio e condôminos, mas sim a Convenção, com apoio na Lei 4.591/64.<br>
                          5. Apelo improvido.(20030110072263APC, Relator SANDRA DE SANTIS, 6ª Turma Cível, julgado em 14/06/2004, DJ 05/08/2004 p. 49)
                      </p>
                      <hr>
                      <p>
                          <b>Desembargador ASDRUBAL NASCIMENTO LIMA - Relator APC 2000 07 1 003768-3</b><br>
                          Conheço do recurso, eis que presentes seus pressupostos de admissibilidade.<br>
                          Ao efetuar uma análise nos autos observei que a decisão objurgada não merece reparos.<br>
                          Com efeito, consoante se infere do conjunto probatório contido nos autos vê-se que o Apelante confessa que realmente devia os encargos condominiais, insurgindo-se contra o chamado <b>desconto por pontualidade</b>, que se caracteriza por conceder abatimento a quem paga antecipadamente a taxa condominial, sendo que no caso em comento o valor sofre um desconto de R$ 30,00 (trinta reais).<br>
                          Ora, como bem assentado na douta sentença, fls. 58/59, "Destarte, em que pese se insurja o réu contra tal forma de fixação de valor, suas alegações não procedem. <b>Com efeito, o abatimento do valor concedido na hipótese de pagamento antecipado constitui o chamado "desconto pontualidade", que nada mais é que a concessão de abatimento no preço da taxa condominial como forma de estimular o condômino a pagá-la com antecedência a fim de permitir ao condomínio a pronta obtenção de recursos para pagar as suas despesas.</b> Tal desconto não se confunde com os acréscimos decorrentes da mora. Prova disso é que a taxa paga no vencimento, embora não goze do desconto, também não sofre o acréscimo dos consectários da mora e da multa. Logo, não prospera o argumento do réu de que o condomínio cobra uma "sobretaxa", sendo o desconto por pontualidade legal."
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A convocação de uma Assembléia Geral, seja ela Ordinária ou Extraordinária, deverá ser realizada através de um edital de convocação, com as seguintes particularidades:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>tipo de Assembléia a ser convocada (Ordinária ou Extraordinária);</li>
                            <li>dia, mês e ano da realização;</li>
                            <li>hora de início em primeira e em segunda convocação (normalmente 30 minutos entre uma convocação e outra ou conforme definido na Convenção);</li>
                            <li>local onde será realizada (de preferência nas dependências do Condomínio);</li>
                            <li>assuntos a serem deliberados;</li>
                            <li>menção sobre o tipo de procuração aceita para representar os Condôminos;</li>
                            <li>menção sobre o impedimento de Condôminos que estão em atraso com suas obrigações condominiais (se assim permitir a Convenção);</li>
                            <li>menção sobre a votação de inquilinos em despesas ordinárias;</li>
                            <li>autor da convocação (Síndico ou percentual de Condôminos definido pela Convenção).</li>
                          </ul>
                        </div>
                      </div>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A entrega do Edital de Convocação deverá obedecer o que está estabelecido na Convenção que, normalmente, é através de correspondência protocolada ou publicação nos jornais de grande circulação.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deverá ser observada uma antecedência mínima para a convocação, conforme definido na Convenção. No caso de assuntos emergenciais, a Assembléia Geral Extraordinária poderá ser convocada até para o mesmo dia, desde que, todos os Condôminos possam ser avisados ou, pelo menos, 2/3 deles.<br>
                      </p>
                      <div class="group-xl">
                          <a class="btn btn-icon btn-icon-left btn-primary btn-no-shadow" href="<?php echo $this->base; ?>/files/modelos/modelo_edital_convocacao.doc"><span class="icon icon-xs fa-arrow-circle-o-down"></span>MODELO DE EDITAL DE CONVOCAÇÃO</a>
                      </div>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Os elevadores do Condomínio são de propriedade comum e devem ter manutenção periódica (mensal) sob contrato, com empresas idôneas e de comprovada eficiência e habilidade técnica (de preferência com o próprio fabricante).<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Quando existirem elevadores social e de serviço, pode-se regulamentar as suas utilizações através de normas e regimentos para os mesmos.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Poderá existir restrição quanto a utilização do elevador social no que diz respeito a trajes das pessoas e tipos de objetos nele transportados, contudo, não poderá haver qualquer tipo de discriminação de pessoas quanto à raça, credo, poder econômico, cor, profissão etc.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Em Brasília  vigora Lei 3.212/2003 que obriga a afixação de uma placa em todos os andares próxima aos elevadores com os seguintes dizeres: Antes de entrar no elevador verifique se o mesmo encontra-se neste andar. Para reforçar a Lei é bom que os elevadores sejam sempre bem iluminados visando condicionar os usuários com uma iluminação forte e de alerta da presença do elevador.<br>
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <h6>&nbsp;&nbsp;&nbsp;&nbsp;Empregados Diretos</h6>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;São os empregados contratados  pelo condomínio, o qual é responsável diretamente pela documentação e registro dos mesmos e por todos os encargos sociais oriundos da folha de pagamento.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Principais vantagens:</b><br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>subordinação total dos empregados às ordens do Síndico;</li>
                            <li>controle da folha de ponto e pagamento;</li>
                            <li>controle dos recolhimentos de encargos sociais;</li>
                            <li>menor custo para o Condomínio, por não existir taxa de administração.</li>
                          </ul>
                        </div>
                      </div>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Principais desvantagens:</b><br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>inexistência de substituto na falta de um dos funcionários;</li>
                            <li>contratação de pessoa jurídica habilitadas para a documentação, registro, pagamento de salários e encargos sociais.</li>
                          </ul>
                        </div>
                      </div>
                      <h6>&nbsp;&nbsp;&nbsp;&nbsp;Empregados Indiretos</h6>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;São os empregados de empresa de conservação e vigilância contratada pelo Condomínio para prestar serviços ao mesmo. Neste caso, o pagamento será efetuado à empresa a qual será responsável, por contrato, pela folha de pagamento e encargos sociais.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Principais vantagens:</b><br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>existência de substitutos na falta de um dos funcionários;</li>
                          </ul>
                        </div>
                      </div>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Principais desvantagens:</b><br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>existência de pessoas habilitadas para a conferência dos pagamentos de salários e recolhimentos de encargos sociais dos empregados, visto que, o Condomínio é co-responsável pelo passivo trabalhista dos empregados das empresas contratadas.</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Todo Condomínio comercial ou residencial deve ter equipamentos de combate à incêndio, sempre nas quantidades necessárias definidas pelas normas do Corpo de Bombeiros. Os principais são:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>mangueiras e extintores;</li>
                            <li>portas de segurança;</li>
                            <li>avisos, cartazes indicativos e preventivos;</li>
                            <li>escadas apropriadas para evacuação imediata;</li>
                            <li>brigada contra incêndio (no caso de grande fluxo de pessoas);</li>
                          </ul>
                        </div>
                      </div>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Os funcionários dos Condomínios devem estar preparados para a utilização dos equipamentos e poderão realizar cursos coletivos ou individuais ministrado pela guarnição do Corpo de Bombeiros mais próxima.<br>
                      </p>
                    </div>
                    <div class="animated fadeIn">
                        <p>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Frações ideais, assim chamadas, são a quota parte ou quinhão que cabe a cada proprietário (Condômino) sobre as coisas comuns, como parte indivisível e inseparável de sua unidade autônoma.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nos Condomínios Comerciais e Residenciais elas são estabelecidas na Escritura de Convenção e refletem o grau de propriedade e responsabilidade de cada proprietário perante as coisas comuns.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Em resumo, cada proprietário, além da sua respectiva unidade autônoma é possuidor de uma fração ideal das coisas comuns. O total das frações ideais na Convenção devem somar 1,000000 (um inteiro).<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Em muitas as Convenções o rateio das despesas é determinado pela tamanho da unidade autônoma ou diretamente relacionada com a fração ideal de propriedade. Exemplo:<br>
                        </p>
                        <p>
                            <b>Rateio total das despesas</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>$ 10.000,00</b><br>
                            Fração da Unidade 101 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0,014207<br>
                            <b>Valor da Taxa de Condomínio</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>$ 142,07</b><br>
                        </p>
                        <hr>
                        <h6>&nbsp;&nbsp;&nbsp;&nbsp;PARECER SOBRE DIVISÃO DE ÁREAS NO CONDOMÍNIO</h6>
                        <p>A divisão de áreas em condomínios regidos pela Lei 4.591 de 16 de dezembro de 1964 (Lei dos Condomínios e Incorporações - LCI), tanto no que se refere à divisão ideal do terreno quanto em relação às áreas de uso em comum, é tema para os mais diversos entendimentos.<br>
                            A LCI estabelece que cada unidade terá como parte inseparável uma fração ideal no terreno e nas coisas comuns, expressa sob forma decimal ou ordinária. Em outro preceito determina que a CONVENÇÃO DE CONDOMÍNIO deverá discriminar as partes de propriedade exclusiva e as do condomínio, com as especificações das diferentes áreas. Para o lançamento das incorporações, ainda, definiu como obrigação o arquivamento, entre outros documentos, de declaração especificando as frações ideais de terreno com as unidades autônomas que a elas corresponderão, bem como descrição e cálculo das áreas das edificações, discriminando, além da global, a das partes comuns.<br>
                            A Associação Brasileira de Normas Técnicas (ABNT), por sua parte, através da NB 140 (atual NBR 12.721), ocupou-se em normalizar a forma de cálculo de áreas e divisão das áreas de uso em comum sem, em todo o texto, referir-se ao modo de fracionamento ideal do terreno. Tudo isto - é bom frisar - destinado a fazer parte do MEMORIAL DE INCORPORAÇÃO para INCORPORAÇÕES EM CONDOMÍNIO.<br>
                            O fato de a ABNT ter restringido sua norma às INCORPORAÇÕES EM CONDOMÍNIO deu origem ao costume de estender as prescrições normativas aos demais casos, justificado este procedimento pela verificação de similitude entre as situações. Ou seja: como a LCI trata de CONDOMÍNIOS (no título I) - isto entendido como condomínio já instituído (construção averbada, convenção registrada, e com matrículas abertas no Cartório de Registro de Imóveis, para cada unidade autônoma)- e de INCORPORAÇÕES IMOBILIÁRIAS (no título II) - "...atividade exercida com o intuito de promover e realizar a construção, para alienação total ou parcial..." - a norma da ABNT, por decorrência, passou a ser referência para todos os aspectos técnicos que envolvem cálculo de áreas em edificações regidas pela lei especial. Mas referência não implica necessariamente determinação normativa.<br>
                            Portanto, a divisão das áreas de uso em comum entre as unidades autônomas tem sua seqüência de cálculo normalizada para o caso de INCORPORAÇÃO EM CONDOMÍNIO. Nas Incorporações conhecidas como "a preço fixo e prazo certo", é costume proceder conforme as determinações encontradas na referida norma técnica. No caso, ainda, de o condomínio ser instituído sobre edifício não incorporado, as normas de serviços das Corregedorias Geral de Justiça recomendam que as especificações sejam apresentadas em instrumento (público ou particular) que caracterize e identifique as unidades autônomas, ao qual é associado, quase sempre, um quadro de áreas elaborado por profissional legalmente habilitado. É nestas duas ultimas situações que surge o costume de adotar, para o cálculo da divisão de áreas de uso em comum, uma norma técnica que trata de caso similar.<br>
                            Analisemos, então, as recomendações normativas da ABNT.<br>
                            A NBR 12.721 utiliza dois critérios de área de uso em comum:<br>
                            a) área de uso em comum de divisão não proporcional;<br>
                            b) área de uso em comum de divisão proporcional.<br>
                            A primeira, refere-se àquelas áreas que são distribuídas mediante designação, independendo do critério de proporção; áreas cujo uso é em comum para determinadas unidades e não para todas.<br>
                            Já para as áreas de uso em comum de divisão proporcional, a norma técnica da ABNT tem um eixo de procedimento: todos os cálculos de divisão de áreas estão relacionados ao custo de construção. A proporcionalidade (que, em princípio, poderia parecer guiada pelas relações entre áreas) tem no custo da unidade autônoma e no custo global da edificação as suas referências.<br>
                            Demasiado seria, para esta análise, descrever tecnicamente todo o processo de cálculo. Basta-nos, assim, saber que as áreas de uso em comum de divisão proporcional são distribuídas obedecendo à proporção entre o custo da área privativa da unidade autônoma e o custo global da edificação.<br>
                            Já para a divisão do terreno em frações ideais não há critério normalizado. O coeficiente último que a Norma propõe - além daquele que determina a divisão das áreas edificadas de uso em comum - é relativo à divisão do custo total da obra. Confundido com "fração ideal", este coeficiente só pode ser assim tratado quando não há áreas sub-rogadas, aquelas dadas em pagamento de parte ou de todo o terreno.<br>
                            Como já foi referido, a LCI determina apenas que estas frações sejam explicitamente declaradas. Tecnicamente, então, a divisão pode ser feita por relação proporcional de áreas reais das unidades autônomas; pelo custo proporcional, conforme o procedimento normalizado; pelo valor venal atribuído às unidades em relação ao valor venal total da edificação; em partes iguais, por acordo entre os proprietários; etc.<br>
                            Cabe lembrar que o termo "ideal" referido em Lei não tem o significado de perfeição conceptível. É ideal por ser uma quota não identificável fisicamente.<br>
                            O projeto do novo Código Civil Brasileiro faz uma opção explícita pela proporcionalidade encontrada na relação de valores: "A fração ideal no solo e nas outras partes comuns é proporcional ao valor da unidade imobiliária, o qual se calcula em relação ao conjunto da edificação." (Art. 1.331, § 3º). Porém, por ter o valor os mais diversos conceitos, muito ainda se terá a definir para que este critério seja adotado como norma.<br>
                            Como se pode concluir, então, nem todos os procedimentos, no caso de divisão de áreas em condomínio especial, têm sua origem em recomendações normalizadas ou definidas em lei. Para INCORPORAÇÕES EM CONDOMÍNIO, existe um modelo a ser seguido. Para as demais situações os critérios devem ser adotados tendo o bom senso como guia principal. Por enquanto, procede-se tecnicamente como "de costume". Sendo assim, é imprópria qualquer análise que defenda esta ou aquela forma como correta ou não.<br>
                            Os costumes tendem a tornar-se regra. Por sua importância, porém, os critérios de cálculo já estão a merecer estudos mais profundos e precisos. Quer-nos parecer que a ABNT deu um primeiro passo, em 1965, quando elaborou a NB 140 (hoje, NBR 12.721), mas os demais não encontraram ainda seu destino.<br>
                        </p>
                    </div>
                    <div class="animated fadeIn">
                        <p>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Para o Condômino que não pagar suas obrigações condominiais na data estabelecida, poderá incidir multa, juros e correção monetária sobre o valor principal do débito, conforme estabelecido na Convenção.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Até a entrada em vigor do novo código civil (10/01/2003) as Convenções poderiam estipular multas de até 20% (vinte por cento). Após esta data, a multa não poderá ultrapassar a 2% (dois por cento) do valor do débito.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A vasta jurisprudência define que os débitos com vencimento até o dia 10/01/2003 (novo código civil) poderá ter a multa calculada em até 20% ou conforme definido na Escritura de Convenção. Após esta data, a multa não poderá ultrapassar a 2% mesmo que a Convenção não tenha sido alterada.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Os Juros de mora são definidos em 1% (um por cento) ao mês ou fração, isto é, se houver um atraso de um dia já incidirá 1% de juros.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A correção monetária é calculada pelos diferenciados índices de inflação, adotados pelos Tribunais de Justiça.<br>
                        </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O Regimento Interno, também chamado de Regulamento Interno, reúne normas de condutas para os moradores de um Condomínio, definindo multas e infrações para desrespeito às cláusulas nele contidas.<br>
                      </p>
                      <div class="group-xl">
                          <a class="btn btn-icon btn-icon-left btn-primary btn-no-shadow" href="<?php echo $this->base; ?>/files/modelos/modelo_regimento_interno.doc"><span class="icon icon-xs fa-arrow-circle-o-down"></span>MODELO DE REGIMENTO INTERNO</a>
                      </div>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Por força da Lei, o Condomínio edificado é obrigado a fazer um seguro contra incêndio e responsabilidade civil a terceiros, abrangendo toda a edificação incluindo-se as áreas comuns.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Como o seguro é previsto em Lei e na Convenção, a Assembléia Geral não poderá optar por não fazê-lo e, toda e qualquer responsabilidade recairá diretamente sobre o Síndico.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As seguradoras têm desenvolvido novas modalidades de seguros contra incêndio e responsabilidade civil, exclusivamente para Condomínios, com custos menores e abrangências maiores, visando até roubos em garagens, danos elétricos e outros.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vale salientar que o seguro do Condomínio não necessita cobrir danos com incêndio no interior das unidades autônomas (conteúdo), visto que, para isto, cada Condômino deverá fazer seu próprio seguro.<br>
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Várias foram as consultas ao nosso Departamento Jurídico, no sentido de verificar a real necessidade/obrigatoriedade de efetuar a Contribuição Sindical, assim como a Contribuição Assistencial, as quais estão sendo cobradas pelos sindicatos patronais.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inicialmente, importante esclarecer que trata-se, o presente, do nosso entendimento sobre a matéria, e que o mesmo pode e deve ser avaliado e discutido pelo Síndico e seu Conselho, antes de que qualquer decisão seja tomada, mesmo porque, cada caso é diferente em algum ponto, merecendo atenção diferenciada.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ao passo que a contribuição é definida por Lei, a liberdade de filiação (ou não) ao Sindicato, é garantida pela Constituição Federal.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Temos portanto, dois tipos de contribuição: a Sindical e a Assistencial.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A contribuição sindical, é prevista pela Constituição Federal, e devida uma vez por ano, para os Sindicatos Patronais (mês de Janeiro) e para o Sindicato dos Empregados (mês de março).<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A contribuição assistencial, é estabelecida pela Assembléia Geral dos Sindicatos e devida para aqueles que forem filiados, tanto para a parte patronal quanto para a laboral. Conforme a legislação abaixo, a contribuição assistencial não é obrigatória e quaisquer ameaças dos Sindicatos são abusivas e contra a Lei.<br>
                      </p>
                      <hr>
                      <p class="text-center">
                          CONSOLIDAÇÃO DAS LEIS DO TRABALHO - CLT<br>
                          DECRETO-LEI Nº5452, DE 01 DE MAIO DE 1943<br>
                          TÍTULO V - Da Organização Sindical
                      </p>
                      <p>
                          Artigos 511 a 610.CAPÍTULO III - Da Contribuição Sindical - Artigos 578 a 610.SEÇÃO I - Da Fixação e do Recolhimento da Contribuição Sindical - Artigos 578 a 591.Artigo Número: 586: A contribuição sindical será recolhida, nos meses fixados no presente Capítulo, à Caixa Econômica Federal, ao Banco do Brasil S.A, ou aos estabelecimentos bancários nacionais integrantes do Sistema de Arrecadação dos Tributos Federais, os quais, de acordo com instruções expedidas pelo Conselho Monetário Nacional, repassarão à Caixa Econômica Federal as importâncias arrecadadas.PAR.1º Integrarão a rede arrecadadora as Caixas Econômicas Estaduais, nas localidades onde inexistam os estabelecimentos previstos no "caput" deste artigo. PAR.2º Tratando-se de empregador, agentes ou trabalhadores autônomos ou profissionais liberais, o recolhimento será efetuado pelos próprios, diretamente ao estabelecimento arrecadador.PAR.3º A contribuição sindical devida pelos empregados e trabalhadores avulsos será recolhida pelo empregador e pelo Sindicato, respectivamente.
                      </p>
                      <hr>
                      <p>
                          Ementa: AÇÃO ORDINÁRIA DE COBRANÇA - SINDICATO - CONTRIBUIÇÃO - LIBERDADE DE ASSOCIAÇÃO - INTELIGÊNCIA DOS ARTIGOS 5º, XX E 8º, V DA CONSTITUIÇÃO DA REPÚBLICA DO BRASIL - RECURSO IMPROVIDO.O mandamento constitucional assegurando que "ninguém poderá ser compelido" a filiar-se a um sindicato ou nele permanecer contra a sua própria vontade, consagrou em nosso país a plena liberdade sindical. Não pode o empregador efetuar o desconto da contribuição sindical de quem não é filiado.Acórdão: Vistos, relatados e discutidos os autos do Recurso de Apelação Cível - Classe II - 20 - nº. 20.182, de Araputanga.ACORDA, em TURMA, a Segunda Câmara Cível do Tribunal de Justiça do Estado de Mato Grosso, sem voto dissidente desprover, contestemente, o recurso, contrariamente a sugestão emanada do órgão ministerial. Custas pela apelante.Presidiu o julgamento o Desembargador ATAHIDE MONTEIRO DA SILVA e dele participaram os Desembargadores BENEDITO PEREIRA DO NASCIMENTO (Relator), ODILES FREITAS SOUZA (Revisor) e MARIANO ALONSO RIBEIRO TRAVASSOS (Vogal). O voto proferido pelo Desembargador relator foi acompanhado na íntegra pelos demais componentes da Turma julgadora.Data: Cuiabá, 7/04/1998.
                      </p>
                      <hr>
                      <p>
                          (...). MANOEL GONÇALVES FERREIRA FILHO distingue claramente entre imposto sindical e contribuição confederativa (ipsis verbis): "Imposto sindical. O texto em estudo consagra implicitamente o chamado imposto sindical, contribuição compulsoriamente arrecadada aos trabalhadores, destinada à manutenção das associações sindicais. É a 'contribuição prevista em lei'. (...)." "Contribuição confederativa. Uma contribuição poderá ser instituída para custeio da confederação a que se filiar, embora indiretamente, determinado sindicato. Deverá ela, porém, ser estabelecida em assembléia geral. Adotada, porém, pelo sindicado, deverá ser descontada em folha pelo empregador" (in Comentários à Constituição brasileira de 1988, 1990, p. 109-110 - destaque nosso). O célebre constitucionalista JOSÉ AFONSO DA SILVA, igualmente, atenta para a mesma distinção (in verbis): "Tema controvertido que, no entanto, ficou admitido pelo art. 8º, IV, que autoriza a assembléia geral a fixar contribuição sindical que, em se tratando de categoria profissional, será descontada em folha, para custeio do sistema confederativo da representação sindical respectiva, independentemente da contribuição prevista em lei. Há portanto, duas contribuições: uma para custeio de confederações e outra de caráter parafiscal, porque compulsória estatuída em lei, que são, hoje, os arts. 578 a 610 da CLT, chamada 'Contribuição Sindical', paga, recolhida e aplicada na execução de programas sociais de interesse das categorias representadas" (in Curso de Direito Constitucional Positivo, 15ª ed., 1998, p. 305-306 - destaque nosso). Não discrepa da doutrina supra referida a jurisprudência do SUPREMO TRIBUNAL FEDERAL (ipsis verbis): Sindicato. Contribuição sindical da categoria. Recepção. "A recepção pela ordem constitucional vigente da contribuição sindical compulsória, prevista no art. 578, CLT, é exigível de todos os integrantes da categoria, independentemente de sua filiação ao sindicato, resulta do art. 8º, IV, in fine, da Constituição; nada obsta à recepção a proclamação, no caput do art. 8º, do princípio da liberdade sindical, que há de ser compreendido a partir dos termos em que a Lei Fundamental a positivou, nos quais a unicidade (art. 8º, II) e a própria contribuição de natureza tributária (art. 8º, IV) - marcas características do modelo corporativista resistente - , dão a medida da sua relatividade (cf. MI 144, Pertence, RTJ 147/868-874); nem a impede a recepção questionada a falta da lei complementar prevista no art. 146, III, CF, à qual alude o art. 149, à vista do disposto no art. 34, §§ 3º e 4º, das Disposições Transitórias (cf. RE 146733, Moreira Alves, RTJ 146/684-694)" (RE 180745/SP, Primeira Turma do Supremo Tribunal federal, Relator Ministro SEPÚLVEDA PERTENCE, DJU 08/05/98). Ora, a petição inicial refere-se justamente à contribuição sindical prevista na Consolidação das Leis do Trabalho, portanto, obrigatória, que não se confunde com as contribuições federativa ou assistencial, estas últimas facultativas. (...)Data do Documento: Belo Horizonte, 11/02/1999.
                      </p>
                      <hr>
                      <p>
                          Acórdão: AC 47.800 - Relator: Des. Trindade dos Santos - Câmara: 1ª Câmara Cível.Ementa: CONTRIBUIÇÃO ASSISTENCIAL PATRONAL - CONDOMÍNIO NÃO SINDICALIZADO - EXIGÊNCIA INDEVIDA - DESACOLHIMENTO DA AÇÃO DE COBRANÇA CONTRA ELE DIRIGIDA - INCONFORMISMO DO SINDICATO RESPECTIVO - DESPROVIMENTO - Especial relevo há que ser emprestado ao permissivo legal constante do art. 8º, caput, do Estatuto Fundamental, que assegura a total liberdade da associação profissional e sindical. Assegurada essa plena liberdade, implicaria na sua ostensiva negação, ou na sua redução a uma questão apenas de retórica, compelir-se judicialmente, numa atitude flagrantemente antidemocrática, os não-filiados a determinada entidade sindical a, em total submissão ao exclusivo arbítrio desta, satisfazerem contribuições sindicais ou assistenciais patronais com as quais não anuíram e das quais discordam. (TJSC - AC 47.800 - Capital - Rel. Des. Trindade dos Santos - 1ª C.C. - J. 23.04.1996)
                      </p>
                      <hr>
                      <p>
                          Acórdão: AC 47.890 - Relator: Des. Trindade dos Santos - Câmara: 1ª Câmara Cível. Ementa: CONTRIBUIÇÃO ASSISTENCIAL PATRONAL - CONDOMÍNIO NÃO SINDICALIZADO - EXIGÊNCIA INDEVIDA - DESACOLHIMENTO DA AÇÃO DE COBRANÇA CONTRA ELE DIRIGIDA - INCONFORMISMO DO SINDICATO RESPECTIVO - DESPROVIMENTO - Assegurando o Texto Fundamental, em seu art. 8º, caput, a liberdade da associação sindical, equivaleria a uma negação dessa liberdade, ou a sua redução a uma questão meramente retórica, compelir-se judicialmente, numa atitude flagrantemente atentatória aos mais comezinhos princípios regedores de um Estado Democrático de Direito, os não-filiados a determinada entidade sindical a, em total submissão ao jugo desta, satisfazerem contribuições sindicais ou assistenciais patronais com as quais não anuíram e das quais discordam. (TJSC - AC 47.890 - Capital - Rel. Des. Trindade dos Santos - 1ª C.C. - J. 23.04.1996) (grifou-se)
                      </p>
                      <hr>
                      <p>
                          Ementa: SINDICATO - Contribuição assistencial - Faculdade do empregado em se opor ao desconto - Artigo 545 da Consolidação das Leis do Trabalho - Recurso não provido. Exceção feita à cobrança da contribuição sindical, veda o artigo 545 da Consolidação das Leis do Trabalho qualquer outro desconto sem autorização do empregado. (...) FUNDAMENTAÇÃO: As razões de recurso não abalaram a fundamentação da respeitável sentença apelada, que deu adequado desate à lide, aplicando corretamente a regra do artigo 545 da Consolidação das Leis do Trabalho, que, exceção feita à cobrança da contribuição sindical, veda qualquer outro desconto sem autorização do empregado. Na hipótese em exame, a empresa autora efetuou o desconto da contribuição assistencial de seus assalariados, deixando de fazê-lo apenas em relação a 35 empregados que se opuseram. Nem se argumente que o artigo 462 da Consolidação das Leis do Trabalho autorizaria o desconto. Tal artigo contém uma vedação ao empregador a proceder qualquer desconto, salvo se decorrente de lei ou de convenção coletiva e o artigo 545, do mesmo diploma legal, uma faculdade ao empregado de se opor a qualquer desconto, salvo em relação ao imposto sindical. (...).
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Conforme definido em Lei, o Síndico poderá ser pessoa física ou jurídica, proprietário ou não e, normalmente nas Convenções, o Síndico é obrigatoriamente Condômino (proprietário) e pessoa física.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Legislação atual não prevê a figura do Subsíndico, contudo, a Convenção poderá estabelecer e caso esta não estabeleça, poderá ser criada por decisão de uma Assembléia Geral. O Subsíndico tem papel importante no Condomínio, auxiliando o Síndico e substituindo-o em caso de ausência.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Para melhor resguardar o patrimônio do Condomínio o Síndico e o Subsíndico deverão ser pessoas conhecidas dos Condôminos e de preferência com interesse e responsabilidade sob os bens comuns.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Convenção poderá fixar o mandato do Síndico em, no máximo 02 anos, sendo permitida a reeleição. Dentre suas atribuições, citamos as principais:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>representar, ativa e passivamente, o condomínio, em juízo ou fora dele, e praticar os atos de defesa dos interesses comuns, nos limites das atribuições conferidas pela lei ou pela convenção;</li>
                            <li>exercer a administração interna da edificação ou do conjunto de edificações, no que respeita a sua vigilância, moralidade e segurança, bem como aos serviços que interessam a todos os moradores;</li>
                            <li>praticar os atos que lhe atribuírem as leis, a convenção e o regimento interno;</li>
                            <li>impor as multas estabelecidas na lei, na convenção ou no regimento interno;</li>
                            <li>cumprir e fazer cumprir a convenção e o regimento interno, bem como executar e fazer executar as deliberações da assembléia;</li>
                            <li>prestar contas à assembléia; dos condôminos;</li>
                            <li>manter guardada durante o prazo de cinco anos, para eventuais necessidades de verificação contábil, toda a documentação relativa ao condomínio.</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="animated fadeIn">
                      <h6>&nbsp;&nbsp;&nbsp;&nbsp;Taxa de Condomínio</h6>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A taxa de condomínio é o valor que cabe a cada proprietário referente às despesas ordinárias do Condomínio. Se a Convenção estipular, o rateio das despesas deverá ser realizado proporcionalmente a fração ideal de cada unidade, isto é, quem tem a maior propriedade paga mais e vice-versa.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O valor da taxa de condomínio é preferencialmente definido pela Assembléia Geral Ordinária anual, mas poderá ser aprovado na mesma Assembléia que o Síndico juntamente com o Conselho definam mensalmente o valor a ser rateado. Está atitude é tomada geralmente em novos condomínios onde é grande a variação das despesas.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As despesas ordinárias de um Condomínio são:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>salários, encargos trabalhistas, contribuições previdenciárias e sociais dos empregados do condomínio;</li>
                            <li>consumo de água e esgoto, gás, luz e força das áreas de uso comum;</li>
                            <li>limpeza, conservação e pintura das instalações e dependências de uso comum;</li>
                            <li>manutenção e conservação das instalações e equipamentos hidráulicos, elétricos, mecânicos e de segurança, de uso comum;</li>
                            <li>manutenção e conservação das instalações e equipamentos de uso comum destinados à prática de esportes e lazer;</li>
                            <li>manutenção e conservação de elevadores, porteiro eletrônico e antenas coletivas;</li>
                            <li>pequenos reparos nas dependências e instalações elétricas e hidráulicas de uso comum;</li>
                            <li>rateios de saldo devedor ordinário de caixa;</li>
                            <li>reposição de fundo de reserva, total ou parcialmente utilizado no custeio ou complementação das despesas ordinárias.</li>
                            <li>outras despesas de caráter comum e ordinário.</li>
                          </ul>
                        </div>
                      </div>
                      <h6>&nbsp;&nbsp;&nbsp;&nbsp;Taxa Extra</h6>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A taxa de extra, como o nome diz, é implantada para o custeio de despesas extraordinárias, as quais não têm caráter constante no Condomínio.Se a Convenção estipular, o rateio das despesas deverá ser realizado proporcionalmente a fração ideal de cada unidade, isto é, que tem a maior propriedade paga mais e vice-versa.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Toda e qualquer taxa extra deve ser definida formalmente em Assembléia Geral com convocação específica para que todos saibam o que será tratado.<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;São exemplos de despesas extraordinárias:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>obras de reformas ou acréscimos que interessem à estrutura integral do imóvel;</li>
                            <li>pintura das fachadas, empenas, poços de aeração e iluminação, bem como das esquadrias externas;</li>
                            <li>obras destinadas a repor as condições de habitabilidade do edifício;</li>
                            <li>instalação de equipamentos de segurança e de incêndio, de telefonia, de intercomunicação, de esporte e de lazer;</li>
                            <li>despesas de decoração e paisagismo nas partes de uso comum;</li>
                            <li>constituição de fundo de reserva.</li>
                          </ul>
                        </div>
                      </div>
                      <h6>&nbsp;&nbsp;&nbsp;&nbsp;Outras Taxas</h6>
                      <p>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Além das taxas normais, o Condomínio poderá definir outras taxas que se fizerem necessárias e cobrá-las em uma única guia de pagamento. As mais comuns são:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li>Taxa de Ocupação de Área Comum - definida para cobrar ocupações como quiosques, vagas de garagem, pc´s e outras áreas de propriedade comum;</li>
                            <li>Taxa de Gás - rateio mensal da conta de gás de uso comum;</li>
                            <li>Seguro Obrigatório - rateio anual do seguro obrigatório;</li>
                            <li>Multa / Infração - valor a ser cobrado do morador que descumpre as normas;</li>
                            <li>Churrasqueira - valor definido para a utilização da churrasqueira comum;</li>
                            <li>Salão de Festas - valor definido para a utilização do salão de festas;</li>
                            <li>Custas Processuais - despesas com cópias, certidões de ônus e montagem de processo judicial para cobrança de taxas em atraso;</li>
                            <li>Custas Judiciais - despesas com o processo judicial desde a sua instauração.</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As vagas de garagem são destinadas a guarda de veículos dos moradores do Condomínio e/ou conforme definido da Convenção e Regimento Interno. Quanto a propriedade da vaga de garagem, existem três formas distintas:<br>
                      </p>
                      <div class="offset-top-10">
                        <div class="inset-xs-left-18">
                          <ul class="list-marked-variant-2">
                            <li><b>Vinculada a unidade autônoma</b> - são vagas, previamente demarcadas, com planta registrada no cartório e pertencentes a uma ou mais unidades autônomas. Na compra e venda do imóvel a vaga está vinculada e não poderá ser desmembrada. Este é o tipo mais comum de vaga de garagem utilizada em edifícios.</li>
                            <li><b>Unidade autônoma</b> - são vagas vendidas separadamente e constituem uma unidade autônoma. Não está vinculada a nenhuma outra unidade e é tratada como um Condômino dentre os outros. Este tipo de vaga é utilizada quando o espaço destinado à garagem do Condomínio é muito grande e a demarcação excede o número de unidades. Com isso, vagas extras são oferecidas aos proprietários de unidades ou pessoas externas.</li>
                            <li><b>Uso comum</b> - são vagas pertencentes a todos os Condôminos. Estas vagas não constam da escritura de nenhuma unidade autônoma e sim da Convenção do Condomínio como partes de uso comum. Neste caso, a Assembléia Geral deverá lhe dar a destinação através de sorteio ou qualquer outro modo justo de distribuição entre os moradores.</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                      
                  </div>
                </div>
                
                <div class="group-xl">
                  <a class="btn btn-icon btn-icon-left btn-primary btn-no-shadow" href="../contato">Quero enviar uma dúvida para a Ebac</a>
                </div>
                
              </div>
            </div>
          </div>
        </section>