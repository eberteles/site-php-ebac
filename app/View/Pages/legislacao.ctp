        <section class="section-top-40 section-sm-top-40">
          <div class="shell">
            <div class="range">
              <div class="cell-xs-12">
                  <p class="text-center"><b>Condomínio:</b> domínio exercido por mais de uma pessoa.</p>
                  <p class="text-center"><b>Condômino:</b> um dos proprietários do condomínio</p><br>
                  A <a href="#lei1" data-custom-scroll-to="lei1">Lei 4.591 de 16 de dezembro de 1964</a>, dispõe sobre condomínio em edificações e as incorporações imobiliárias. Esta é lei maior de um condomínio, a qual estabelece normas, condutas e procedimentos para a formação e manutenção da vida condominial.<br><br>
                  A <a href="#lei2" data-custom-scroll-to="lei2">Lei 8.245 de 18 de outubro de 1991</a>, também chamada de Lei do Inquilinato, estabeleceu diversas normas e procedimentos, as quais passaram a regular o relacionamento Condômino e Inquilino e, principalmente, as obrigações e deveres do Inquilino perante o Condomínio.<br><br>
                  A <a href="#lei3" data-custom-scroll-to="lei3">Lei 10.406 de 10 de Janeiro de 2002</a>, também chamada de Código Civil, modificou e acrescentou vários textos da Lei 4.591/64, entrando em vigor em 10/01/2003.<br><br>
                  A <a href="#escritura" data-custom-scroll-to="escritura">Escritura de Convenção</a> de um Condomínio é um contrato celebrado entre os proprietários, registrado no Cartório de Registro de Imóveis pertinente, na qual são estabelecidas, além das obrigações e deveres previstos nas Leis, particularidades e decisões específicas dos Condôminos.<br>
                      Normalmente, consta da Escritura de Convenção, normas, atribuições, obrigações e deveres que não tenham uma modificação constante, visto que, o ritual de  registro e alteração de uma Convenção é dispendioso e depende da vontade da maioria do Condôminos.<br><br>
                  O <a href="#regimento" data-custom-scroll-to="regimento">Regimento Interno ou Regulamento Interno</a> é um documento obrigatório do Condomínio, definido na Escritura de Convenção e aprovado pela Assembléia Geral dos Condôminos,  e tem por objetivo, detalhar os direitos e deveres do moradores, ocupantes, usuários e proprietários.<br>
                      Nele irão constar normas e regras não estabelecidas nas Leis e Escritura de Convenção, as quais detalham as regras de convivência em comunidade.
                  <br><br>
                  A <b>ata de uma Assembléia</b> é um documento celebrado em reunião de Condôminos e/ou Ocupantes, convocada por Edital específico, e tem como objetivo tomar decisões que não estão previstas nas Leis,  Escritura de Convenção e no Regimento Interno<br><br>
                  As <b>decisões do Síndico, Subsíndico ou Membros da Administração</b> visam cumprir o  estabelecido nas Leis, Escritura de Convenção, Regimento Interno ou em Atas das Assembléias.<br><br>
                  
                  <br>
                  
                  <div class="grid-element" id="lei1">
                      <h6>Lei 4.591/64 - Título I</h6>
<p class="text-center"><strong>CONDOMÍNIO E INCORPORAÇÃO</strong></p>
<p class="text-center"><strong>LEI Nº 4591, DE 16 DE DEZEMBRO DE 1964</strong></p>
<br>
<strong>       O PRESIDENTE DA REPÚBLICA</strong>, faço saber que o CONGRESSO NACIONAL decreta e eu sanciono a seguinte Lei: <br>
<p class="text-center">TÍTULO I </p>
<p class="text-center">DO CONDOMÍNIO </p>
        Art. 1º As edificações ou conjuntos de edificações, de um ou mais pavimentos, construídos sob a forma de unidades isoladas entre si, destinadas a fins residenciais ou não-residenciais, poderão ser alienados, no todo ou em parte, objetivamente considerados, e constituirá, cada unidade, propriedade autônoma sujeita às limitações desta Lei. <br>
        § 1º Cada unidade será assinalada por designação especial, numérica ou alfabética, para efeitos de identificação e discriminação. <br>
        § 2º A cada unidade caberá, como parte inseparável, uma fração ideal do terreno e coisas comuns, expressa sob forma decimal ou ordinária <br>
   Art. 2º Cada unidade com saída para a via pública, diretamente ou por processo de passagem comum, será sempre tratada como objeto de propriedade exclusiva, qualquer que seja o número de suas peças e sua destinação, inclusive <u>(VETADO)</u> edifício-garagem, com ressalva das restrições que se lhe imponham. <br>
        § 1º O direito à guarda de veículos nas garagens ou locais a isso destinados nas edificações ou conjuntos de edificações será tratado como objeto de propriedade exclusiva, com ressalva das restrições que ao mesmo sejam impostas por instrumentos contratuais adequados, e será vinculada à unidade habitacional a que corresponder, no caso de não lhe ser atribuída fração ideal específica de terreno. <u>(Parágrafo incluído pela Lei nº 4.864, de 29.11.1965)</u><br>
        § 2º O direito de que trata o § 1º dêste artigo poderá ser transferido a outro condômino, independentemente da alienação da unidade a que corresponder, vedada sua transferência a pessoas estranhas ao condomínio. <u>(Parágrafo incluído pela Lei nº 4.864, de 29.11.1965)</u><br>
        § 3º Nos edifícios-garagem, às vagas serão atribuídas frações ideais de terreno específicas.<u>(Parágrafo incluído pela Lei nº 4.864, de 29.11.1965)</u><br>
        Art. 3º O terreno em que se levantam a edificação ou o conjunto de edificações e suas instalações, bem como as fundações, paredes externas, o teto, as áreas internas de ventilação, e tudo o mais que sirva a qualquer dependência de uso comum dos proprietários ou titulares de direito à aquisição de unidades ou ocupantes, constituirão condomínio de todos, e serão insuscetíveis de divisão, ou de alienação destacada da respectiva unidade. Serão, também, insuscetíveis de utilização exclusiva por qualquer condômino <u>(VETADO)</u>. <br>
        Art. 4º A alienação de cada unidade, a transferência de direitos pertinentes à sua aquisição e a constituição de direitos reais sôbre ela independerão do consentimento dos condôminos,<u>(VETADO)</u>.   <br>
        <s>Parágrafo único. O adquirente de uma unidade responde pelos débitos do alienante, em relação ao condomínio, inclusive multas. </s><br>
      Parágrafo único - A alienação ou transferência de direitos de que trata este artigo dependerá de prova de quitação das obrigações do alienante para com o respectivo condomínio. <u>(Redação dada pela Lei nº 7.182, de 27.3.1984)</u><br>
        Art. 5º O condomínio por meação de parede, soalhos, e tetos das unidades isoladas, regular-se-á pelo disposto no Código Civil, no que lhe fôr aplicável. <br>
        Art. 6º Sem prejuízo do disposto nesta Lei, regular-se-á pelas disposições de direito comum o condomínio por quota ideal de mais de uma pessoa sôbre a mesma unidade autônoma. <br>
        Art. 7º O condomínio por unidades autônomas instituir-se-á por ato entre vivos ou por testamento, com inscrição obrigatória no Registro de Imóvel, dêle constando; a individualização de cada unidade, sua identificação e discriminação, bem como a fração ideal sôbre o terreno e partes comuns, atribuída a cada unidade, dispensando-se a descrição interna da unidade. <br>
        Art. 8º Quando, em terreno onde não houver edificação, o proprietário, o promitente comprador, o cessionário dêste ou o promitente cessionário sôbre êle desejar erigir mais de uma edificação, observar-se-á também o seguinte: <br>
        a) em relação às unidades autônomas que se constituírem em casas térreas ou assobradadas, será discriminada a parte do terreno ocupada pela edificação e também aquela eventualmente reservada como de utilização exclusiva dessas casas, como jardim e quintal, bem assim a fração ideal do todo do terreno e de partes comuns, que corresponderá às unidades; <br>
        b) em relação às unidades autônomas que constituírem edifícios de dois ou mais pavimentos, será discriminada a parte do terreno ocupada pela edificação, aquela que eventualmente fôr reservada como de utilização exclusiva, correspondente às unidades do edifício, e ainda a fração ideal do todo do terreno e de partes comuns, que corresponderá a cada uma das unidades; <br>
        c) serão discriminadas as partes do total do terreno que poderão ser utilizadas em comum pelos titulares de direito sôbre os vários tipos de unidades autônomas; <br>
        d) serão discriminadas as áreas que se constituírem em passagem comum para as vias públicas ou para as unidades entre si. <br>
<p class="text-center">Capítulo II </p>
<p class="text-center">Da Convenção de Condomínio </p>
        Art. 9º Os proprietários, promitentes compradores, cessionários ou promitentes cessionários dos direitos pertinentes à aquisição de unidades autônomas, em edificações a serem construídas, em construção ou já construídas, elaborarão, por escrito, a Convenção de condomínio, e deverão, também, por contrato ou por deliberação em assembléia, aprovar o Regimento Interno da edificação ou conjunto de edificações. <br>
        § 1º Far-se-á o registro da Convenção no Registro de Imóveis, bem como a averbação das suas eventuais alterações. <br>
        § 2º Considera-se aprovada, e obrigatória para os proprietários de unidades, promitentes compradores, cessionários e promitentes cessionários, atuais e futuros, como para qualquer ocupante, a Convenção que reúna as assinaturas de titulares de direitos que representem, no mínimo, 2/3 das frações ideais que compõem o condomínio. <br>
        § 3º Além de outras normas aprovadas pelos interessados, a Convenção deverá conter: <br>
        a) a discriminação das partes de propriedade exclusiva, e as de condomínio, com especificações das diferentes áreas; <br>
        b) o destino das diferentes partes; <br>
        c) o modo de usar as coisas e serviços comuns; <br>
        d) encargos, forma e proporção das contribuições dos condôminos para as despesas de custeio e para as extraordinárias; <br>
        e) o modo de escolher o síndico e o Conselho Consultivo; <br>
        f) as atribuições do síndico, além das legais; <br>
        g) a definição da natureza gratuita ou remunerada de suas funções; <br>
        h) o modo e o prazo de convocação das assembléias gerais dos condôminos; <br>
        i) o quorum para os diversos tipos de votações; <br>
        j) a forma de contribuição para constituição de fundo de reserva; <br>
        l) a forma e o quorum para as alterações de convenção; <br>
        m) a forma e o quorum para a aprovarão do Regimento Interno quando não incluídos na própria Convenção. <br>
        § 4º No caso de conjunto de edificações, a que se refere o art. 8º, a convenção de condomínio fixará os direitos e as relações de propriedade entre os condôminos das várias edificações, podendo estipular formas pelas quais se possam desmembrar e alienar porções do terreno, inclusive as edificadas. <u>(Parágrafo incluído pela Lei nº 4.864, de 29.11.1965)</u><br>
        Art. 10. É defeso a qualquer condômino: <br>
        I - alterar a forma externa da fachada; <br>
        Il - decorar as partes e esquadriais externas com tonalidades ou côres diversas das empregadas no conjunto da edificação; <br>
        III - destinar a unidade a utilização diversa de finalidade do prédio, ou usá-la de forma nociva ou perigosa ao sossêgo, à salubridade e à segurança dos demais condôminos; <br>
        IV- embaraçar o uso das partes comuns. <br>
        § 1º O transgressor ficará sujeito ao pagamento de multa prevista na convenção ou no regulamento do condomínio, além de ser compelido a desfazer a obra ou abster-se da prática do ato, cabendo, ao síndico, com autorização judicial, mandar desmanchá-Ia, à custa do transgressor, se êste não a desfizer no prazo que lhe fôr estipulado. <br>
        § 2º O proprietário ou titular de direito à aquisição de unidade poderá fazer obra que <u>(VETADO)</u> ou modifique sua fachada, se obtiver a aquiescência da unanimidade dos condôminos <br>
        Art. 11. Para efeitos tributários, cada unidade autônoma será tratada como prédio isolado, contribuindo o respectivo condômino, diretamente, com as importâncias relativas aos impostos e taxas federais, estaduais e municipais, na forma dos respectivos lançamentos. <br>
<p class="text-center">CAPíTULO III </p>
<p class="text-center">Das Despesas do Condomínio </p>
        Art. 12. Cada condômino concorrerá nas despesas do condomínio, recolhendo, nos prazos previstos na Convenção, a quota-parte que lhe couber em rateio. <br>
        § 1º Salvo disposição em contrário na Convenção, a fixação da quota no rateio corresponderá à fração ideal de terreno de cada unidade. <br>
        § 2º Cabe ao síndico arrecadar as contribuições competindo-lhe promover, por via executiva, a cobrança judicial das quotas atrasadas. <br>
        § 3º O condômino que não pagar a sua contribuição no prazo fixado na Convenção fica sujeito ao juro moratório de 1% ao mês, e multa de até 20% sôbre o débito, que será atualizado, se o estipular a Convenção, com a aplicação dos índices de correção monetária levantados pelo Conselho Nacional de Economia, no caso da mora por período igual ou superior a seis meses. <br>
        § 4º As obras que interessarem à estrutura integral da edificação ou conjunto de edificações, ou ao serviço comum, serão feitas com o concurso pecuniário de todos os proprietários ou titulares de direito à aquisição de unidades, mediante orçamento prévio aprovado em assembléia-geral, podendo incumbir-se de sua execução o síndico, ou outra pessoa, com aprovação da assembléia. <br>
        § 5º A renúncia de qualquer condômino aos seus direitos, em caso algum valerá como escusa para exonerá-lo de seus encargos. <br>
<p class="text-center">Capítulo IV </p>
<p class="text-center">Do Seguro,do Incêndio, da Demolição e da Reconstrução Obrigatória </p>
        Art. 13. Proceder-se-á ao seguro da edificação ou do conjunto de edificações, neste caso, discriminadamente, abrangendo tôdas as unidades autônomas e partes comuns, contra incêndio ou outro sinistro que cause destruição no todo ou em parte, computando-se o prêmio nas despesas ordinárias do condomínio. <br>
        Parágrafo único. O seguro de que trata êste artigo será obrigatòriamente feito dentro de 120 dias, contados da data da concessão do &quot;habite-se&quot;, sob pena de ficar o condomínio sujeito à multa mensal equivalente a 1/12 do impôsto predial, cobrável executivamente pela Municipalidade. <br>
        Art. 14. Na ocorrência de sinistro total, ou que destrua mais de dois terços de uma edificação, seus condôminos reunir-se-ão em assembléia especial, e deliberarão sôbre a sua reconstrução ou venda do terreno e materiais, por quorum mínimo de votos que representem metade, mais uma das frações ideais do respectivo terreno. <br>
        § 1º Rejeitada a proposta de reconstrução, a mesma assembléia, ou outra para êste fim convocada, decidirá, pelo mesmo quorum, do destino a ser dado ao terreno, e aprovará a partilha do valor do seguro entre os condôminos, sem prejuízo do que receber cada um pelo seguro facultativo de sua unidade. <br>
        § 2º Aprovada, a reconstrução será feita, guardados, obrigatòriamente, o mesmo destino, a mesma forma externa e a mesma disposição interna. <br>
        § 3º Na hipótese do parágrafo anterior, a minoria não poderá ser obrigada a contribuir para a reedificação, caso em que a maioria poderá adquirir as partes dos dissidentes, mediante avaliação judicial, feita em vistoria. <br>
        Art. 15. Na hipótese de que trata o § 3º do artigo antecedente, à maioria poderão ser adjudicadas, por sentença, as frações ideais da minoria. <br>
        § 1º Como condição para o exercício da ação prevista neste artigo, com a inicial, a maioria oferecerá e depositará, à disposição do Juízo, as importâncias arbitradas na vistoria para avaliação, prevalecendo as de eventual desempatador. <br>
        § 2º Feito o depósito de que trata o parágrafo anterior, o Juiz, liminarmente, poderá autorizar a adjudicação à maioria, e a minoria poderá levantar as importâncias depositadas; o Oficial de Registro de Imóveis, nestes casos, fará constar do registro que a adjudicação foi resultante de medida liminar. <br>
        § 3º Feito o depósito, será expedido o mandado de citação, com o prazo de dez dias para a contestação, <u>VETADO</u>. <br>
        § 4º Se não contestado, o Juiz, imediatamente, julgará o pedido. <br>
        § 5º Se contestado o pedido, seguirá o processo o rito ordinário. <br>
        § 6º Se a sentença fixar valor superior ao da avaliação feita na vistoria, o condomínio em execução restituirá à minoria a respectiva diferença, acrescida de juros de mora à prazo de 1% ao mês, desde a data da concessão de eventual Iiminar, ou pagará o total devido, com os juros da mora a conter da citação. <br>
        § 7º Transitada em julgado a sentença, servirá ela de título definitivo para a maioria, que deverá registrá-la no Registro de Imóveis. <br>
        § 8º A maioria poderá pagar e cobrar da minoria, em execução de sentença, encargos fiscais necessários à adjudicação definitiva a cujo pagamento se recusar a minoria. <br>
        Art. 16. Em caso de sinistro que destrua menos de dois têrços da edificação, o síndico promoverá o recebimento do seguro e a reconstrução ou os reparos nas partes danificadas. <br>
<s>Art. 17. Em caso de condenação da edificação pela autoridade pública, ou ameaça de ruína, pelo voto dos condôminos que representem mais de dois têrços das quotas ideais do respectivo terreno poderá ser decidida a sua demolição e reconstrução.<br />
  Parágrafo único. A minoria não fica obrigada a contribuir para as obras, mas assegura-se a maioria o direito de adquirir as partes dos dissidentes, mediante avaliação judicial, aplicando-se o processo previsto no art. 15. </s><br>
        Art. 17. Os condôminos que representem, pelo menos 2/3 (dois terços) do total de unidades isoladas e frações ideais correspondentes a 80% (oitenta por cento) do terreno e coisas comuns poderão decidir sobre a demolição e reconstrução do prédio, ou sua alienação, por motivos urbanísticos ou arquitetônicos, ou, ainda, no caso de condenação do edifício pela autoridade pública, em razão de sua insegurança ou insalubridade.  <u>(Redação dada pela Lei nº 6.709, de 31.10.1979)</u><br>
       § 1º A minoria não fica obrigada a contribuir para as obras, mas assegura-se à maioria o direito de adquirir as partes dos dissidentes, mediante avaliação judicial, aplicando-se o processo previsto no art. 15. <br>
       § 2º Ocorrendo desgaste, pela ação do tempo, das unidades habitacionais de uma edificação, que deprecie seu valor unitário em relação ao valor global do terreno onde se acha construída, os condôminos, pelo quorum mínimo de votos que representem 2/3 (dois terços) das unidades isoladas e frações ideais correspondentes a 80% (oitenta por cento) do terreno e coisas comuns, poderão decidir por sua alienação total, procedendo-se em relação à minoria na forma estabelecida no art. 15, e seus parágrafos, desta Lei. <br>
      § 3º Decidida por maioria a alienação do prédio, o valor atribuído à quota dos condôminos vencidos será correspondente ao preço efetivo, e, no mínimo, à avaliação prevista no § 2º ou, a critério desses, a imóvel localizado em área próxima ou adjacente com a mesma área útil de construção.&quot; <br>
<s>Art. 18. Em caso de desapropriação parcial de uma edificação ou de um conjunto de edificações, serão indenizados os proprietários das unidades expropriadas, ingressando no condomínio a entidade expropriante, que se sujeitará às disposições desta Lei e se submeterá às da Convenção do condomínio e do Regulamento Interno.<br />
  Parágrafo único.</s> <u>(VETADO)</u><br>
<s>Art. 18. A desapropriação de edificações ou conjuntos de edificações abrangerá sempre a sua totalidade, com todas as suas dependências, indenizando-se os proprietários das unidades expropriadas. <u>(Redação dada pela Lei nº 4.864, de 29.11.1965)</u></s><br>
Art. 18. A aquisição parcial de uma edificação, ou de um conjunto de edificações, ainda que por fôrça de desapropriação, importará no ingresso do adquirente no condomínio, ficando sujeito às disposições desta lei, bem assim às da convenção do condomínio e do regulamento interno. <u>(Redação dada pela Decreto-Lei nº 981, de 21.10.1969)</u><br>
<p class="text-center">Capítulo V </p>
<p class="text-center">Utilização da Edificação ou do Conjunto de Edificações </p>
        Art. 19. Cada condômino tem o direito de usar e fruir, com exclusividade, de sua unidade autônoma, segundo suas conveniências e interêsses, condicionados, umas e outros às normas de boa vizinhança, e poderá usar as partes e coisas comuns de maneira a não causar dano ou incômodo aos demais condôminos ou moradores, nem obstáculo ou embaraço ao bom uso das mesmas partes por todos. <br>
        Parágrafo único. <u>(VETADO)</u>. <br>
        Art. 20. Aplicam-se ao ocupante do imóvel, a qualquer título, tôdas as obrigações referentes ao uso, fruição e destino da unidade. <br>
        Art. 21. A violação de qualquer dos deveres estipulados na Convenção sujeitará o infrator à multa fixada na própria Convenção ou no Regimento Interno, sem prejuízo da responsabilidade civil ou criminal que, no caso, couber. <br>
        Parágrafo único. Compete ao síndico a iniciativa do processo e a cobrança da multa, por via executiva, em benefício do condomínio, e, em caso de omitir-se êle, a qualquer condômino. <br>
<p class="text-center">Capítulo VI </p>
<p class="text-center">Da Administração do Condomínio </p>
        Art. 22. Será eleito, na forma prevista pela Convenção, um síndico do condomínio, cujo mandato não poderá exceder de 2 anos, permitida a reeleição. <br>
        § 1º Compete ao síndico: <br>
        a) representar ativa e passivamente, o condomínio, em juízo ou fora dêle, e praticar os atos de defesa dos interêsses comuns, nos limites das atribuições conferidas por esta Lei ou pela Convenção; <br>
        b) exercer a administração interna da edificação ou do conjunto de edificações, no que respeita à sua vigência, moralidade e segurança, bem como aos serviços que interessam a todos os moradores; <br>
        c) praticar os atos que lhe atribuírem as leis a Convenção e o Regimento Interno; <br>
        d) impor as multas estabelecidas na Lei, na Convenção ou no Regimento Interno; <br>
        e) cumprir e fazer cumprir a Convenção e o Regimento Interno, bem como executar e fazer executar as deliberações da assembléia; <br>
        f) prestar contas à assembléia dos condôminos. <br>
        g) manter guardada durante o prazo de cinco anos para eventuais necessidade de verificação contábil, toda a documentação relativa ao condomínio. <u>(Alínea incluída pela Lei nº 6.434, de 15.7.1977)</u><br>
        § 2º As funções administrativas podem ser delegadas a pessoas de confiança do síndico, e sob a sua inteira responsabilidade, mediante aprovação da assembléia geral dos condôminos. <br>
        § 3º A Convenção poderá estipular que dos atos do síndico caiba recurso para a assembléia, convocada pelo interessado. <br>
        § 4º Ao síndico, que poderá ser condômino ou pessoa física ou jurídica estranha ao condomínio, será fixada a remuneração pela mesma assembléia que o eleger, salvo se a Convenção dispuser diferentemente. <br>
        § 5º O síndico poderá ser destituído, pela forma e sob as condições previstas na Convenção, ou, no silêncio desta pelo voto de dois têrços dos condôminos, presentes, em assembléia-geral especialmente convocada. <br>
        § 6º A Convenção poderá prever a eleição de subsíndicos, definindo-lhes atribuições e fixando-lhes o mandato, que não poderá exceder de 2 anos, permitida a reeleição. <br>
        Art. 23. Será eleito, na forma prevista na Convenção, um Conselho Consultivo, constituído de três condôminos, com mandatos que não poderão exceder de 2 anos, permitida a reeleição. <br>
        Parágrafo único. Funcionará o Conselho como órgão consultivo do síndico, para assessorá-lo na solução dos problemas que digam respeito ao condomínio, podendo a Convenção definir suas atribuições específicas. <br>
<p class="text-center">Capítulo VII </p>
<p class="text-center">Da Assembléia Geral </p>
        Art. 24. Haverá, anualmente, uma assembléia geral ordinária dos condôminos, convocada pelo síndico na forma prevista na Convenção, à qual compete, além das demais matérias inscritas na ordem do dia, aprovar, por maioria dos presentes, as verbas para as despesas de condomínio, compreendendo as de conservação da edificação ou conjunto de edificações, manutenção de seus serviços e correlatas. <br>
        § 1º As decisões da assembléia, tomadas, em cada caso, pelo quorum que a Convenção fixar, obrigam todos os condôminos. <br>
        § 2º O síndico, nos oito dias subseqüentes à assembléia, comunicará aos condôminos o que tiver sido deliberado, inclusive no tocante à previsão orçamentária, o rateio das despesas, e promoverá a arrecadação, tudo na forma que a Convenção previr. <br>
        § 3º Nas assembléias gerais, os votos serão proporcionais às frações ideais do terreno e partes comuns, pertencentes a cada condômino, salvo disposição diversa da Convenção. <br>
        <s>§ 4° Nas decisões da assembléia que envolvam despesas ordinárias do condomínio, o locatário poderá votar, caso o condômino locador a ela não compareça. <u>(Parágrafo incluído pela Lei nº 8.245, de 18.10.1991)</u></s><br>
        § 4º Nas decisões da Assembléia que não envolvam despesas extraordinárias do condomínio, o locatário poderá votar, caso o condômino-locador a ela não compareça. <u>(Redação dada pela Lei nº 9.267, de 25.3.1996)</u><br>
        Art. 25. Ressalvado o disposto no § 3º do art. 22, poderá haver assembléias gerais extraordinárias, convocadas pelo síndico ou por condôminos que representem um quarto, no mínimo do condomínio, sempre que o exigirem os interêsses gerais. <br>
        Parágrafo único. Salvo estipulação diversa da Convenção, esta só poderá ser modificada em assembléia geral extraordinária, pelo voto mínimo de condôminos que representem 2/3 do total das frações ideais. <br>
        Art. 26. <u>(VETADO)</u>. <br>
        Art. 27. Se a assembléia não se reunir para exercer qualquer dos podêres que lhe competem, 15 dias após o pedido de convocação, o Juiz decidirá a respeito, mediante requerimento dos interessados. <br>
<p class="text-center">Título II </p>
<p class="text-center">DAS INCORPORAÇÕES </p>
<p class="text-center">CAPÍTULO I </p>
<p class="text-center">Disposições Gerais </p>
        Art. 28. <br>
       ...<br>
       Art. 70 <br>
        Brasília, 16 de dezembro de 1964; 143º da Independência e 76º da República. <br>
            H. CASTELLO BRANCO<br />
  <em>            Milton Soares Campos</em><br>

                  </div>
                  
                  <br>
                  
                  <div class="grid-element" id="lei2">
                      <h6>Lei do Inquilinato</h6>
<p class="text-center"><strong>LEI Nº8245, DE 18 DE OUTUBRO DE 1991</strong></p>
<p class="text-center">LEI DO INQUILINATO</p>
<p class="text-center">(Artigos que se relacionam com Condomínio)</p>
<p class="text-center">LEI Nº8245, DE 18 DE OUTUBRO DE 1991</p>
<p class="text-center">LEI DO INQUILINATO</p>
<p class="text-center">(Artigos que se relacionam com Condomínio)</p>
<br>
Artigo 1 A locação de imóvel urbano regula-se pelo disposto nesta lei.<br>
Parágrafo único Continuam regulados pelo Código Civil e pelas leis especiais:<br>
a) as locações:<br>
1. De imóveis de propriedade da União, dos Estados e dos Municípios, de suas autarquias e fundações públicas;<br>
2. De vagas autônomas de garagem ou de espaços para estacionamento de veículos;<br>
3. De espaços destinados à publicidade;<br>
4. Em &quot;apart-hotéis&quot;, hotéis-residência ou equiparados, assim considerados aqueles que prestam serviços regulares a seus usuários e como tais sejam autorizados a funcionar;<br>
b) o arrendamento mercantil, em qualquer de suas modalidades.<br>
 <br>
. . . .<br>
 <br>
Artigo 22 O locador é obrigado a:<br>
 <br>
I - entregar ao locatário o imóvel alugado em estado de servir ao uso a que se destina;<br>
II - garantir, durante o tempo da locação, o uso pacífico do imóvel locado;<br>
III - manter, durante a locação, a forma e o destino do imóvel;<br>
IV - responder pelos vícios ou defeitos anteriores à locação;<br>
V - fornecer ao locatário, caso este solicite, descrição minuciosa do estado do imóvel, quando de sua entrega, com expressa referência aos eventuais defeitos existentes;<br>
VI - fornecer ao locatário recibo discriminado das importâncias por este pagas, vedada a quitação genérica;<br>
VII - pagar as taxas de administração imobiliária, se houver, e de intermediações, nestas compreendidas as despesas necessárias à aferição da idoneidade do pretendente ou de seu fiador;<br>
VIII - pagar os impostos e taxas, e ainda o prêmio de seguro complementar contra fogo, que incidam ou venham a incidir sobre o imóvel, salvo disposição expressa em contrário no contrato;<br>
IX - exibir ao locatário, quando solicitado, os comprovantes relativos às parcelas que estejam sendo exigidas;<br>
X - pagar as despesas extraordinárias de condomínio.<br>
 <br>
Parágrafo único Por despesas extraordinárias de condomínio se entendem aquelas que não se refiram aos gastos rotineiros de manutenção do edifício, especialmente:<br>
a) obras de reformas ou acréscimos que interessem à estrutura integral do imóvel;<br>
b) pintura das fachadas, empenas, poços de aeração e iluminação, bem como das esquadrias externas;<br>
c) obras destinadas a repor as condições de habitabilidade do edifício;<br>
d) indenizações trabalhistas e previdenciárias pela dispensa de empregados, ocorridas em data anterior ao início da locação;<br>
e) instalação de equipamentos de segurança e de incêndio, de telefonia, de intercomunicação, de esporte e de lazer;<br>
f) despesas de decoração e paisagismo nas partes de uso comum;<br>
g) constituição de fundo de reserva.<br>
 <br>
 <br>
Artigo 23 O locatário é obrigado a:<br>
I - pagar pontualmente o aluguel e os encargos da locação, legal ou contratualmente exigíveis, no prazo estipulado ou, em sua falta, até o sexto dia útil do mês seguinte ao vencido, no imóvel locado, quando outro local não tiver sido indicado no contrato;<br>
II - servir-se do imóvel para o uso convencionado ou presumido, compatível com a natureza deste e com o fim a que se destina, devendo tratá-lo com o mesmo cuidado como se fosse seu;<br>
III - restituir o imóvel, finda a locação, no estado em que o recebeu, salvo as deteriorações decorrentes do seu uso normal;<br>
IV - levar imediatamente ao conhecimento do locador o surgimento de qualquer dano ou defeito cuja reparação a este incumba, bem como as eventuais turbações de terceiros;<br>
V - realizar a imediata reparação dos danos verificados no imóvel, ou nas suas instalações, provocados por si, seus dependentes, familiares, visitantes ou prepostos;<br>
VI - não modificar a forma interna ou externa do imóvel sem o consentimento prévio e por escrito do locador;<br>
VII - entregar imediatamente ao locador os documentos de cobrança de tributos e encargos condominiais, bem como qualquer intimação, multa ou exigência de autoridade pública, ainda que dirigida a ele, locatário;<br>
VIII - pagar as despesas de telefone e de consumo de força, luz e gás, água e esgoto;<br>
IX - permitir a vistoria do imóvel pelo locador ou por seu mandatário, mediante combinação prévia de dia e hora, bem como admitir que seja o mesmo visitado e examinado por terceiros, na hipótese prevista no art.27;<br>
X - cumprir integralmente a convenção de condomínio e os regulamentos internos;<br>
XI - pagar o prêmio do seguro de fiança;<br>
XII - pagar as despesas ordinárias de condomínio;<br>
 <br>
PAR.1º Por despesas ordinárias de condomínio se entendem as necessárias à administração respectiva, especialmente:<br>
a) salários, encargos trabalhistas, contribuições previdenciárias e sociais dos empregados do condomínio;<br>
b) consumo de água e esgoto, gás, luz e força das áreas de uso comum;<br>
c) limpeza, conservação e pintura das instalações e dependências de uso comum;<br>
d) manutenção e conservação das instalações e equipamentos hidráulicos, elétricos, mecânicos e de segurança, de uso comum;<br>
e) manutenção e conservação das instalações e equipamentos de uso comum destinados à prática de esportes e lazer;<br>
f) manutenção e conservação de elevadores, porteiro eletrônico e antenas coletivas;<br>
g) pequenos reparos nas dependências e instalações elétricas e hidráulicas de uso comum;<br>
h) rateios de saldo devedor, salvo se referentes a período anterior ao início da locação;<br>
i) reposição de fundo de reserva, total ou parcialmente utilizado no custeio ou complementação das despesas referidas nas alíneas anteriores, salvo se referentes a período anterior ao início da locação.<br>
PAR.2º O locatário fica obrigado ao pagamento das despesas referidas no parágrafo anterior, desde que comprovadas a previsão orçamentária e o rateio mensal, podendo exigir a qualquer tempo a comprovação das mesmas.<br>
PAR.3º No edifício constituído por unidades imobiliárias autônomas, de propriedade da mesma pessoa, os locatários ficam obrigados ao pagamento das despesas referidas no §1º deste artigo, desde que comprovadas.<br>
 <br>
. . . .<br>
 <br>
Artigo 34 Havendo condomínio no imóvel, a preferência do condômino terá prioridade sobre a do locatário.<br>
 <br>
. . . .<br>
 <br>
Artigo 54 Nas relações entre lojistas e empreendedores de &quot;shopping-center&quot;, prevalecerão as condições livremente pactuadas nos contratos de locação respectivos e as disposições procedimentais previstas nesta lei.<br>
PAR.1º O empreendedor não poderá cobrar do locatário em &quot;shopping-center&quot;:<br>
a) as despesas referidas nas alíneas &quot;a&quot;, &quot;b&quot; e &quot;d&quot; do parágrafo único do art.22; e<br>
b) as despesas com obras ou substituições de equipamentos, que impliquem modificar o projeto ou o memorial descritivo da data do habite-se e obras de paisagismo nas partes de uso comum.<br>
PAR.2º As despesas cobradas do locatário devem ser previstas em orçamento, salvo casos de urgência ou força maior, devidamente demonstradas, podendo o locatário, a cada sessenta dias, por si ou entidade de classe exigir a comprovação das mesmas.<br>
 <br>
. . . .<br>
 <br>
Artigo 83 Ao art.24 da Lei nº4.591, de 16 de dezembro de 1964, fica acrescido o seguinte §4º:<br>
&quot;Art.24 ..............................................................<br>
PAR.4º Nas decisões da assembléia que envolvam despesas ordinárias do condomínio, o locatário poderá votar, caso o condômino-locador a ela não compareça&quot;.<br>
 <br>
. . . .<br>
                  </div>
                  
                  <br>
                  
                  <div class="grid-element" id="lei3">
                      <h6>Lei 10.406/2002 - Código Civil</h6>
<p class="text-center"><strong>Código Civil</strong></p>
<p class="text-center"><strong>LEI Nº 10.406 de 10 de Janeiro de 2002</strong></p>
<p align="justify"> <br>
... assuntos da Lei relativos aos Condomínios<br>
<br>
<p class="text-center">CAPÍTULO VI</p>
<p class="text-center">Do Condomínio Geral</p>
<p class="text-center">Seção I</p>
<p class="text-center">Do Condomínio Voluntário</p>
<p class="text-center">Subseção I</p>
<p class="text-center">Dos Direitos e Deveres dos Condôminos</p>
Art. 1.314. Cada condômino pode usar da coisa conforme sua destinação, sobre ela exercer todos os direitos compatíveis com a indivisão, reivindicá-la de terceiro, defender a sua posse e alhear a respectiva parte ideal, ou gravá-la.<br>
Parágrafo único. Nenhum dos condôminos pode alterar a destinação da coisa comum, nem dar posse, uso ou gozo dela a estranhos, sem o consenso dos outros.<br>
Art. 1.315. O condômino é obrigado, na proporção de sua parte, a concorrer para as despesas de conservação ou divisão da coisa, e a suportar os ônus a que estiver sujeita.<br>
Parágrafo único. Presumem-se iguais as partes ideais dos condôminos.<br>
Art. 1.316. Pode o condômino eximir-se do pagamento das despesas e dívidas, renunciando à parte ideal.<br>
§ 1o Se os demais condôminos assumem as despesas e as dívidas, a renúncia lhes aproveita, adquirindo a parte ideal de quem renunciou, na proporção dos pagamentos que fizerem.<br>
§ 2o Se não há condômino que faça os pagamentos, a coisa comum será dividida.<br>
Art. 1.317. Quando a dívida houver sido contraída por todos os condôminos, sem se discriminar a parte de cada um na obrigação, nem se estipular solidariedade, entende-se que cada qual se obrigou proporcionalmente ao seu quinhão na coisa comum.<br>
Art. 1.318. As dívidas contraídas por um dos condôminos em proveito da comunhão, e durante ela, obrigam o contratante; mas terá este ação regressiva contra os demais.<br>
Art. 1.319. Cada condômino responde aos outros pelos frutos que percebeu da coisa e pelo dano que lhe causou.<br>
Art. 1.320. A todo tempo será lícito ao condômino exigir a divisão da coisa comum, respondendo o quinhão de cada um pela sua parte nas despesas da divisão.<br>
§ 1o Podem os condôminos acordar que fique indivisa a coisa comum por prazo não maior de cinco anos, suscetível de prorrogação ulterior.<br>
§ 2o Não poderá exceder de cinco anos a indivisão estabelecida pelo doador ou pelo testador.<br>
§ 3o A requerimento de qualquer interessado e se graves razões o aconselharem, pode o juiz determinar a divisão da coisa comum antes do prazo.<br>
Art. 1.321. Aplicam-se à divisão do condomínio, no que couber, as regras de partilha de herança (arts. 2.013 a 2.022).<br>
Art. 1.322. Quando a coisa for indivisível, e os consortes não quiserem adjudicá-la a um só, indenizando os outros, será vendida e repartido o apurado, preferindo-se, na venda, em condições iguais de oferta, o condômino ao estranho, e entre os condôminos aquele que tiver na coisa benfeitorias mais valiosas, e, não as havendo, o de quinhão maior.<br>
Parágrafo único. Se nenhum dos condôminos tem benfeitorias na coisa comum e participam todos do condomínio em partes iguais, realizar-se-á licitação entre estranhos e, antes de adjudicada a coisa àquele que ofereceu maior lanço, proceder-se-á à licitação entre os condôminos, a fim de que a coisa seja adjudicada a quem afinal oferecer melhor lanço, preferindo, em condições iguais, o condômino ao estranho.<br>
Subseção II Da Administração do Condomínio<br>
Art. 1.323. Deliberando a maioria sobre a administração da coisa comum, escolherá o administrador, que poderá ser estranho ao condomínio; resolvendo alugá-la, preferir-se-á, em condições iguais, o condômino ao que não o é.<br>
Art. 1.324. O condômino que administrar sem oposição dos outros presume-se representante comum.<br>
Art. 1.325. A maioria será calculada pelo valor dos quinhões.<br>
§ 1o As deliberações serão obrigatórias, sendo tomadas por maioria absoluta.<br>
§ 2o Não sendo possível alcançar maioria absoluta, decidirá o juiz, a requerimento de qualquer condômino, ouvidos os outros.<br>
§ 3o Havendo dúvida quanto ao valor do quinhão, será este avaliado judicialmente.<br>
Art. 1.326. Os frutos da coisa comum, não havendo em contrário estipulação ou disposição de última vontade, serão partilhados na proporção dos quinhões.<br>
Seção II Do Condomínio Necessário<br>
Art. 1.327. O condomínio por meação de paredes, cercas, muros e valas regula-se pelo disposto neste Código (arts. 1.297 e 1.298; 1.304 a 1.307).<br>
Art. 1.328. O proprietário que tiver direito a estremar um imóvel com paredes, cercas, muros, valas ou valados, tê-lo-á igualmente a adquirir meação na parede, muro, valado ou cerca do vizinho, embolsando-lhe metade do que atualmente valer a obra e o terreno por ela ocupado (art. 1.297).<br>
Art. 1.329. Não convindo os dois no preço da obra, será este arbitrado por peritos, a expensas de ambos os confinantes.<br>
Art. 1.330. Qualquer que seja o valor da meação, enquanto aquele que pretender a divisão não o pagar ou depositar, nenhum uso poderá fazer na parede, muro, vala, cerca ou qualquer outra obra divisória.<br>
CAPÍTULO VII Do Condomínio Edilício<br>
Seção I Disposições Gerais<br>
Art. 1.331. Pode haver, em edificações, partes que são propriedade exclusiva, e partes que são propriedade comum dos condôminos.<br>
§ 1o As partes suscetíveis de utilização independente, tais como apartamentos, escritórios, salas, lojas, sobrelojas ou abrigos para veículos, com as respectivas frações ideais no solo e nas outras partes comuns, sujeitam-se a propriedade exclusiva, podendo ser alienadas e gravadas livremente por seus proprietários.<br>
§ 2o O solo, a estrutura do prédio, o telhado, a rede geral de distribuição de água, esgoto, gás e eletricidade, a calefação e refrigeração centrais, e as demais partes comuns, inclusive o acesso ao logradouro público, são utilizados em comum pelos condôminos, não podendo ser alienados separadamente, ou divididos.<br>
<s>§ 3o A fração ideal no solo e nas outras partes comuns é proporcional ao valor da unidade imobiliária, o qual se calcula em relação ao conjunto da edificação.</s><br>
§ 3o A cada unidade imobiliária caberá, como parte inseparável, uma fração ideal no solo e nas outras partes comuns, que será identificada em forma decimal ou ordinária no instrumento de instituição do condomínio. <u>(Redação dada pela Lei nº 10.931, de 2004)</u><br>
§ 4o Nenhuma unidade imobiliária pode ser privada do acesso ao logradouro público.<br>
§ 5o O terraço de cobertura é parte comum, salvo disposição contrária da escritura de constituição do condomínio.<br>
Art. 1.332. Institui-se o condomínio edilício por ato entre vivos ou testamento, registrado no Cartório de Registro de Imóveis, devendo constar daquele ato, além do disposto em lei especial:<br>
I - a discriminação e individualização das unidades de propriedade exclusiva, estremadas uma das outras e das partes comuns;<br>
II - a determinação da fração ideal atribuída a cada unidade, relativamente ao terreno e partes comuns;<br>
III - o fim a que as unidades se destinam.<br>
Art. 1.333. A convenção que constitui o condomínio edilício deve ser subscrita pelos titulares de, no mínimo, dois terços das frações ideais e torna-se, desde logo, obrigatória para os titulares de direito sobre as unidades, ou para quantos sobre elas tenham posse ou detenção.<br>
Parágrafo único. Para ser oponível contra terceiros, a convenção do condomínio deverá ser registrada no Cartório de Registro de Imóveis.<br>
Art. 1.334. Além das cláusulas referidas no art. 1.332 e das que os interessados houverem por bem estipular, a convenção determinará:<br>
I - a quota proporcional e o modo de pagamento das contribuições dos condôminos para atender às despesas ordinárias e extraordinárias do condomínio;<br>
II - sua forma de administração;<br>
III - a competência das assembléias, forma de sua convocação e quorum exigido para as deliberações;<br>
IV - as sanções a que estão sujeitos os condôminos, ou possuidores;<br>
V - o regimento interno.<br>
§ 1o A convenção poderá ser feita por escritura pública ou por instrumento particular.<br>
§ 2o São equiparados aos proprietários, para os fins deste artigo, salvo disposição em contrário, os promitentes compradores e os cessionários de direitos relativos às unidades autônomas.<br>
Art. 1.335. São direitos do condômino:<br>
I - usar, fruir e livremente dispor das suas unidades;<br>
II - usar das partes comuns, conforme a sua destinação, e contanto que não exclua a utilização dos demais compossuidores;<br>
III - votar nas deliberações da assembléia e delas participar, estando quite.<br>
Art. 1.336. São deveres do condômino:<br>
<s>I - Contribuir para as despesas do condomínio, na proporção de suas frações ideais;</s><br>
I - contribuir para as despesas do condomínio na proporção das suas frações ideais, salvo disposição em contrário na convenção; <u>(Redação dada pela Lei nº 10.931, de 2004)</u><br>
II - não realizar obras que comprometam a segurança da edificação;<br>
III - não alterar a forma e a cor da fachada, das partes e esquadrias externas;<br>
IV - dar às suas partes a mesma destinação que tem a edificação, e não as utilizar de maneira prejudicial ao sossego, salubridade e segurança dos possuidores, ou aos bons costumes.<br>
§ 1o O condômino que não pagar a sua contribuição ficará sujeito aos juros moratórios convencionados ou, não sendo previstos, os de um por cento ao mês e multa de até dois por cento sobre o débito.<br>
§ 2o O condômino, que não cumprir qualquer dos deveres estabelecidos nos incisos II a IV, pagará a multa prevista no ato constitutivo ou na convenção, não podendo ela ser superior a cinco vezes o valor de suas contribuições mensais, independentemente das perdas e danos que se apurarem; não havendo disposição expressa, caberá à assembléia geral, por dois terços no mínimo dos condôminos restantes, deliberar sobre a cobrança da multa.<br>
Art. 1337. O condômino, ou possuidor, que não cumpre reiteradamente com os seus deveres perante o condomínio poderá, por deliberação de três quartos dos condôminos restantes, ser constrangido a pagar multa correspondente até ao quíntuplo do valor atribuído à contribuição para as despesas condominiais, conforme a gravidade das faltas e a reiteração, independentemente das perdas e danos que se apurem.<br>
Parágrafo único. O condômino ou possuidor que, por seu reiterado comportamento anti-social, gerar incompatibilidade de convivência com os demais condôminos ou possuidores, poderá ser constrangido a pagar multa correspondente ao décuplo do valor atribuído à contribuição para as despesas condominiais, até ulterior deliberação da assembléia.<br>
Art. 1.338. Resolvendo o condômino alugar área no abrigo para veículos, preferir-se-á, em condições iguais, qualquer dos condôminos a estranhos, e, entre todos, os possuidores.<br>
Art. 1.339. Os direitos de cada condômino às partes comuns são inseparáveis de sua propriedade exclusiva; são também inseparáveis das frações ideais correspondentes as unidades imobiliárias, com as suas partes acessórias.<br>
§ 1o Nos casos deste artigo é proibido alienar ou gravar os bens em separado.<br>
§ 2o É permitido ao condômino alienar parte acessória de sua unidade imobiliária a outro condômino, só podendo fazê-lo a terceiro se essa faculdade constar do ato constitutivo do condomínio, e se a ela não se opuser a respectiva assembléia geral.<br>
Art. 1.340. As despesas relativas a partes comuns de uso exclusivo de um condômino, ou de alguns deles, incumbem a quem delas se serve.<br>
Art. 1.341. A realização de obras no condomínio depende:<br>
I - se voluptuárias, de voto de dois terços dos condôminos;<br>
II - se úteis, de voto da maioria dos condôminos.<br>
§ 1o As obras ou reparações necessárias podem ser realizadas, independentemente de autorização, pelo síndico, ou, em caso de omissão ou impedimento deste, por qualquer condômino.<br>
§ 2o Se as obras ou reparos necessários forem urgentes e importarem em despesas excessivas, determinada sua realização, o síndico ou o condômino que tomou a iniciativa delas dará ciência à assembléia, que deverá ser convocada imediatamente.<br>
§ 3o Não sendo urgentes, as obras ou reparos necessários, que importarem em despesas excessivas, somente poderão ser efetuadas após autorização da assembléia, especialmente convocada pelo síndico, ou, em caso de omissão ou impedimento deste, por qualquer dos condôminos.<br>
§ 4o O condômino que realizar obras ou reparos necessários será reembolsado das despesas que efetuar, não tendo direito à restituição das que fizer com obras ou reparos de outra natureza, embora de interesse comum.<br>
Art. 1.342. A realização de obras, em partes comuns, em acréscimo às já existentes, a fim de lhes facilitar ou aumentar a utilização, depende da aprovação de dois terços dos votos dos condôminos, não sendo permitidas construções, nas partes comuns, suscetíveis de prejudicar a utilização, por qualquer dos condôminos, das partes próprias, ou comuns.<br>
Art. 1.343. A construção de outro pavimento, ou, no solo comum, de outro edifício, destinado a conter novas unidades imobiliárias, depende da aprovação da unanimidade dos condôminos.<br>
Art. 1.344. Ao proprietário do terraço de cobertura incumbem as despesas da sua conservação, de modo que não haja danos às unidades imobiliárias inferiores.<br>
Art. 1.345. O adquirente de unidade responde pelos débitos do alienante, em relação ao condomínio, inclusive multas e juros moratórios.<br>
Art. 1.346. É obrigatório o seguro de toda a edificação contra o risco de incêndio ou destruição, total ou parcial.<br>
Seção II Da Administração do Condomínio<br>
Art. 1.347. A assembléia escolherá um síndico, que poderá não ser condômino, para administrar o condomínio, por prazo não superior a dois anos, o qual poderá renovar-se.<br>
Art. 1.348. Compete ao síndico:<br>
I - convocar a assembléia dos condôminos;<br>
II - representar, ativa e passivamente, o condomínio, praticando, em juízo ou fora dele, os atos necessários à defesa dos interesses comuns;<br>
III - dar imediato conhecimento à assembléia da existência de procedimento judicial ou administrativo, de interesse do condomínio;<br>
IV - cumprir e fazer cumprir a convenção, o regimento interno e as determinações da assembléia;<br>
V - diligenciar a conservação e a guarda das partes comuns e zelar pela prestação dos serviços que interessem aos possuidores;<br>
VI - elaborar o orçamento da receita e da despesa relativa a cada ano;<br>
VII - cobrar dos condôminos as suas contribuições, bem como impor e cobrar as multas devidas;<br>
VIII - prestar contas à assembléia, anualmente e quando exigidas;<br>
IX - realizar o seguro da edificação.<br>
§ 1o Poderá a assembléia investir outra pessoa, em lugar do síndico, em poderes de representação.<br>
§ 2o O síndico pode transferir a outrem, total ou parcialmente, os poderes de representação ou as funções administrativas, mediante aprovação da assembléia, salvo disposição em contrário da convenção.<br>
Art. 1.349. A assembléia, especialmente convocada para o fim estabelecido no § 2o do artigo antecedente, poderá, pelo voto da maioria absoluta de seus membros, destituir o síndico que praticar irregularidades, não prestar contas, ou não administrar convenientemente o condomínio.<br>
Art. 1.350. Convocará o síndico, anualmente, reunião da assembléia dos condôminos, na forma prevista na convenção, a fim de aprovar o orçamento das despesas, as contribuições dos condôminos e a prestação de contas, e eventualmente eleger-lhe o substituto e alterar o regimento interno.<br>
§ 1o Se o síndico não convocar a assembléia, um quarto dos condôminos poderá fazê-lo.<br>
§ 2o Se a assembléia não se reunir, o juiz decidirá, a requerimento de qualquer condômino.<br>
<s>Art. 1.351. Depende da aprovação de dois terços dos votos dos condôminos a alteração da convenção e do regimento interno; a mudança da destinação do edifício, ou da unidade imobiliária, depende de aprovação pela unanimidade dos condôminos.</s><br>
Art. 1.351. Depende da aprovação de 2/3 (dois terços) dos votos dos condôminos a alteração da convenção; a mudança da destinação do edifício, ou da unidade imobiliária, depende da aprovação pela unanimidade dos condôminos. (Redação dada pela Lei nº 10.931, de 2004)<br>
Art. 1.352. Salvo quando exigido quorum especial, as deliberações da assembléia serão tomadas, em primeira convocação, por maioria de votos dos condôminos presentes que representem pelo menos metade das frações ideais.<br>
Parágrafo único. Os votos serão proporcionais às frações ideais no solo e nas outras partes comuns pertencentes a cada condômino, salvo disposição diversa da convenção de constituição do condomínio.<br>
Art. 1.353. Em segunda convocação, a assembléia poderá deliberar por maioria dos votos dos presentes, salvo quando exigido quorum especial.<br>
Art. 1.354. A assembléia não poderá deliberar se todos os condôminos não forem convocados para a reunião.<br>
Art. 1.355. Assembléias extraordinárias poderão ser convocadas pelo síndico ou por um quarto dos condôminos.<br>
Art. 1.356. Poderá haver no condomínio um conselho fiscal, composto de três membros, eleitos pela assembléia, por prazo não superior a dois anos, ao qual compete dar parecer sobre as contas do síndico.<br>
Seção III Da Extinção do Condomínio<br>
Art. 1.357. Se a edificação for total ou consideravelmente destruída, ou ameace ruína, os condôminos deliberarão em assembléia sobre a reconstrução, ou venda, por votos que representem metade mais uma das frações ideais.<br>
§ 1o Deliberada a reconstrução, poderá o condômino eximir-se do pagamento das despesas respectivas, alienando os seus direitos a outros condôminos, mediante avaliação judicial.<br>
§ 2o Realizada a venda, em que se preferirá, em condições iguais de oferta, o condômino ao estranho, será repartido o apurado entre os condôminos, proporcionalmente ao valor das suas unidades imobiliárias.<br>
Art. 1.358. Se ocorrer desapropriação, a indenização será repartida na proporção a que se refere o § 2o do artigo antecedente.<br>
....<br>
Art. 2.043. Até que por outra forma se disciplinem, continuam em vigor as disposições de natureza processual, administrativa ou penal, constantes de leis cujos preceitos de natureza civil hajam sido incorporados a este Código.<br>
Art. 2.044. Este Código entrará em vigor 1 (um) ano após a sua publicação.<br>
Art. 2.045. Revogam-se a Lei no 3.071, de 1o de janeiro de 1916 - Código Civil e a Parte Primeira do Código Comercial, Lei no 556, de 25 de junho de 1850.<br>
Art. 2.046. Todas as remissões, em diplomas legislativos, aos Códigos referidos no artigo antecedente, consideram-se feitas às disposições correspondentes deste Código.<br>
Brasília, 10 de janeiro de 2002; 181o da Independência e 114o da República.<br>
FERNANDO HENRIQUE CARDOSO<br>
Aloysio Nunes Ferreira Filho<br>
                  </div>
                  
                  <br>
                  
                  <div class="grid-element" id="escritura">
                      <h6>Escritura de Convenção</h6>
                      
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A escritura de convenção de um condomínio é constituída de normas e regras com o objetivo de regular o bem comum e a convivência em coletividade. A convenção deve ser registrada no Cartório de Registro de Imóveis para que produza todos os efeitos necessários definidos na Lei.<br>
                      <br>
                      <div class="group-xl">
                          <a class="btn btn-icon btn-icon-left btn-primary btn-no-shadow" href="<?php echo $this->base; ?>/files/modelos/modelo_escritura_convencao.doc"><span class="icon icon-xs fa-arrow-circle-o-down"></span>MODELO DE ESCRITURA CONVENÇÃO</a>
                      </div>
                  </div>
                  
                  <br>
                  
                  <div class="grid-element" id="regimento">
                      <h6>Regimento Interno</h6>
                      
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O Regimento Interno, também chamado de Regulamento Interno, reúne normas de condutas para os moradores de um Condomínio, definindo multas e infrações para desrespeito às cláusulas nele contidas.<br>
                      <br>
                      <div class="group-xl">
                          <a class="btn btn-icon btn-icon-left btn-primary btn-no-shadow" href="<?php echo $this->base; ?>/files/modelos/modelo_regimento_interno.doc"><span class="icon icon-xs fa-arrow-circle-o-down"></span>MODELO DE REGIMENTO INTERNO</a>
                      </div>
                  </div>
                  
              </div>
            </div>
          </div>
        </section>