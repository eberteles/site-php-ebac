        <section class="section-top-40 section-sm-top-40">
          <div class="shell">
            <div class="range">
              <div class="cell-xs-12">
                <h5>EBAC - Administração de Condomínios</h5>
                <hr>
              </div>
              <div class="cell-xs-12 offset-top-40">
                <div class="range">
                  <div class="cell-xs-6 cell-sm-4">
                    <!-- CountTo-->
                    <div class="counter-wrap"><span class="icon icon-lg icon-primary linecons-like"></span>
                      <div class="counter-value">
                        <p class="h3"><span data-from="0" data-to="100" class="counter">100%</span><span class="symbol">%</span></p>
                      </div>
                      <div class="divider-center divider-md divider-denim"></div>
                      <div class="counter-text">
                        <p class="text-style-2">Satisfação Garantida</p>
                      </div>
                    </div>
                  </div>
                  <div class="cell-xs-6 cell-sm-4 offset-top-45 offset-xs-top-0">
                    <!-- CountTo-->
                    <div class="counter-wrap"><span class="icon icon-lg icon-primary linecons-small58"></span>
                      <div class="counter-value">
                        <p class="h3"><span data-from="0" data-to="27" class="counter">27</span></p>
                      </div>
                      <div class="divider-center divider-md divider-denim"></div>
                      <div class="counter-text">
                        <p class="text-style-2">Anos de Mercado</p>
                      </div>
                    </div>
                  </div>
                  <div class="cell-xs-6 cell-sm-4 offset-top-45 offset-sm-top-0">
                    <!-- CountTo-->
                    <div class="counter-wrap"><span class="icon icon-lg icon-primary linecons-user12"></span>
                      <div class="counter-value">
                        <p class="h3"><span data-from="0" data-to="20000" class="counter">20000</span></p>
                      </div>
                      <div class="divider-center divider-md divider-denim"></div>
                      <div class="counter-text">
                        <p class="text-style-2">Clientes Satisfeitos</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div class="container">
              
            <div class="row">

              <div class="col-xs-12 offset-top-30">
                <!-- Isotope Content-->
                <div data-isotope-layout="masonry" data-isotope-group="gallery" data-photo-swipe-gallery="gallery" class="row isotope isotope-spacing-1">
                  <div data-filter="Category 3" class="col-xs-12 col-sm-6 col-md-4 isotope-item">
                    <div class="thumbnail thumbnail-variant-4">
                      <figure><img src="<?php echo $this->base; ?>/images/conheca/comercial1-menor.jpg" alt="" width="370" height="240"/>
                      </figure><a href="<?php echo $this->base; ?>/images/conheca/comercial1.jpg" data-photo-swipe-item="" data-size="1200x800" class="thumbnail-original-link"></a>
                      <div class="caption">
                        <div class="caption-text">
                          <p>Conheça nossas instalações!</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div data-filter="Category 2" class="col-xs-12 col-sm-6 col-md-4 isotope-item">
                    <div class="thumbnail thumbnail-variant-4">
                      <figure><img src="<?php echo $this->base; ?>/images/conheca/equipe-menor.jpg" alt="" width="370" height="510"/>
                      </figure><a href="<?php echo $this->base; ?>/images/conheca/equipe.jpg" data-photo-swipe-item="" data-size="1200x800" class="thumbnail-original-link"></a>
                      <div class="caption">
                        <div class="caption-text">
                          <h6>Nosso Time</h6>
                          <p>Precisando de ajuda com seu Condomínio? Nossos profissionais estão preparados para oferecer serviços de alta qualidade.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div data-filter="Category 1" class="col-xs-12 col-sm-6 col-md-4 isotope-item">
                    <div class="thumbnail thumbnail-variant-4">
                      <figure><img src="<?php echo $this->base; ?>/images/conheca/recepcao-menor.jpg" alt="" width="370" height="240"/>
                      </figure><a href="<?php echo $this->base; ?>/images/conheca/recepcao.jpg" data-photo-swipe-item="" data-size="1200x801" class="thumbnail-original-link"></a>
                      <div class="caption">
                        <div class="caption-text">
                          <p>Conheça nossas instalações!</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div data-filter="Category 3" class="col-xs-12 col-sm-6 col-md-4 isotope-item">
                    <div class="thumbnail thumbnail-variant-4">
                      <figure><img src="<?php echo $this->base; ?>/images/conheca/comercial2-menor.jpg" alt="" width="370" height="510"/>
                      </figure><a href="<?php echo $this->base; ?>/images/conheca/comercial2.jpg" data-photo-swipe-item="" data-size="1200x801" class="thumbnail-original-link"></a>
                      <div class="caption">
                        <div class="caption-text">
                          <p>Conheça nossas instalações!</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div data-filter="Category 1" class="col-xs-12 col-sm-6 col-md-4 isotope-item">
                    <div class="thumbnail thumbnail-variant-4">
                      <figure><img src="<?php echo $this->base; ?>/images/conheca/instalacoes1-menor.jpg" alt="" width="370" height="510"/>
                      </figure><a href="<?php echo $this->base; ?>/images/conheca/instalacoes1.jpg" data-photo-swipe-item="" data-size="1200x800" class="thumbnail-original-link"></a>
                      <div class="caption">
                        <div class="caption-text">
                          <p>Conheça nossas instalações!</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div data-filter="Category 2" class="col-xs-12 col-sm-6 col-md-4 isotope-item">
                    <div class="thumbnail thumbnail-variant-4">
                      <figure><img src="<?php echo $this->base; ?>/images/conheca/instalacoes2-menor.jpg" alt="" width="370" height="240"/>
                      </figure><a href="<?php echo $this->base; ?>/images/conheca/instalacoes2.jpg" data-photo-swipe-item="" data-size="1200x804" class="thumbnail-original-link"></a>
                      <div class="caption">
                        <div class="caption-text">
                          <p>Conheça nossas instalações!</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                
              <div class="col-xs-12">
                <div class="range inset-lg-left-30">
                  <div class="col-xs-2">
                    <div class="offset-top-30 offset-md-top-55">
                      <h5>Telefone</h5>
                      <hr>
                      <address class="contact-info">
                        <div class="unit unit-horizontal unit-spacing-xs">
                          <div class="unit-left icon-adjust-vertical"><span class="icon icon-xs icon-primary mdi mdi-phone"></span></div>
                          <div class="unit-body">
                            <ul class="list-links">
                              <li><a href="#" class="link-gray">(61) 3344-3010</a></li>
                            </ul>
                          </div>
                        </div>
                      </address>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="offset-top-30 offset-md-top-55">
                      <h5>Endereço</h5>
                      <hr>
                      <address class="contact-info">
                        <div class="unit unit-horizontal unit-spacing-xs">
                          <div class="unit-left icon-adjust-vertical"><span class="icon icon-xs icon-primary mdi mdi-map-marker"></span></div>
                          <div class="unit-body">
                            QRSW 05 Comercio Local 01<br>
                            Lojas 1, 3, 5, 20, 22, 23, 25 e 27<br>
                            Sudoeste, Brasília-DF<br>
                            Cep: 70675-550
                          </div>
                        </div>
                      </address>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="offset-top-30 offset-md-top-55">
                      <h5>Aberto ao Público</h5>
                      <hr>
                      <div class="offset-top-22">
                        <div class="contact-info">
                          <div class="unit unit-horizontal unit-spacing-xs">
                            <div class="unit-left icon-adjust-vertical"><span class="icon icon-xs icon-primary mdi mdi-calendar-clock"></span></div>
                            <div class="unit-body">
                              <span class="text-gray">Segunda à Sexta: <br>09h às 12h - 13h às 16h</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-4">                      
                      <div class="offset-top-30 offset-md-top-55">
                        <h5>Empresa</h5>
                        <hr>
                        <div class="unit unit-horizontal unit-spacing-xs offset-top-22">
                          <div class="unit-left icon-adjust-vertical"><span class="icon icon-xs icon-primary mdi fa-building"></span></div>
                          <div class="unit-body">
                              CNPJ: 37.172.459/0001-82<br>
                              Fundação: 01 de dezembro de 1989<br>
                              Área da Sede: 300 m2 (própria)
                          </div>
                        </div>
                      </div>
                 </div>
                </div>
              </div>
            </div>
          </div>
        </section>