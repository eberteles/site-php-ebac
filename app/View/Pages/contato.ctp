        <section class="section-top-40 section-sm-top-40">
          <div class="shell">
            <div class="range">
              <div class="cell-lg-12">
                <h5>Antes de enviar uma solicitação verifique as maiores dúvidas dos nossos clientes:</h5>
                <hr>
                <br>
                <!-- Responsive-tabs-->
                <div data-type="accordion" class="responsive-tabs responsive-tabs-horizontal">
                  <ul class="resp-tabs-list">
                    <li>Como acessar os dados na Internet?</li>
                    <li>Como emitir um Nada-Consta?</li>
                    <li>Como emitir uma 2ª via do Boleto de Cobrança?</li>
                    <li>Como posso desmembrar as taxas em várias guias?</li>
                    <li>O que fazer quando pago o boleto com valor errado?</li>
                    <li>Como atualizar o cadastro da Unidade?</li>
                    <li>Posso fazer pagamento através de depósito em conta corrente?</li>
                    <li>Existe necessidade em antecipar emissões de boletos?</li>
                    <li>Como verificar os pagamentos da unidade?</li>
                    <li>Por que são cobrados Honorários de Advogado?</li>
                    <li>Como proceder para realizar um Acordo Administrativo?</li>
                    <li>É possível utilizar débito em conta corrente para pagamento do condomínio?</li>
                  </ul>
                  <div class="resp-tabs-container">
                    <div class="animated fadeIn">
                      <p>
                          Para acessar os dados da Unidade e do Condomínio basta estar com o CPF (pessoa física) ou CNPJ (pessoa jurídica) cadastrados na base de dados da Ebac. Se ainda não estiver cadastrado, poderá fazê-lo pelos atalhos da tela de logon. Para acessar a tela de logon clique em um dos menus "Minha Unidade" ou "Meu Condomínio".
                      </p>
                    </div>
                    <div class="animated fadeIn">
                        <p>
                            Acessando a opção "Nada-Consta" no menu "Minha Unidade". No formulário de logon informe o CPF/CNPJ. Caso ainda não seja cadastrado utilize as opções de cadastramento. Nosso nada-consta não necessita de assinatura ou reconhecimento de firma, pois poderá ser validado por qualquer pessoa através do código fornecido.
                        </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          Acessando a opção "Boleto de Cobrança" no menu "Minha Unidade". No formulário de logon informe o CPF/CNPJ. Caso ainda não seja cadastrado utilize as opções de cadastramento. A guia disponível é a última emitida pela Ebac e a próxima emissão será mostrada para um melhor acompanhamento da disponibilização do próximo boleto.
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          As guias da Ebac, há mais de 15 anos, são cumulativas podendo constar diversas taxas na mesma guia. Esta forma de cobrança mantém a inadimplência abaixo de 5%, facilita o pagamento e evita erros com cálculos pelos caixas dos bancos. Assim, se por algum motivo não foi possível pagar o Condomínio basta aguardar o próximo boleto que estará corretamente calculado. Em raríssimas situações e se autorizado pelo Condomínio, as taxas podem ser desmembradas em várias guias para pagamentos até em datas diferentes. Para isso, o interessado deverá retirá-las pessoalmente na nossa sede.
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          Os sistemas da Ebac estão preparados para gerar diferenças de pagamento a menor ou a maior. Por exemplo, quando se paga o valor sem desconto e o valor a pagar era com desconto, o pagamento a maior será creditado automaticamente no próximo boleto. Da mesma forma, quando se paga um valor a menor a diferença a pagar será debitada no próximo boleto.
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          Acessando a opção "Cadastro" no menu "Minha Unidade". No formulário de logon informe o CPF/CNPJ. Caso ainda não seja cadastrado utilize as opções de cadastramento.
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          Não. O depósito em conta corrente não quita débitos pois não identifica o pagador/unidade.
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          Não. Mantendo os dados cadastrais atualizados, a emissão do boleto na página da Ebac é simples e rápida e pode ser feita até mesmo antes de recebê-lo pelo correio.
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          Acessando a opção "Taxas Quitadas" no menu "Minha Unidade". No formulário de logon informe o CPF/CNPJ. Caso ainda não seja cadastrado utilize as opções de cadastramento. No mínimo, os 12 últimos pagamentos estarão disponíveis para consulta e poderão ser impressos para controle ou para comprovações diversas.
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          Por força de contrato, a Ebac está obrigada a cobrar as taxas em aberto através do seu Departamento Jurídico. Visando não onerar o Condomínio e, consequentemente, os Condôminos que se mantém sempre em dia, somente o devedor pagará os honorários advocatícios. Desta forma, a Ebac consegue manter uma inadimplência abaixo de 5% e uma taxa de condomínio mais próxima possível da realidade das despesas sem fundo de caixa para inadimplência.
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          Em situações especiais, o devedor poderá evitar uma demanda judicial procurando, pessoalmente, nosso Departamento Jurídico para compor um acordo administrativo. Com autorização do Condomínio e/ou Assembléia Geral as taxas serão transformadas em parcelas com vencimentos futuros. Tudo isso formalizado em um único documento discriminado, assinado e com firma reconhecida em cartório e terá valor de confissão de dívida.
                      </p>
                    </div>
                    <div class="animated fadeIn">
                      <p>
                          Não. A Ebac está preparada tecnicamente para tal situação. Contudo, o procedimento ainda é inviável para o Condomínio com relação ao alto custo das tarifas bancárias, pois seria obrigado a manter conta corrente em todos os bancos que os moradores tenham conta. Tão logo seja possível a realização dos débitos a Ebac divulgará amplamente .
                      </p>
                    </div>
                      
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </section>

        <section class="section-60 section-sm-90">
          <div class="shell">
            <div class="range">
              <div class="col-sm-12 col-md-9" id="div-form">
                <h5 id="falar-ebac">Quero falar com a Ebac por email</h5>
                <hr>
                <div class="offset-top-50">
                  <!-- RD Mailform  -->
                  <form data-form-output="form-output-global" data-form-type="fale-com-ebac" method="post" action="<?php echo $this->base; ?>/envia/fale_com_ebac" class="rd-mailform rd-mailform-mod-1">
                    <div class="range">
                      <div class="cell-sm-6">
                        <div class="form-group">
                          <label for="contact-assunto" class="form-label-outside">Assunto</label>
                          <select id="contact-assunto" name="cmbMotivo" data-constraints="@Required" class="form-control">
                            <option selected="">Selecione um dos Assuntos</option>
                            <option>01-Dúvida</option><option>02-Elogio</option>
                            <option>03-Reclamação</option>
                            <option>04-Sugestão</option>
                            <option>05-Cadastro</option>
                            <option>06-Guia de Cobrança (Boleto)</option>
                            <option>07-Pagamentos Realizados</option>
                            <option>08-Site da Ebac (Internet)</option>
                          </select>
                        </div>
                      </div>
                      <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                        <div class="form-group">
                          <label for="contact-last-name" class="form-label-outside">Nome Remetente</label>
                          <input id="contact-last-name" type="text" name="txtNome" data-constraints="@Required" class="form-control">
                        </div>
                      </div>
                      <div class="cell-sm-6 offset-top-18">
                        <div class="form-group">
                          <label for="contact-condominio" class="form-label-outside">Condomínio</label>
                          <input id="contact-condominio" type="text" name="txtNomeCon" class="form-control">
                        </div>
                      </div>
                      <div class="cell-sm-6 offset-top-18">
                        <div class="form-group">
                          <label for="contact-unidade" class="form-label-outside">Unidade</label>
                          <input id="contact-unidade" type="text" name="txtUnidade" class="form-control">
                        </div>
                      </div>
                      <div class="cell-sm-6 offset-top-18">
                        <div class="form-group">
                          <label for="contact-telefone" class="form-label-outside">Telefone</label>
                          <input id="contact-telefone" type="text" name="txtFone1" class="form-control telefone">
                        </div>
                      </div>
                      <div class="cell-sm-6 offset-top-18">
                        <div class="form-group">
                          <label for="contact-celular" class="form-label-outside">Celular</label>
                          <input id="contact-celular" type="text" name="txtFone2" class="form-control telefone">
                        </div>
                      </div>
                      <div class="cell-sm-6 offset-top-18">
                        <div class="form-group">
                          <label for="contact-email" class="form-label-outside">E-mail</label>
                          <input id="contact-email" type="email" name="txtEmail" data-constraints="@Email @Required" class="form-control">
                        </div>
                      </div>
                      <div class="cell-sm-6 offset-top-18">
                        <div class="form-group">
                          <label for="contact-email2" class="form-label-outside">Confirmação do E-mail</label>
                          <input id="contact-email2" type="email" name="txtConfirmaEmail" data-constraints="@Email @Required" class="form-control">
                        </div>
                      </div>
                      <div class="cell-xs-12 offset-top-18">
                        <div class="form-group">
                          <label for="contact-message" class="form-label-outside">Mensagem</label>
                          <textarea id="contact-message" name="txtTexto" data-constraints="@Required" class="form-control"></textarea>
                        </div>
                      </div>
                      <div class="cell-xs-12 offset-top-30">
                        <button type="submit" class="btn btn-primary">Enviar Mensagem</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

              <div class="col-sm-12 col-md-9 hidden" id="div-form-sucesso">
                <h5 id="falar-ebac">Quero falar com a Ebac por email</h5>
                <hr>
                <div class="offset-top-50">
                    <div class="grid-element">
                        <h4 class="text-center">Confirmação</h4><br><br>
                        <p class="text-center">Os dados foram enviados para a Ebac.</p><br>
                        <p class="text-center">Aguarde a resposta em até 2 dias úteis</p>
                    </div>
                </div>
              </div>

              <div class="col-sm-12 col-md-9 hidden" id="div-form-error">
                <h5 id="falar-ebac">Quero falar com a Ebac por email</h5>
                <hr>
                <div class="offset-top-50">
                    <div class="grid-element">
                        <h4 class="text-center">Não foi possível enviar sua Mensagem</h4><br><br>
                        <p class="text-center">Publicando Dados ou Instabilidade Temporária no Provedor.</p><br>
                        <p class="text-center">Tente mais tarde!</p>
                    </div>
                </div>
              </div>

              <div class="col-xs-12 col-md-3">
                <div class="range inset-lg-left-30">
                  <div class="col-xs-12 col-sm-6 col-md-12">
                    <div class="offset-top-30 offset-md-top-55">
                      <h5>Telefone</h5>
                      <hr>
                      <address class="contact-info">
                        <div class="unit unit-horizontal unit-spacing-xs">
                          <div class="unit-left icon-adjust-vertical"><span class="icon icon-xs icon-primary mdi mdi-phone"></span></div>
                          <div class="unit-body">
                            <ul class="list-links">
                              <li><a href="#" class="link-gray">(61) 3344-3010</a></li>
                            </ul>
                          </div>
                        </div>
                      </address>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-12">
                    <div class="offset-top-30 offset-md-top-55">
                      <h5>Endereço</h5>
                      <hr>
                      <address class="contact-info">
                        <div class="unit unit-horizontal unit-spacing-xs">
                          <div class="unit-left icon-adjust-vertical"><span class="icon icon-xs icon-primary mdi mdi-map-marker"></span></div>
                          <div class="unit-body">
                            QRSW 05 Comercio Local 01<br>
                            Lojas 1, 3, 5, 20, 22, 23, 25 e 27<br>
                            Sudoeste, Brasília-DF<br>
                            Cep: 70675-550
                          </div>
                        </div>
                      </address>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-12">
                    <div class="offset-top-30 offset-md-top-55">
                      <h5>Aberto ao Público</h5>
                      <hr>
                      <div class="offset-top-22">
                        <div class="contact-info">
                          <div class="unit unit-horizontal unit-spacing-xs">
                            <div class="unit-left icon-adjust-vertical"><span class="icon icon-xs icon-primary mdi mdi-calendar-clock"></span></div>
                            <div class="unit-body">
                              <span class="text-gray">Segunda à Sexta: <br>09h às 12h - 13h às 16h</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>