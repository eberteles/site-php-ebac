        <br>
        
<!--        <img src="<?php echo $this->base; ?>/images/banner-topo.png" class="img-responsive center-block"/>-->

        <section class="section-top-40 section-sm-top-40">
          <div class="shell text-center text-sm-left">
            <div class="range">
              <div class="cell-xs-12">
                <h4><span class="icon icon-sm-variant-2 icon-primary fa-building"></span> Principais Serviços On-Line</h4>
                <hr>
              </div>
            </div>
            <ul class="range list-icons-variant-1 offset-top-40">
              <li class="cell-sm-6 cell-lg-4">
                <div class="unit unit-sm-horizontal unit-spacing-sm">
                  <div class="unit-left"><span class="icon icon-sm icon-circle-md icon-white icon-primary-filled mdi fa-barcode"></span></div>
                  <div class="unit-body">
                    <h6>Boleto de Cobrança</h6>
                    <p><?php echo $this->Html->link('Acesse aqui para emitir a 2ª via do boleto da sua Unidade.', array('controller' => 'unidades', 'action' => 'boleto')); ?></p>
                  </div>
                </div>
              </li>
              <li class="cell-sm-6 cell-lg-4 offset-top-40 offset-sm-top-0">
                <div class="unit unit-sm-horizontal unit-spacing-sm">
                  <div class="unit-left"><span class="icon icon-md icon-circle-md icon-white icon-primary-filled mdi fa-folder-open-o"></span></div>
                  <div class="unit-body">
                    <h6>Taxas em Aberto</h6>
                    <p><?php echo $this->Html->link('Consulta as taxas em aberto da sua Unidade.', array('controller' => 'unidades', 'action' => 'taxas_abertas')); ?></p>
                  </div>
                </div>
              </li>
              <li class="cell-sm-6 cell-lg-4 offset-top-40 offset-lg-top-0">
                <div class="unit unit-sm-horizontal unit-spacing-sm">
                  <div class="unit-left"><span class="icon icon-sm-variant-2 icon-circle-md icon-white icon-primary-filled mdi fa-check-square-o"></span></div>
                  <div class="unit-body">
                    <h6>Emitir Nada-Consta</h6>
                    <p><?php echo $this->Html->link('Acesse aqui e imprima o Nada_Conta da sua Unidade.', array('controller' => 'unidades', 'action' => 'nada_consta')); ?></p>
                  </div>
                </div>
              </li>
            </ul>
            <ul class="range list-icons-variant-1 offset-top-40">
              <li class="cell-sm-6 cell-lg-4">
                <div class="unit unit-sm-horizontal unit-spacing-sm">
                  <div class="unit-left"><span class="icon icon-sm icon-circle-md icon-white icon-primary-filled mdi fa-bullhorn"></span></div>
                  <div class="unit-body">
                    <h6>Comunicados</h6>
                    <p><?php echo $this->Html->link('Aqui você poderá ver os comunicados emitidos para o seu Condomínio.', array('controller' => 'condominios', 'action' => 'comunicados')); ?></p>
                  </div>
                </div>
              </li>
              <li class="cell-sm-6 cell-lg-4 offset-top-40 offset-sm-top-0">
                <div class="unit unit-sm-horizontal unit-spacing-sm">
                  <div class="unit-left"><span class="icon icon-md icon-circle-md icon-white icon-primary-filled mdi fa-archive"></span></div>
                  <div class="unit-body">
                    <h6>Convenção e Regimento</h6>
                    <p><?php echo $this->Html->link('Tenha acesso à Convenção e o Regimento do seu Condomínio.', array('controller' => 'condominios', 'action' => 'convencao_regimento')); ?></p>
                  </div>
                </div>
              </li>
              <li class="cell-sm-6 cell-lg-4 offset-top-40 offset-lg-top-0">
                <div class="unit unit-sm-horizontal unit-spacing-sm">
                  <div class="unit-left"><span class="icon icon-sm-variant-2 icon-circle-md icon-white icon-primary-filled mdi fa-file-text-o"></span></div>
                  <div class="unit-body">
                    <h6>Editais e Atas</h6>
                    <p><?php echo $this->Html->link('Verifique aqui todas os Editais e Atas emitidos pelo seu Condomínio.', array('controller' => 'condominios', 'action' => 'edital_ata')); ?></p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </section>
          
        <section class="section-top-40 section-sm-top-75">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <h4><span class="icon icon-sm-variant-2 icon-primary fa-question-circle"></span> Dúvidas</h4>
                <hr>
              </div>
            </div>
            <div class="row grid-system-row offset-top-32">
              <div class="col-md-4 col-xs-12">
                <div class="grid-element">
                  <h6>Assuntos Condominiais</h6>
                  <br>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As dúvidas que nos são enviadas são pesquisadas, respondidas e transformadas em assuntos, formando um banco de dados para consulta gratuita dos interessados.
                  <br>
                  <?php echo $this->Html->link('continuar lendo', array('controller' => 'duvida'), array('class'=>'btn btn-sm btn-primary')); ?>
                </div>
              </div>
              <div class="col-md-4 col-xs-12">
                <div class="grid-element">
                  <h6>Legislações Condonominiais</h6>
                  <br>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Entenda as Leis que regulam a relação entre Condomínio e Condônimo. <br>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dentro desta sessão separamos as principais Legislações Condonominiais.
                  <br>
                  <?php echo $this->Html->link('continuar lendo', array('controller' => 'legislacao'), array('class'=>'btn btn-sm btn-primary')); ?>
                </div>
              </div>
              <div class="col-md-4 col-xs-12">
                <div class="grid-element">
                  <h6>Maiores dúvidas dos nossos clientes</h6>
                  <br>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deseja acessar dados da sua Unidade? Emitir Nada-Conta, ou uma 2ª via do Boleto de Cobrança?<br>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Confira essas e outras dúvidas nesta sessão.
                  <br>
                  <?php echo $this->Html->link('continuar lendo', array('controller' => 'contato'), array('class'=>'btn btn-sm btn-primary')); ?>
                </div>
              </div>
            </div>
          </div>
        </section>