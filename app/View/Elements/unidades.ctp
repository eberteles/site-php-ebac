
    <div class="row grid-system-row">
      <div class="col-xs-12">
        <?php if(isset($unidades)) { ?>
        <table class="table table-venice-blue table-hover">
          <thead>
            <tr>
                <th>Unidade</th>
                <th>Ocupante</th>
                <th>Proprietário</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($unidades as $unidade) { ?>
            <tr>
                <td><?php echo $this->Html->link($unidade['Unidade']['unidade'], array('controller' => 'usuarios', 'action' => 'trocar', $unidade['Unidade']['id'])); ?></td>
                <td><?php echo $this->Html->link($unidade['Ocupante']['nome'], array('controller' => 'usuarios', 'action' => 'trocar', $unidade['Unidade']['id'])); ?></td>
                <td><?php echo $this->Html->link($unidade['Proprietario']['nome'], array('controller' => 'usuarios', 'action' => 'trocar', $unidade['Unidade']['id'])); ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php } ?>
      </div>
    </div>