
<!DOCTYPE html>
<html lang="pt-br" class="wide wow-animation">
  <head>
        <?php echo $this->Html->charset(); ?>
        <title>EBAC - Administração de Condomínios</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="EBAC" />
    <meta name="description" content="EBAC - Administração de Condomínios. Transparência para o Condômino, Eficiência para o Síndico" />
    <meta name="keywords" content="administração de condomínios, ebac, síndico, contabilidade, gestão de condomínio, empresa que administra condominios, condomínio online, imobiliarias, Guia de cobrança detalhada, Dados do condomínio na Internet, Demonstrativo condominial, Conselho Regional de Administração" />
    <meta name="application-name" content="EBAC" />
    <link rel="icon" href="<?php echo $this->base; ?>/images/favicon.ico" type="image/x-icon">
    
    <style type="text/css">
        .blue{color:#0000FF;}
        .red{color:#FF0000;}
    </style>
    
  </head>
  <body>
    <?php echo $this->fetch('content'); ?>
  </body>
</html>