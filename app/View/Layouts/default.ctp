
<!DOCTYPE html>
<html lang="pt-br" class="wide wow-animation">
  <head>
        <?php echo $this->Html->charset(); ?>
        <title>EBAC - Administração de Condomínios</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="EBAC" />
    <meta name="description" content="EBAC - Administração de Condomínios. Transparência para o Condômino, Eficiência para o Síndico" />
    <meta name="keywords" content="administração de condomínios, ebac, síndico, contabilidade, gestão de condomínio, empresa que administra condominios, condomínio online, imobiliarias, Guia de cobrança detalhada, Dados do condomínio na Internet, Demonstrativo condominial, Conselho Regional de Administração" />
    <meta name="application-name" content="EBAC" />
    <link rel="icon" href="<?php echo $this->base; ?>/images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,600italic,900">
    <?php
        //echo $this->Html->css('cake.generic');
        echo $this->Html->css('style');
    ?>
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo $this->base; ?>/images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>
  <body>
    <?php
        echo $this->Html->script( 'core.min' );
        echo $this->Html->script( 'bootbox' );
    ?>
      
    <!-- Page-->
    <div class="page">
      <!-- Page Header-->
      <header class="page-head">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-md-layout="rd-navbar-static" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-clone="false" data-md-stick-up-offset="226px" data-lg-stick-up-offset="196px" class="rd-navbar rd-navbar-center rd-navbar-dark">
            <div class="rd-navbar-inner">
              <div class="rd-navbar-aside">
                <div class="rd-navbar-aside-content">
                    <p class="rd-navbar-fixed--hidden" style="text-align: center">Trasparência para o Condômino, Eficiência para o Síndico</p>
                </div>
              </div>
              <div class="rd-navbar-middle-panel">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button data-rd-navbar-toggle=".rd-navbar-outer-panel" class="rd-navbar-toggle"><span></span></button>
                  <!-- RD Navbar Brand--><a href="<?php echo $this->base; ?>/" class="rd-navbar-brand">
                    <div class="rd-navbar-fixed--hidden"><img src="<?php echo $this->base; ?>/images/logo-ebac.png" alt="" />
                    </div>
                    <div class="rd-navbar-fixed--visible"><img src="<?php echo $this->base; ?>/images/logo-ebac-176x43.png" alt="" width="176" height="43"/>
                    </div></a>
                </div>
              </div>
              <div class="rd-navbar-outer-panel">
                <div class="rd-navbar-nav-wrap">
                  <!-- RD Navbar Nav-->
                  <ul class="rd-navbar-nav">
                    <li><a href="<?php echo $this->base; ?>/">Início</a></li>
                    <li><a href="<?php echo $this->base; ?>/conheca/">Conheça a Empresa</a></li>
                    <li><a href="#">Minha Unidade</a>
                      <!-- RD Navbar Dropdown-->
                      <ul class="rd-navbar-dropdown">
                        <li><a href="<?php echo $this->base; ?>/unidades/cadastro">Cadastro</a></li>
                        <li><a href="<?php echo $this->base; ?>/unidades/taxas_quitadas">Taxas Quitadas</a></li>
                        <li><a href="<?php echo $this->base; ?>/unidades/taxas_abertas">Taxas em Aberto</a></li>
                        <li><a href="<?php echo $this->base; ?>/unidades/boleto">Boleto de Cobrança</a></li>
                        <li><a href="<?php echo $this->base; ?>/unidades/nada_consta">Emitir Nada-Consta</a></li>
                        <li><a href="<?php echo $this->base; ?>/unidades/validar">Validar Nada-Consta</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Meu Condomínio</a>
                      <ul class="rd-navbar-dropdown">
                        <li><a href="<?php echo $this->base; ?>/condominios/cadastro">Cadastro Geral</a></li>
                        <li><a href="<?php echo $this->base; ?>/condominios/comunicados">Comunicados</a></li>
                        <li><a href="<?php echo $this->base; ?>/condominios/convencao_regimento">Convenção e Regimento</a></li>
                        <li><a href="<?php echo $this->base; ?>/condominios/edital_ata">Editais e Atas</a></li>
                        <li><a href="<?php echo $this->base; ?>/condominios/fluxo">Receitas e Despesas</a></li>
                        <li><a href="<?php echo $this->base; ?>/condominios/taxas_quitadas">Taxas Quitadas</a></li>
                        <li><a href="<?php echo $this->base; ?>/condominios/taxas_abertas">Taxas em Aberto</a></li>
                        <li><a href="<?php echo $this->base; ?>/condominios/rateio">Rateio de Despesas</a></li>
                        <?php
                            if(AuthComponent::user('role') != '' || AuthComponent::user('username') == $this->Session->read('UnidadeAtual.Condominio.Sindico.documento')) {
                        ?>
                        <li><a href="<?php echo $this->base; ?>/votacoes/">Votação em Assembléias</a></li>
                        <?php
                            }
                        ?>
                        <li><a href="<?php echo $this->base; ?>/condominios/galeria">Fotos e Imagens</a></li>
                        <li><a href="<?php echo $this->base; ?>/condominios/diversos">Diversos</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Dúvidas</a>
                      <!-- RD Navbar Dropdown-->
                      <ul class="rd-navbar-dropdown">
                        <li><a href="<?php echo $this->base; ?>/duvida/">Assuntos Condominiais</a></li>
                        <li><a href="<?php echo $this->base; ?>/legislacao/">Legislações</a></li>
                        <li><a href="<?php echo $this->base; ?>/contato/">Contato</a></li>
                      </ul>
                    </li>
                    <?php
                        if( AuthComponent::user('id') != null && ( AuthComponent::user('role') == 'ADMINISTRADORA' || AuthComponent::user('role') == 'SINDICO' || AuthComponent::user('username') == $this->Session->read('UnidadeAtual.Condominio.Sindico.documento') ) ) {
                    ?>
                    <li><a href="<?php echo $this->base; ?>/condominios/acesso">Auditoria de Acessos</a></li>
                    <?php
                        }
                    ?>
                  </ul>
                </div>
                <div class="rd-navbar-top-panel">
                  <div class="rd-navbar-top-panel-inner">
                      <img class="hidden-sm hidden-xs" src="<?php echo $this->base; ?>/images/tempo-ebac.png" alt="" />
                      <?php if(AuthComponent::user('id') > 0) { ?>
                        <div>
                            <p><?php echo AuthComponent::user('nome'); ?><br>
                                <?php echo $this->Session->read('UnidadeAtual.Condominio.nome'); ?><br>
                                Unidade: <?php echo $this->Session->read('UnidadeAtual.Unidade.unidade'); ?><br></p>
                                <?php if(count($this->Session->read('unidades'))>1 || count($this->Session->read('condominios'))>1) {?>
                                <a href="<?php echo $this->base; ?>/unidades/selecionar" title="Mudar de Unidade"><span class="icon icon-xs icon-gray fa-exchange"></span></a>&nbsp;&nbsp;
                                <?php } ?>
                                <a href="<?php echo $this->base; ?>/usuarios/sair" title="Sair"><span class="icon icon-xs fa-sign-out"></span></a>
                        </div>
                      <?php } else {
                          ?>
                        <a href="<?php echo $this->base; ?>/usuarios/login" class="btn btn-xs btn-primary-variant-1">área restrita</a>
                      <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">
          
		<div id="content">
			
			<?php echo $this->Breadcrumb->render(); ?>

			<?php echo $this->Flash->render(); ?>

                        <?php 
                        echo $this->fetch('content');
                        echo $this->element('sql_dump');
                        ?>
		</div>
        <br>
          
      </main>
      <!-- Page Footer-->
      <footer class="page-foot bg-gray-dark">
        <div class="shell text-center text-sm-left">
          <div class="range range-sm-center">
            <div class="cell-sm-10 cell-md-12">
              <div class="range range-md-justify">
                <div class="cell-sm-6 cell-md-2 wrap-lg-justify-vertical">
                  <!-- div class="brand-wrap"><a href="<?php echo $this->base; ?>/" class="brand"><img src="<?php echo $this->base; ?>/images/logo-ebac-b2.png" /></a></!-->
                  <div>
                      <address class="contact-info contact-info-contrast">
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-left icon-adjust-vertical"><span class="icon icon-xs icon-white mdi mdi-map-marker"></span></div>
                          <div class="unit-body"><a href="#" class="nowrap">QRSW 05 Comercio Local 01<br>Lojas 1, 3, 5, 20, 22, 23, 25 e 27<br>Sudoeste, Brasília-DF<br>Cep: 70675-550</a></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs offset-top-22">
                          <div class="unit-left icon-adjust-vertical"><span class="icon icon-xs icon-white mdi fa-clock-o"></span></div>
                          <div class="unit-body"><span>Aberto ao Público:</span><span>Seg-Sex: 09h às 12h - 13h às 16h</span></div>
                        </div>
                      </address>
                  </div>
                </div>
                <div class="cell-sm-4 cell-md-2 offset-top-55 offset-sm-top-0">
                  <div class="max-width-300">
                    <h5 class="h5-variant-1">Minha Unidade</h5>
                    <hr>
                  </div>
                  <div class="offset-top-18">
                    <address class="contact-info contact-info-contrast">
                    <!-- RD Mailform-->
                      <div class="unit-body"><span><a href="<?php echo $this->base; ?>/unidades/cadastro">Cadastro</a></span></div>
                      <div class="unit-body"><span><a href="<?php echo $this->base; ?>/unidades/taxas_quitadas">Taxas Quitadas</a></span></div>
                      <div class="unit-body"><span><a href="<?php echo $this->base; ?>/unidades/taxas_abertas">Taxas em Aberto</a></span></div>
                        <div class="unit-body"><span><a href="<?php echo $this->base; ?>/unidades/boleto">Boleto de Cobrança</a></span></div>
                      <div class="unit-body"><span><a href="<?php echo $this->base; ?>/unidades/nada_consta">Emitir Nada-Consta</a></span></div>
                      <div class="unit-body"><span><a href="<?php echo $this->base; ?>/unidades/validar">Validar Nada-Consta</a></span></div>
                    </address>
                  </div>
                </div>
                <div class="cell-md-2 cell-lg-4 offset-top-55 offset-md-top-0">
                  <div class="range">
                    <div class="cell-xs-12">
                      <h5 class="h5-variant-1">Meu Condomínio</h5>
                      <hr>
                    </div>
                    <div class="cell-xs-5 cell-sm-6 cell-md-12 cell-lg-6 offset-top-18 text-xs-left">
                      <address class="contact-info contact-info-contrast">
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><span><a href="<?php echo $this->base; ?>/condominios/cadastro">Cadastro Geral</a></span></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><span><a href="<?php echo $this->base; ?>/condominios/comunicados">Comunicados</a></span></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><span><a href="<?php echo $this->base; ?>/condominios/convencao_regimento">Convenção e Regimento</a></span></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><span><a href="<?php echo $this->base; ?>/condominios/edital_ata">Editais e Atas</a></span></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><span><a href="<?php echo $this->base; ?>/condominios/fluxo">Receitas e Despesas</a></span></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><span><a href="<?php echo $this->base; ?>/condominios/taxas_quitadas">Taxas Quitadas</a></span></div>
                        </div>                         
                      </address>
                    </div>
                    <div class="cell-xs-7 cell-sm-6 cell-md-12 cell-lg-6 offset-top-22 offset-xs-top-18 inset-lg-left-9 text-xs-left">
                      <address class="contact-info contact-info-contrast">
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><a href="<?php echo $this->base; ?>/condominios/taxas_abertas" class="nowrap">Taxas em Aberto</a></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><a href="<?php echo $this->base; ?>/condominios/rateio" class="nowrap">Rateio de Despesas</a></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><a href="<?php echo $this->base; ?>/votacoes/" class="nowrap">Votação em Assembléias</a></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><a href="<?php echo $this->base; ?>/condominios/galeria" class="nowrap">Fotos e Imagens</a></div>
                        </div>
                        <div class="unit unit-xs-horizontal unit-spacing-xs">
                          <div class="unit-body"><a href="<?php echo $this->base; ?>/condominios/diversos" class="nowrap">Diversos</a></div>
                        </div>
                      </address>
                    </div>
                  </div>
                </div>
                <div class="cell-sm-4 cell-md-2 offset-top-55 offset-sm-top-0">
                  <div class="max-width-300">
                    <h5 class="h5-variant-1">Dúvidas</h5>
                    <hr>
                  </div>
                  <div class="offset-top-18">
                    <address class="contact-info contact-info-contrast">
                    <!-- RD Mailform-->
                      <div class="unit-body"><span><a href="<?php echo $this->base; ?>/duvida/">Assuntos Condominiais</a></span></div>
                      <div class="unit-body"><span><a href="<?php echo $this->base; ?>/legislacao/">Legislações</a></span></div>
                      <div class="unit-body"><span><a href="<?php echo $this->base; ?>/contato/">Contato</a></span></div>
                    </address>
                  </div>
                </div>
              </div>
              <div class="range offset-top-55 offset-sm-top-60 offset-lg-top-88">
                <div class="cell-xs-12">
                  <p class="rights">&#169;&nbsp;<span id="copyright-year"></span>&nbsp;Todos os direitos reservados&nbsp;</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>

    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Fechar (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Tela Cheia" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Anterior" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Próxima" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    
    <?php
        echo $this->Html->script( 'jquery.maskMoney' );
        echo $this->Html->script( 'jquery.maskedinput' );
        echo $this->Html->script( 'script' );
    ?>
</html>