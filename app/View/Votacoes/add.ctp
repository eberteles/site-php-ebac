<?php echo $this->Form->create('Votacao', array('url'=>array('action'=>'add'))); ?>

    <section>
      <div class="shell">
        <div class="range">
            
          <div class="col-sm-12 col-md-12" id="div-form">
            <div class="offset-top-30">
                
                <div class="range">
                  <div class="cell-sm-12">
                    <table class="table table-venice-blue table-hover">
                      <thead>
                        <tr>
                            <th>Nova Votação</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                  <div class="cell-sm-3">
                    <div class="form-group">
                        <?php echo $this->Form->input('assembleia', array('class'=>'form-control', 'label'=>' ', 'options' => $assembleias)); ?>
                    </div>
                  </div>
                  <div class="cell-sm-2 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                        <?php echo $this->Form->input('data', array('type'=>'text','data-constraints'=>'@Required', 'class'=>'form-control data-pt', 'label'=>array('class'=>'form-label-outside', 'text'=>'Data') )); ?>
                    </div>
                  </div>
                  <div class="cell-sm-7 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                        <?php echo $this->Form->input('assunto', array('data-constraints'=>'@Required', 'class'=>'form-control', 'label'=>array('class'=>'form-label-outside', 'text'=>'Pauta da Votação'))); ?>
                    </div>
                  </div>
                    
                <?php
                    $opcoes = array();
                    for($i=1; $i <= $this->data['Votacao']['quantidade']; $i++) {
                        $opcoes[$i] = $i;
                ?>                    
                  <div class="cell-sm-12 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                        <?php echo $this->Form->input('Proposta.' . $i, array('data-constraints'=>'@Required', 'class'=>'form-control', 'required'=>'required', 'label'=>array('class'=>'form-label-outside', 'text'=>'Proposta - ' . $i))); ?>
                    </div>
                  </div>
                <?php
                    }
                ?>
                    
                  <div class="cell-sm-12 offset-top-18 offset-sm-top-0">
                    <div class="group group-middle offset-top-30 text-center text-xs-left">
                      <button type="submit" name="btnNovo" value="Finalizar" class="btn btn-sm btn-primary">finalizar votação</button>
                    </div>
                  </div>
                    
                </div>
            </div>
          </div>

        </div>
      </div>
    </section><br><br>

<table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th>Unidade</th>
            <th>Fração Ideal</th>
            <th>Votação</th>
            <th>Propostas</th>
        </tr>
      </thead>

      <tbody>
        <?php
        foreach ($unidades as $unidade) {
            echo '<tr>';
            
            echo '<td>' . $unidade['Unidade']['unidade'] . '</td>';
            echo '<td>' . $unidade['Unidade']['fracao'] . $this->Form->hidden('Fracao.' . $unidade['Unidade']['unidade'], array('value'=>$unidade['Unidade']['fracao'])) . '</td>';
            echo '<td>' . $this->Form->checkbox('UnidadeVotacao.' . $unidade['Unidade']['unidade']) . '</td>';
            echo '<td>' . $this->Form->radio('UnidadeProposta.' . $unidade['Unidade']['unidade'], $opcoes, array('unidade'=>$unidade['Unidade']['unidade'],'class' => 'marcar','legend' => false, 'separator'=> ' &nbsp; &nbsp; ')) . '</td>';

            echo '</tr>';
        }
        ?>
      </tbody>

    </table>
    
                  <div class="cell-sm-12 offset-top-18 offset-sm-top-0">
                    <div class="group group-middle offset-top-30 text-center text-xs-left">
                      <button type="submit" name="btnNovo" value="Finalizar" class="btn btn-sm btn-primary">finalizar votação</button>
                    </div>
                  </div>
    
    <script>
        
        $(".marcar").click(function () {
            $("#UnidadeVotacao"+$(this).attr('unidade')).prop("checked", true);
        });
        
    </script>