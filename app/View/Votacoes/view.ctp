<table border="0" cellpadding="4" cellspacing="1" width="100%" bordercolor="#C0C0C0">
  <tr>
    <td height="121" align="center">
        <img border="0" src="<?php echo $this->base; ?>/images/ebaclogo.jpg" width="131" height="111"></td>
    <td height="121" align="center">
        <font face="Verdana" size="5"><b>Administração de Condomínios</b></font><br><br>
        <b><font face="Verdana" size="2">www.ebac.com.br</font></b>
    </td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10">
  <tr>
     <td align="center" height="1"></td>
  </tr>
</table>

<table border="0" cellpadding="8" cellspacing="1" width="100%" height="50" bordercolor="#C0C0C0">
  <tr>
    <td align="center" bgcolor="#FFFF99"><b><font face="Verdana" size="3" color="#000000"><?php echo sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id') ) . ' - ' . $this->Session->read('UnidadeAtual.Condominio.nome'); ?></font></b>
    </td>
  </tr>
  <tr>
    <td align="center"><b><font face="Verdana" size="4" color="#000000">Resultado da Votação: <?php echo $votacao['Votacao']['assunto']; ?></font></b></td>
  </tr>
  <tr>
    <td align="center"><b><font face="Verdana" size="4" color="#000000"><?php echo $assembleias[$votacao['Votacao']['assembleia']] . ' - ' . $this->Formatacao->data($votacao['Votacao']['data']); ?></font></b></td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10">
  <tr>
     <td align="center" height="1"></td>
  </tr>
</table>

<?php
foreach ($votacao['Proposta'] as $proposta) {
?>
<hr width="700" size="2" color="#FFA54A">
<table width="100%" bordercolor="#C0C0C0" border="0" cellspacing="1" cellpadding="8">
  <tr>
    <td height="21" width="250">
      <p align="left"><font face="Verdana" size="2"><b>Proposta:</b></font></td>
    <td height="21"><font face="Verdana" size="2"><?php echo $proposta['descricao']; ?></font></td>
  </tr>
  <tr>
    <td height="21" width="250">
      <p align="left"><font face="Verdana" size="2"><b>Unidades:</b></font></td>
    <td height="21"><font face="Verdana" size="2"><?php echo $proposta['unidades']; ?></font></td>
  </tr>
  <tr>
    <td height="21" width="250">
      <p align="left"><font face="Verdana" size="2"><b>Soma Frações:</b></font></td>
    <td height="21"><font face="Verdana" size="2"><?php echo $proposta['soma_fracoes']; ?> <b>(<?php echo $this->Formatacao->porcentagem($proposta['soma_fracoes'] * 100); ?>)</b></font></td>
  </tr>
</table>

<table width="100%" cellspacing="1" height="10">
  <tr>
     <td align="center" height="1"></td>
  </tr>
</table>

<?php
}
?>