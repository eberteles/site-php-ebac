
    <section>
      <div class="shell">
        <div class="range">
            
          <div class="col-sm-12 col-md-12" id="div-form">
            <div class="offset-top-30">
                
                <?php echo $this->Form->create('Votacao', array('url'=>array('action'=>'add'))); ?>
                <div class="range">
                  <div class="cell-sm-12">
                    <table class="table table-venice-blue table-hover">
                      <thead>
                        <tr>
                            <th>Nova Votação</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                  <div class="cell-sm-4">
                    <div class="form-group">
                        <?php echo $this->Form->input('assembleia', array('class'=>'form-control', 'label'=>' ', 'options' => $assembleias)); ?>
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                        <?php echo $this->Form->input('data', array('type'=>'text','data-constraints'=>'@Required', 'class'=>'form-control data-pt', 'label'=>array('class'=>'form-label-outside', 'text'=>'Data') )); ?>
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                        <?php echo $this->Form->input('quantidade', array('maxlength'=>1,'data-constraints'=>'@Required @Numeric', 'value'=>'5', 'class'=>'form-control', 'label'=>array('class'=>'form-label-outside', 'text'=>'Quantidade de Propostas'))); ?>
                    </div>
                  </div>
                  <div class="cell-sm-10 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                        <?php echo $this->Form->input('assunto', array('data-constraints'=>'@Required', 'class'=>'form-control', 'label'=>array('class'=>'form-label-outside', 'text'=>'Pauta da Votação'))); ?>
                    </div>
                  </div>
                  <div class="cell-sm-12 offset-top-18 offset-sm-top-0">
                    <div class="group group-middle offset-top-30 text-center text-xs-left">
                      <button type="submit" name="btnNovo" value="novaVotacao" class="btn btn-sm btn-primary">nova votação</button>
                    </div>
                  </div>

                </div>
            </div>
          </div>

        </div>
      </div>
    </section><br><br>

    <table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th>Data</th>
            <th>Assembléia</th>
            <th>Assunto</th>
        </tr>
      </thead>
      <?php if( count($votacoes) > 0 ) { ?>
      <tbody>
        <?php
        foreach ($votacoes as $votacao) {
            echo '<tr>';
            
            echo '<td><a target="_blank" href="' . $this->base . '/votacoes/view/' . $votacao['Votacao']['id'] . '">' . $this->Formatacao->data($votacao['Votacao']['data']) . '</a></td>';
            echo '<td>Geral ' . (($votacao['Votacao']['assembleia'] == 'O') ? 'Ordinária' : 'Extraordinária') . '</td>';
            echo '<td>' . $votacao['Votacao']['assunto'] . '</td>';

            echo '</tr>';
        }
        ?>
      </tbody>
      <?php } else { ?>
      <tbody>
        <tr>
            <td colspan="3">Não Existem Votações Salvas</td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
    
    <script>
        function abrirVotacao(votacao) {
            window.open('<?php echo $this->base; ?>/votacoes/view/'+votacao, '_blank');
        }
        
        <?php
            if( isset($id_votacao) && $id_votacao > 0 ){
                echo 'abrirVotacao(' . $id_votacao . ');';
            }
        ?>
    </script>
