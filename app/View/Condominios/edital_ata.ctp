
<section class="section-top-40 section-sm-top-40">
    <div class="row grid-system-row">
      <div class="col-xs-12">
        <table class="table table-venice-blue table-hover">
          <thead>
            <tr>
                <th>Data</th>
                <th>Assembléia</th>
                <th>Edital</th>
                <th>Ata</th>
                <th>Principais Assuntos</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($arquivos as $arquivo) { ?>
            <tr>
                <td><?php echo $arquivo['data']; ?></td>
                <td><?php echo $arquivo['tipo']; ?></td>
                <td><?php if(isset($arquivo['edital'])) { ?><a href="<?php echo $arquivo['edital']; ?>" target="_blank">Edital<?php } else { echo '--'; } ?></a></td>
                <td><?php if(isset($arquivo['ata'])) { ?><a href="<?php echo $arquivo['ata']; ?>" target="_blank">Ata<?php } else { echo '--'; } ?></a></td>
                <td><?php echo $arquivo['resumo']; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
</section>