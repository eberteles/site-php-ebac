
<section class="section-top-40 section-sm-top-40">
    <div class="row grid-system-row">
      <div class="col-xs-12">
        <table class="table table-venice-blue table-hover">
          <tbody>
            <?php foreach ($arquivos as $arquivo) { ?>
            <tr>
                <td><a href="<?php echo $arquivo['link']; ?>" target="_blank"><span class="icon icon-sm-variant-1 icon-primary fa-file-pdf-o"></span> <?php echo $arquivo['nome']; ?></a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
</section>