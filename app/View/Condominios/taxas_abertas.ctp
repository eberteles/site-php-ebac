<?php
$vl_total   = 0;
?>
<br><br>

<table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th>Unidade</th>
            <th>Taxas</th>
            <th>Situação</th>
            <th>Valor em Aberto</th>
        </tr>
      </thead>
      <?php if( count($taxas) > 0 ) { ?>
      <tbody>
        <?php
        foreach ($taxas as $taxa) {
            echo '<tr>';
            echo '<td>' . $taxa['Unidade']['unidade'] . '</td>';
            echo '<td>' . $taxa['Unidade']['taxas_abertas'] . '</td>';
            echo '<td>' . $this->Formatacao->situacaoUnidade($taxa['Unidade']['acordo_administrativo'], $taxa['Unidade']['acordor_judicial'], $taxa['Unidade']['ajuizada'] ) . '</td>';
            echo '<td>' . $this->Formatacao->moeda($taxa['Unidade']['divida']) . '</td>';
            echo '</tr>';
            $vl_total += $taxa['Unidade']['divida'];
        }
        ?>
      </tbody>
      <?php } else { ?>
      <tbody>
        <tr>
            <td colspan="4">Nenhuma Taxa em Aberto</td>
        </tr>
      </tbody>
      <?php } ?>
      <thead>
        <tr>
            <th colspan="3">Total:</th>
            <th><?php echo $this->Formatacao->moeda($vl_total); ?></th>
        </tr>
      </thead>
    </table>
<p style="color: #FF0000; text-align: center">O valor já está acrescido de multas, juros e correção monetária (sem honorários de advogado)</p>
