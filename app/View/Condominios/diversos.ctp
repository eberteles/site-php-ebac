
<section class="section-top-40 section-sm-top-40">
    <div class="row grid-system-row">
      <div class="col-xs-12">
        <table class="table table-venice-blue table-hover">
          <thead>
            <tr>
                <th>Data do Arquivo</th>
                <th>Resumo</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($arquivos as $arquivo) { ?>
            <tr>
                <td><a href="<?php echo $arquivo['link']; ?>" target="_blank"><?php echo $arquivo['data']; ?></a></td>
                <td><?php echo $arquivo['resumo']; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
</section>