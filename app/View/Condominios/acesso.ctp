    <section>
      <div class="shell">
        <div class="range">
          <div class="col-sm-12 col-md-12" id="div-form">
            <div class="offset-top-30">
                
                <div class="range">
                  <input type="hidden" id="url" value="<?php echo $this->base; ?>/condominios/acesso/">
                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="cmbReferencia" class="form-label-outside">Mês/Ano</label>
                        <select id="cmbReferencia" >
                            <option value="0">Selecione</option>
                        <?php
                            foreach ($referencias as $anoCombo) {
                                echo '<option ' . (($referencia == $anoCombo[0]['referencia']) ? 'selected' : '' ) . '>' . $anoCombo[0]['referencia'] . '</option>';
                            }
                        ?>
                        </select>
                    </div>
                  </div>

                </div>
            </div>
          </div>

        </div>
      </div>
    </section><br><br>

<table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th>Data e Hora</th>
            <th>Tipo de Usuário</th>
            <th>Nome do Usuário</th>
            <th>Unidade</th>
            <th>Rotina Acessada</th>
        </tr>
      </thead>

      <tbody>
        <?php
        foreach ($acessos as $acesso) {
            echo '<tr>';
            
            echo '<td>' . $this->Formatacao->dataHora($acesso['Acesso']['created']) . '</td>';
            echo '<td>' . $acesso['Acesso']['usuario'] . '</td>';
            echo '<td>' . $acesso['Acesso']['responsavel'] . '</td>';
            echo '<td>' . $acesso['Unidade']['unidade'] . '</td>';
            echo '<td>' . $acesso['Rotina']['descricao'] . '</td>';
            
            echo '</tr>';
        }
        ?>
      </tbody>

    </table>