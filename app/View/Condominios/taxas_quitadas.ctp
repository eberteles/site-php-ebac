<?php
$vl_total   = 0;
?>

    <section>
      <div class="shell">
        <div class="range">
          <div class="col-sm-12 col-md-12" id="div-form">
            <div class="offset-top-30">
                
                <div class="range">
                  <input type="hidden" id="url" value="<?php echo $this->base; ?>/condominios/taxas_quitadas/">
                  <input type="hidden" id="ano_select" value="<?php echo $referencia; ?>">
                  <input type="hidden" id="lancamento_select" value="<?php echo $lancamento; ?>">
                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="cmbAnoReferencia" class="form-label-outside">Referência</label>
                        <select id="cmbAnoReferencia" >
                            <option value="0">Selecione</option>
                        <?php
                            foreach ($referencias as $anoCombo) {
                                echo '<option ' . (($referencia == $anoCombo[0]['referencia']) ? 'selected' : '' ) . '>' . $anoCombo[0]['referencia'] . '</option>';
                            }
                        ?>
                        </select>
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                      <label for="contact-email-ocupante" class="form-label-outside">Tipo de Lançamento</label>
                        <select id="cmbLancamento" >
                            <option value="0">Todos</option>
                        <?php
                            foreach ($lancamentos as $chave => $nomeLancamento ) {
                                echo '<option value="' . $chave . '" ' . (($lancamento == $chave) ? 'selected' : '' ) . '>' . $nomeLancamento . '</option>';
                            }
                        ?>
                        </select>
                    </div>
                  </div>

                </div>
            </div>
          </div>

        </div>
      </div>
    </section><br><br>

<table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th>Unidade</th>
            <th>Tipo Lançamento</th>
            <th>Referência</th>
            <th>Vencimento</th>
            <th>Valor Principal</th>
            <th>Pagamento</th>
            <th>Valor Pago</th>
        </tr>
      </thead>
      <?php if( count($taxas) > 0 ) { ?>
      <tbody>
        <?php
        foreach ($taxas as $taxa) {
            echo '<tr>';
            echo '<td>' . $taxa['Unidade']['unidade'] . '</td>';
            echo '<td>' . $taxa['Taxa']['nome'] . '</td>';
            echo '<td>' . $taxa['UnidadeTaxa']['mes'] . '/' . $taxa['UnidadeTaxa']['ano'] . '</td>';
            echo '<td>' . $this->Formatacao->data($taxa['UnidadeTaxa']['vencimento']) . '</td>';
            echo '<td>' . $this->Formatacao->moeda($taxa['UnidadeTaxa']['valor']) . '</td>';
            echo '<td>' . $this->Formatacao->data($taxa['UnidadeTaxa']['pagamento']) . '</td>';
            echo '<td>' . $this->Formatacao->moeda($taxa['UnidadeTaxa']['pago']) . '</td>';
            echo '</tr>';
            $vl_total += $taxa['UnidadeTaxa']['pago'];
        }
        ?>
      </tbody>
      <?php } else { ?>
      <tbody>
        <tr>
            <td colspan="7">Não Existem Taxas Quitadas</td>
        </tr>
      </tbody>
      <?php } ?>
      <thead>
        <tr>
            <th colspan="6">Total:</th>
            <th><?php echo $this->Formatacao->moeda($vl_total); ?></th>
        </tr>
      </thead>
    </table>

