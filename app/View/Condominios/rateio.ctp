<?php
$totalTaxa          = 0;
$totalComDesconto   = 0;
$totalFracao        = 0;
?>

    <section>
      <div class="shell">
        <div class="range">
          <div class="col-sm-12 col-md-12" id="div-form">
            <div class="offset-top-30">
                <?php echo $this->Form->create('Condominio', array('class'=>'rd-mailform')); ?>
                <div class="range">
                  <div class="cell-sm-4">
                    <div class="form-group">
                        <?php echo $this->Form->input('taxa', array('class'=>'form-control', 'label'=>' ', 'options' => $taxas)); ?>
                    </div>
                  </div>
                  <div class="cell-sm-3 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                        <?php echo $this->Form->input('valor', array('data-constraints'=>'@Required', 'class'=>'form-control money', 'label'=>array('class'=>'form-label-outside', 'text'=>'Valor') )); ?>
                    </div>
                  </div>
                  <div class="cell-sm-2 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                        <?php echo $this->Form->input('desconto', array('maxlength'=>6,'class'=>'form-control percent', 'label'=>array('class'=>'form-label-outside', 'text'=>'Desconto'))); ?>
                    </div>
                  </div>
                  <div class="cell-sm-1 offset-top-18 offset-sm-top-0">
                    <div class="form-group">
                        <label for="cmbAnoReferencia" class="form-label-outside">Considera Isenções</label><br>
                        <?php echo $this->Form->checkbox('isencao'); ?>
                    </div>
                  </div>
                  <div class="cell-sm-2 offset-top-18 offset-sm-top-0">
                    <div class="group group-middle offset-top-30 text-center text-xs-left">
                      <button type="submit" name="btnCalcular" value="Calcular" class="btn btn-sm btn-primary">Calcular</button>
                    </div>
                  </div>

                </div>
            </div>
          </div>

        </div>
      </div>
    </section><br><br>
<?php if( isset($unidades) && count($unidades) > 0 ) { ?>
    <table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th><?php echo $taxas[$this->data['Condominio']['taxa']]; ?></th>
            <th>Rateio: <?php echo $this->data['Condominio']['valor']; ?></th>
            <th>Desconto: <?php echo $this->data['Condominio']['desconto']; ?></th>
            <th>Isenções: <?php echo ($this->data['Condominio']['isencao'] == '1') ? 'Sim' : 'Não'; ?></th>
        </tr>
      </thead>
    </table><br>
    <table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th>Unidade</th>
            <th>Valor da Taxa</th>
            <th>Com Desconto</th>
            <th>Isenções</th>
            <th>Fração Ideal</th>
        </tr>
      </thead>
      
      <tbody>
        <?php
        $reduzao    = 1 - $this->Session->read('UnidadeAtual.Condominio.reducao');
        $rateiro    = $this->Formatacao->getFloat($this->data['Condominio']['valor']);
        $desconto   = $this->Formatacao->getFloat($this->data['Condominio']['desconto']);
        
        foreach ($unidades as $unidade) {
            
            $valorTaxa  = 0;
            
            if($this->data['Condominio']['isencao'] == '1' && $reduzao > 0) {
                $valorTaxa = round( ( $unidade['Unidade']['fracao'] * ($rateiro / $reduzao) ) * ($unidade['Unidade']['percentual_ordinario'] / 100), 2 );
            } else {
                $valorTaxa = round( $unidade['Unidade']['fracao'] * $rateiro, 2 );
            }
            
            $valorDesconto  = round( ( ($desconto > 0) ? ($valorTaxa * (1 - ($desconto / 100)) ) : $valorTaxa ), 2 );
            
            echo '<tr>';
            echo '<td>' . $unidade['Unidade']['unidade'] . '</td>';
            echo '<td>' . $this->Formatacao->moeda($valorTaxa) . '</td>';
            echo '<td>' . $this->Formatacao->moeda($valorDesconto) . '</td>';
            echo '<td>' . ( ($unidade['Unidade']['percentual_ordinario'] < 100 && $this->data['Condominio']['isencao'] == '1') ? ((100 - $unidade['Unidade']['percentual_ordinario']) . '%') : '' ) . '</td>';
            echo '<td>' . $unidade['Unidade']['fracao'] . '</td>';
            echo '</tr>';
            
            $totalTaxa          += $valorTaxa;
            $totalComDesconto   += $valorDesconto;
            $totalFracao        += $unidade['Unidade']['fracao'];
        }
        ?>
      </tbody>

      <thead>
        <tr>
            <th><?php echo count($unidades); ?></th>
            <th><?php echo $this->Formatacao->moeda($totalTaxa); ?></th>
            <th><?php echo $this->Formatacao->moeda($totalComDesconto); ?></th>
            <th>-</th>
            <th><?php echo $totalFracao; ?></th>
        </tr>
      </thead>
      
    </table>
<?php } ?>
