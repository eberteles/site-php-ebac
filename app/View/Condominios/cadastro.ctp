
<section class="section-top-40 section-sm-top-40">
    <div class="row grid-system-row">
      <div class="col-xs-4">
          <center><img src="<?php echo $this->base; ?>/documentos/cond<?php echo sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')); ?>/<?php echo sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')); ?>Logo.jpg" alt="" width="350" /></center>
      </div>
      <div class="col-xs-8">
        <table class="table table-venice-blue table-hover">
          <tbody>
            <tr>
                <td colspan="2"><b>Endereço:</b> <?php echo $this->Session->read('UnidadeAtual.Condominio.endereco'); ?></td>
            </tr>
            <tr>
                <td colspan="2"><b>Complemento:</b> <?php echo $this->Session->read('UnidadeAtual.Proprietario.bairro') . ', ' . $this->Session->read('UnidadeAtual.Proprietario.cidade') . ' - ' . $this->Session->read('UnidadeAtual.Proprietario.uf') . ' / ' . $this->Formatacao->cep($this->Session->read('UnidadeAtual.Proprietario.cep')); ?></td>
            </tr>
            <tr>
                <td><b>Cnpj:</b> <?php echo $this->Formatacao->documento($this->Session->read('UnidadeAtual.Condominio.cnpj')); ?></td>
                <td><b>Telefone: <?php echo $this->Formatacao->telefone($this->Session->read('UnidadeAtual.Condominio.telefone'), false); ?></b></td>
            </tr>
            <tr>
                <td colspan="2"><b>E-mail:</b> <?php echo $this->Session->read('UnidadeAtual.Condominio.email'); ?></td>
            </tr>
            <tr>
                <td colspan="2"><b>Site:</b> <a target="_blank" href="<?php echo $this->Session->read('UnidadeAtual.Condominio.site'); ?>"><?php echo $this->Session->read('UnidadeAtual.Condominio.site'); ?></a></td>
            </tr>
            <tr>
                <td><b>Síndic<?php echo ($this->Session->read('UnidadeAtual.Condominio.Sindico.sexo') == 'M') ? 'o' : 'a'; ?>:</b> <?php echo $this->Session->read('UnidadeAtual.Condominio.Sindico.nome'); ?></td>
                <td><b>Unidade:</b> <?php echo $this->Session->read('UnidadeAtual.Condominio.Sindico.unidade'); ?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-xs-12">
        <?php if(isset($unidades)) { ?>
        <table class="table table-venice-blue table-hover">
          <thead>
            <tr>
                <th>Unidade</th>
                <th>Ocupante</th>
                <th>Proprietário</th>
<!--                <th>Imobiliária</th>-->
            </tr>
          </thead>
          <tbody>
            <?php foreach ($unidades as $unidade) { ?>
            <tr>
                <td><?php echo $unidade['Unidade']['unidade']; ?></td>
                <td><?php echo $unidade['Ocupante']['nome']; ?></td>
                <td><?php echo $unidade['Proprietario']['nome']; ?></td>
<!--                <td>< ?php echo $unidade['Imobiliaria']['nome']; ?></td>-->
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php } ?>
      </div>
    </div>
</section>