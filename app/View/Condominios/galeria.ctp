
        <section>
          <div class="container">
              
            <div class="row">

              <div class="col-xs-12 offset-top-30">
                <!-- Isotope Content-->
                <div data-isotope-layout="masonry" data-isotope-group="gallery" data-photo-swipe-gallery="gallery" class="row isotope isotope-spacing-1">
                    
                  <?php foreach ($arquivos as $arquivo) { ?>
                  <div data-filter="Category 3" class="col-xs-12 col-sm-6 col-md-3 isotope-item">
                    <div class="thumbnail thumbnail-variant-4">
                      <figure><img src="<?php echo $arquivo['link']; ?>" alt="" width="370" height="240"/>
                      </figure><a href="<?php echo $arquivo['link']; ?>" data-photo-swipe-item="" data-size="1200x800" class="thumbnail-original-link"></a>
                      <div class="caption">
                        <div class="caption-text">
                          <h6><?php echo $arquivo['resumo']; ?></h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                    
                </div>
              </div>
                
            </div>
          </div>
        </section>