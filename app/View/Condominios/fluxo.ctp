<?php
$vl_total   = 0;
$lancamentos    = 0;
$totalDespesas  = 0;
$totalReceitas  = 0;
?>

    <section>
      <div class="shell">
        <div class="range">
          <div class="col-sm-12 col-md-12" id="div-form">
            <div class="offset-top-30">
                
                <div class="range">
                  <input type="hidden" id="url" value="<?php echo $this->base; ?>/condominios/fluxo/">
                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="cmbReferencia" class="form-label-outside">Referência</label>
                        <select id="cmbReferencia" >
                            <option value="0">Selecione</option>
                        <?php
                            foreach ($referencias as $anoCombo) {
                                echo '<option ' . (($referencia == $anoCombo[0]['referencia']) ? 'selected' : '' ) . '>' . $anoCombo[0]['referencia'] . '</option>';
                            }
                        ?>
                        </select>
                    </div>
                  </div>

                </div>
            </div>
          </div>

        </div>
      </div>
    </section><br><br>

<table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th>Movimento</th>
            <th>Conta / Histórico</th>
            <th>Documento</th>
            <th>Despesas</th>
            <th>Receitas</th>
        </tr>
      </thead>

      <tbody>
        <?php
        foreach ($taxas as $taxa) {
            echo '<tr>';
            echo '<td>' . $referencia . '</td>';
            echo '<td>Receita Total com Taxas<br>' . $taxa[0]['nu_total'] . ' Taxas Recebidas</td>';
            echo '<td>CRÉD. CONTA</td>';
            echo '<td>&nbsp;</td>';
            echo '<td>' . $this->Formatacao->moeda($taxa[0]['tt_pago']) . '</td>';
            echo '</tr>';
            $vl_total += $taxa['0']['tt_pago'];
            
            $totalReceitas = $taxa['0']['tt_pago'];
            $lancamentos++;
        }
        ?>
        <?php
        foreach ($fluxos as $fluxo) {
            echo '<tr>';
            echo '<td>' . $this->Formatacao->data($fluxo['Fluxo']['data']) . '</td>';
            echo '<td>' . $fluxo['Fluxo']['conta'] . '<br>' . $fluxo['Fluxo']['historico'] . '</td>';
            echo '<td>' . $fluxo['Fluxo']['documento'] . '</td>';
            
            if( (substr($fluxo['Fluxo']['tipo'], 0, 1) == 'D' && substr($fluxo['Fluxo']['tipo'], 1, 1) != 'D') || 
                (substr($fluxo['Fluxo']['tipo'], 0, 1) != 'D' && substr($fluxo['Fluxo']['tipo'], 1, 1) == 'D')) {
                $fluxo['Fluxo']['valor'] = $fluxo['Fluxo']['valor'] * -1;
            }
            
            if( substr($fluxo['Fluxo']['tipo'], 0, 1) == 'D' ) { // Despesa
                echo '<td>' . $this->Formatacao->moeda($fluxo['Fluxo']['valor']) . '</td>';
                echo '<td>&nbsp;</td>';
                $totalDespesas  += $fluxo['Fluxo']['valor'];
            } else { // Receita
                echo '<td>&nbsp;</td>';
                echo '<td>' . $this->Formatacao->moeda($fluxo['Fluxo']['valor']) . '</td>';
                $totalReceitas  += $fluxo['Fluxo']['valor'];
            }
            
            echo '</tr>';
            $vl_total += $taxa['0']['tt_pago'];
            
            $lancamentos++;
        }
        ?>
      </tbody>

      <thead>
        <tr>
            <th colspan="3">Totais:</th>
            <th><?php echo $this->Formatacao->moeda($totalDespesas); ?></th>
            <th><?php echo $this->Formatacao->moeda($totalReceitas); ?></th>
        </tr>
      </thead>
    </table>
    <br>
<table class="table table-venice-blue table-hover">
      <thead>
        <tr>
            <th colspan="4"><center>Saldos Acumulados do Mês <?php echo $referencia; ?></center></th>
        </tr>
      </thead>

      <tbody>
          <tr>
              <td><center><b>Ordinário</b></center></td>
              <td><center><b>Extraordinário</b></center></td>
              <td><center><b>Fundo de Reserva</b></center></td>
              <td><center><b>Total Geral</b></center></td>
          </tr>
          <tr>
              <td><center><?php echo $this->Formatacao->moeda($saldo['Saldo']['ordinario']); ?></center></td>
              <td><center><?php echo $this->Formatacao->moeda($saldo['Saldo']['extraordinario']); ?></center></td>
              <td><center><?php echo $this->Formatacao->moeda($saldo['Saldo']['reserva']); ?></center></td>
              <td><center><?php echo $this->Formatacao->moeda($saldo['Saldo']['ordinario'] + $saldo['Saldo']['extraordinario'] + $saldo['Saldo']['reserva']); ?></center></td>
          </tr>
      </tbody>
</table>