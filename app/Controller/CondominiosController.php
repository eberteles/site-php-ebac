<?php

App::uses('File', 'Utility');
App::uses('Folder', 'Utility');

define('DADOS', ROOT . DS . 'dados' . DS);

class CondominiosController extends AppController {
    
    public $helpers = array('CakePtbr.Formatacao');
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Breadcrumb->add('Meu Condomínio');
   }
   
    public function index() {
        $this->redirect(array('controller' => 'condominios', 'action' => 'cadastro'));
    }
    
    public function cadastro() {
        $this->gravaAcesso(Configure::read('Rotina.CadastroGeral'));
        $this->Breadcrumb->add('Cadastro Geral');
        
        if( $this->Auth->user('role') != '' || $this->Auth->user('username') == $this->Session->read('UnidadeAtual.Condominio.Sindico.documento') ) {
            $this->loadModel('Unidade');
            $this->set('unidades', 
                $this->Unidade->findAllByCondominioId( $this->Session->read('UnidadeAtual.Condominio.id') )
            );
        }
    }
    
    public function comunicados() {
        $this->gravaAcesso(Configure::read('Rotina.Comunicados'));
        $this->Breadcrumb->add('Comunicados');
        
        $nomeDiretorio  = ROOT . DS . 'documentos' . DS . 'cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id'));
        
        $dir    = new Folder( $nomeDiretorio );
        $pdf    = $dir->find(sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . 'Comunicado.*\.pdf');
        $arquivos   = array();
        $indice     = 0;
        foreach ($pdf as $nomePdf) {
            $arquivos[$indice]['pdf']    = $nomePdf;
            list($ano, $mes, $dia) = explode('_', substr($nomePdf, 13, 10));
            $arquivos[$indice]['data']   = $dia . '/' . $mes . '/' . $ano;
            
            $txt    = new File($nomeDiretorio . DS . substr($nomePdf, 0, -3) . 'txt');

            $arquivos[$indice]['resumo'] = utf8_encode($txt->read());
            $arquivos[$indice]['link']   = $this->base . '/documentos/cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . '/' . $nomePdf;
            
            $txt->close();
            
            $indice++;
        }
        if($indice == 0) { $this->Flash->set('Comunicados ainda não disponíveis!'); }
        $this->set(compact('arquivos'));
    }
    
    public function diversos() {
        $this->gravaAcesso(Configure::read('Rotina.Diversos'));
        $this->Breadcrumb->add('Documentos e Arquivos Diversos');
        
        $nomeDiretorio  = ROOT . DS . 'documentos' . DS . 'cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id'));
        
        $dir    = new Folder( $nomeDiretorio );
        $pdf    = $dir->find(sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . 'Diversos.*\.pdf');
        $arquivos   = array();
        $indice     = 0;
        foreach ($pdf as $nomePdf) {
            $arquivos[$indice]['pdf']    = $nomePdf;
            list($ano, $mes, $dia) = explode('_', substr($nomePdf, 11, 10));
            $arquivos[$indice]['data']   = $dia . '/' . $mes . '/' . $ano;
            
            $txt    = new File($nomeDiretorio . DS . substr($nomePdf, 0, -3) . 'txt');

            $arquivos[$indice]['resumo'] = utf8_encode($txt->read());
            $arquivos[$indice]['link']   = $this->base . '/documentos/cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . '/' . $nomePdf;
            
            $txt->close();
            
            $indice++;
        }
        if($indice == 0) { $this->Flash->set('Ainda não disponíveis!'); }
        $this->set(compact('arquivos'));
    }
    
    public function galeria() {
        $this->gravaAcesso(Configure::read('Rotina.FotosDoCondominio'));
        $this->Breadcrumb->add('Fotos e Imagens Diversas');
        
        $nomeDiretorio  = ROOT . DS . 'documentos' . DS . 'cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id'));
        
        $dir    = new Folder( $nomeDiretorio );
        $pdf    = $dir->find(sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . 'Foto.*\.jpg');
        $arquivos   = array();
        $indice     = 0;
        foreach ($pdf as $nomePdf) {
            $arquivos[$indice]['jpg']   = $nomePdf;
            
            $txt    = new File($nomeDiretorio . DS . substr($nomePdf, 0, -3) . 'txt');

            $arquivos[$indice]['resumo'] = utf8_encode($txt->read());
            $arquivos[$indice]['link']   = $this->base . '/documentos/cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . '/' . $nomePdf;
            
            $txt->close();
            
            $indice++;
        }
        if($indice == 0) { $this->Flash->set('Ainda não disponíveis!'); }
        $this->set(compact('arquivos'));
    }    
    
    public function convencao_regimento() {
        $this->gravaAcesso(Configure::read('Rotina.ConvencoesERegimentos'));
        $this->Breadcrumb->add('Convenção e Regimento Interno');
        
        $nomeDiretorio  = ROOT . DS . 'documentos' . DS . 'cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id'));
        
        $dir        = new Folder( $nomeDiretorio );
        $convencao  = $dir->find(sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . 'Convencao.*\.pdf');
        $regimento  = $dir->find(sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . 'Regimento.*\.pdf');
        
        $arquivos   = array();
        $indice     = 0;
        foreach ($convencao as $nomePdf) {
            $arquivos[$indice]['nome']   = 'Escritura de Convenção';
            $arquivos[$indice]['link']   = $this->base . '/documentos/cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . '/' . $nomePdf;
            $indice++;
        }
        foreach ($regimento as $nomePdf) {
            $arquivos[$indice]['nome']   = 'Regimento Interno';
            $arquivos[$indice]['link']   = $this->base . '/documentos/cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . '/' . $nomePdf;
            $indice++;
        }
        if($indice == 0) { $this->Flash->set('Documentos ainda não disponíveis!'); }
        $this->set(compact('arquivos'));
    }
    
    public function edital_ata() {
        $this->gravaAcesso(Configure::read('Rotina.EditaisEAtas'));
        $this->Breadcrumb->add('Editais e Atas');
        
        $nomeDiretorio  = ROOT . DS . 'documentos' . DS . 'cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id'));
        
        $dir      = new Folder( $nomeDiretorio );
        $resumos  = $dir->find(sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . 'Edital.*\.txt');
        $arquivos = array();
        $indice     = 0;
        foreach ($resumos as $nomeTxt) {
            list($ano, $mes, $dia) = explode('_', substr($nomeTxt, 9, 10));
            $arquivos[$indice]['data']   = $dia . '/' . $mes . '/' . $ano;
            $arquivos[$indice]['tipo']   = (substr($nomeTxt, 19, 3) == 'Ago') ? 'Ordinária' : ( (substr($nomeTxt, 19, 3) == 'Age') ? 'Extraordinária' : 'Outras' ) ;
            
            $txt    = new File($nomeDiretorio . DS . $nomeTxt);
            $arquivos[$indice]['resumo'] = utf8_encode($txt->read());
            $txt->close();
            
            $pdfEditar  = $dir->find(substr($nomeTxt, 0, -3) . 'pdf');
            if(count($pdfEditar) > 0) {
                $arquivos[$indice]['edital'] = $this->base . '/documentos/cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . '/' . substr($nomeTxt, 0, -3) . 'pdf';
            }
            
            $pdfAta     = $dir->find(substr($nomeTxt, 0, 3) . 'Ata' . substr($nomeTxt, 9, -3) . 'pdf');
            if(count($pdfAta) > 0) {
                $arquivos[$indice]['ata']    = $this->base . '/documentos/cond' . sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . '/' . substr($nomeTxt, 0, 3) . 'Ata' . substr($nomeTxt, 9, -3) . 'pdf';
            }
                        
            $indice++;
        }
        if($indice == 0) { $this->Flash->set('Documentos ainda não disponíveis!'); }
        $this->set(compact('arquivos'));
    }
    
    public function fluxo($mes = 0, $ano = 0) {
        $this->gravaAcesso(Configure::read('Rotina.ReceitasEDespesas'));
        if($mes == 0 || $ano ==0) {
            $ano = date('Y');
            $mes = date('m');
        }
        
        $this->Breadcrumb->add('Receitas e Despesas');
        
        $this->loadModel('UnidadeTaxa');
        
        $this->set('taxas', 
            $this->UnidadeTaxa->find('all', array(
                    'fields' => array('count(`UnidadeTaxa`.`id`) AS nu_total', 'sum(`UnidadeTaxa`.`pago`) AS tt_pago'),
                    'conditions' => array(
                        'UnidadeTaxa.unidade_id like "'. $this->Session->read('UnidadeAtual.Condominio.id') . '%"',
                        'MONTH(UnidadeTaxa.pagamento)'=>$mes, 'YEAR(UnidadeTaxa.pagamento)'=> $ano)
            ))
        );
        
        $this->loadModel('Fluxo');
        $this->set('referencias', 
            $this->Fluxo->find('all', array(
                    'fields' => array("DATE_FORMAT(Fluxo.data,'%m/%Y') AS referencia"),
                    'conditions' => array('Fluxo.condominio_id'=> $this->Session->read('UnidadeAtual.Condominio.id')),
                    'group' => array('MONTH(Fluxo.data)', 'YEAR(Fluxo.data)'),
                    'order' => array('YEAR(Fluxo.data) DESC', 'MONTH(Fluxo.data) DESC')
            ))
        );
        
        $this->set('referencia', sprintf("%02d", $mes) . '/' . $ano );
        
        $this->set('fluxos', 
            $this->Fluxo->find('all', array(
                    'conditions' => array(
                        'Fluxo.condominio_id'=> $this->Session->read('UnidadeAtual.Condominio.id'),
                        'MONTH(Fluxo.data)'=>$mes, 'YEAR(Fluxo.data)'=> $ano
                    )
            ))
        );
        
        $this->loadModel('Saldo');
        $saldo  = $this->Saldo->findById(sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id')) . sprintf("%02d", $mes) . $ano );
        if(!isset($saldo['Saldo'])) { $saldo['Saldo'] = array('ordinario'=>0, 'extraordinario'=>0, 'reserva'=>0); }
        $this->set(compact('saldo'));
    }
    
    public function acesso($mes = 0, $ano = 0) {
        if($mes == 0 || $ano ==0) {
            $ano = date('Y');
            $mes = date('m');
        }
        
        $this->Breadcrumb->add('Acessos dos Usuários');
        
        $this->loadModel('Acesso');
        
        $this->set('referencias', 
            $this->Acesso->find('all', array(
                    'fields' => array("DATE_FORMAT(Acesso.created,'%m/%Y') AS referencia"),
                    'conditions' => array('Acesso.condominio_id'=> $this->Session->read('UnidadeAtual.Condominio.id')),
                    'group' => array('MONTH(Acesso.created)', 'YEAR(Acesso.created)'),
                    'order' => array('YEAR(Acesso.created) DESC', 'MONTH(Acesso.created) DESC')
            ))
        );
        $this->set('referencia', sprintf("%02d", $mes) . '/' . $ano );
        
        $this->set('acessos', 
            $this->Acesso->find('all', array(
                    'conditions' => array(
                        'Acesso.condominio_id'=> $this->Session->read('UnidadeAtual.Condominio.id'),
                        'MONTH(Acesso.created)'=>$mes, 'YEAR(Acesso.created)'=> $ano
                    )
            ))
        );        
    }
    
    public function taxas_abertas() {
        $this->gravaAcesso(Configure::read('Rotina.TotalTaxasEmAberto'));
        $this->Breadcrumb->add('Taxas em Aberto');
        
        $this->loadModel('Unidade');
        $this->set('taxas', 
            $this->Unidade->find('all', array(
                    'recursive' => -1,
                    'conditions' => array('Unidade.condominio_id'=>$this->Session->read('UnidadeAtual.Condominio.id'), 'Unidade.taxas_abertas > '=>0)
            ))
        );
    }
    
    public function taxas_quitadas($mes = 0, $ano = 0, $lancamento = 0) {
        $this->gravaAcesso(Configure::read('Rotina.TotalTaxasQuitadas'));
        if($mes == 0 || $ano ==0) {
            $ano = date('Y');
            $mes = date('m');
        }
        $this->Breadcrumb->add('Taxas Quitadas por Referência');
        
        $this->loadModel('UnidadeTaxa');
        $this->loadModel('Taxa');
        
        $condTaxas  = array('UnidadeTaxa.unidade_id like "' . $this->Session->read('UnidadeAtual.Condominio.id') . '%"', 
            'MONTH(UnidadeTaxa.pagamento)'=>$mes, 'YEAR(UnidadeTaxa.pagamento)'=> $ano);

        if($lancamento > 0) {
            $condTaxas['UnidadeTaxa.taxa_id'] = $lancamento;
        }
        $this->set('taxas', 
            $this->UnidadeTaxa->find('all', array(
                    'conditions' => $condTaxas,
                    'order' => array('UnidadeTaxa.pagamento ASC', 'UnidadeTaxa.unidade_id ASC')
            ))
        );
        
        $this->set('lancamentos', 
            $this->Taxa->find('list', array(
                    'conditions' => array('Taxa.id in( '
                        . 'SELECT DISTINCT taxa_id from unidade_taxas '
                        . ' where unidade_id like "' . $this->Session->read('UnidadeAtual.Condominio.id')  . '%" '
                        . ' and MONTH(pagamento) = ' . $mes . ' and YEAR(pagamento) = ' . $ano . ' )'),
                    'order' => array('Taxa.nome ASC')
            ))
        );
        
        $this->set('referencias', 
            $this->UnidadeTaxa->find('all', array(
                    'fields' => array("DATE_FORMAT(UnidadeTaxa.pagamento,'%m/%Y') AS referencia"),
                    'conditions' => array('UnidadeTaxa.unidade_id like "' . $this->Session->read('UnidadeAtual.Condominio.id') . '%"'),
                    'group' => array('MONTH(UnidadeTaxa.pagamento)', 'YEAR(UnidadeTaxa.pagamento)'),
                    'order' => array('YEAR(UnidadeTaxa.pagamento) DESC', 'MONTH(UnidadeTaxa.pagamento) DESC')
            ))
        );
        
        $this->set('referencia', sprintf("%02d", $mes) . '/' . $ano );
        
        $this->set(compact('lancamento'));
        
    }
    
    public function rateio() {
        $this->gravaAcesso(Configure::read('Rotina.RateioDespesas'));
        $this->Breadcrumb->add('Rateio de Despesas pela Fração Ideal');
        
        $this->set('taxas', 
            array(0=>'Taxa de Condomínio', 1=>'Taxa Extra', 2=>'Taxa Complementar', 3=>'Taxa de Ocupação', 4=>'Outras Taxas')
        );
        
        if(isset($this->data['Condominio']['valor']) && strlen($this->data['Condominio']['valor']) > 7) {
        
            $this->loadModel('Unidade');
            $this->set('unidades', 
                $this->Unidade->find('all', array(
                        'recursive' => -1,
                        'conditions' => array('Unidade.condominio_id'=>$this->Session->read('UnidadeAtual.Condominio.id'), 'Unidade.fracao > '=>0)
                ))
            );
        }
    }       
}
        
?>