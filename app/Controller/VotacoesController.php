<?php

class VotacoesController extends AppController {
    
    public $components = array('Mpdf.Mpdf');
    
    public $helpers = array('CakePtbr.Formatacao');
    public $uses = array('Votacao');
    
    public $assembleias = array('O'=>'Assembléia Geral Ordinária', 'E'=>'Assembléia Geral Extraordinária');
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Breadcrumb->add('Meu Condomínio');
   }
    
    public function index($id_votacao = 0) {
        $this->gravaAcesso(Configure::read('Rotina.Votacao'));
        $this->Breadcrumb->add('Votação pela Fração Ideal');
        
        $this->set('votacoes', 
            $this->Votacao->find('all', array(
                    'conditions' => array('Votacao.condominio_id'=> $this->Session->read('UnidadeAtual.Condominio.id')),
                    'order' => array('Votacao.data ASC')
            ))
        );
        
        $this->set('assembleias', $this->assembleias);
        
        $this->set(compact('id_votacao'));
        
    }
    
    private function configuraVotacao() {
        $votacao    = array();
        $votacao['Votacao'] = $this->data['Votacao'];
        $votacao['Votacao']['condominio_id'] = $this->Session->read('UnidadeAtual.Condominio.id');
        
        foreach ($this->data['Proposta'] as $id => $proposta) {
            $votacao['Proposta'][$id] = array('descricao'=>$proposta, 'unidades'=>'', 'soma_fracoes'=>0, 'percentual'=>0);
        }            
        
        foreach ($this->data['UnidadeVotacao'] as $unidade => $votou) {
            if($votou == '1') {
                if(isset( $votacao['Proposta'][ $this->data['UnidadeProposta'][$unidade] ] )) {
                    $votacao['Proposta'][ $this->data['UnidadeProposta'][$unidade] ]['unidades'] .= $unidade . ' ';
                    $votacao['Proposta'][ $this->data['UnidadeProposta'][$unidade] ]['soma_fracoes'] += $this->data['Fracao'][$unidade];
                }
            }
        }
        
        return $votacao;
    }
    
    public function add() {
        $this->Breadcrumb->add('Votações', '/votacoes/');
        $this->Breadcrumb->add('Votação pela Fração Ideal');
        
        if($this->request->is('post')) {
            
            if(isset($this->data['UnidadeVotacao']) && isset($this->data['UnidadeProposta'])) { // gravar
                $this->Votacao->create();
                $this->Votacao->saveAssociated($this->configuraVotacao());
                $this->Flash->set('Votação Finalizada. O Relatório será apresentado em uma Nova Janela!');
                return $this->redirect(array('action' => 'index', $this->Votacao->id));
            }
        
            $this->loadModel('Unidade');
            $this->set('unidades', 
                $this->Unidade->find('all', array(
                        'recursive' => -1,
                        'conditions' => array('Unidade.condominio_id'=>$this->Session->read('UnidadeAtual.Condominio.id'), 'Unidade.fracao > '=>0)
                ))
            );
            
            $this->set('assembleias', $this->assembleias);
        }
        else {
            $this->redirect(array('action' => 'index'));
        }
        
    }
    
    public function view($votacao = 0) {
        
        $this->layout   = '';
        
        $this->set('votacao', 
            $this->Votacao->find('first', array(
                    'conditions' => array('Votacao.id'=>$votacao, 'Votacao.condominio_id'=> $this->Session->read('UnidadeAtual.Condominio.id'))
            ))
        );
        
        $this->set('assembleias', $this->assembleias);
        
        // initializing mPDF
        $this->Mpdf->init();

        // setting filename of output pdf file
        $this->Mpdf->setFilename('votacao.pdf');

        // setting output to I, D, F, S
        $this->Mpdf->setOutput('I');
        
    }
    
}

?>