<?php

class UnidadesController extends AppController {
    
    public $components = array('Mpdf.Mpdf');
    
    public $helpers = array('CakePtbr.Formatacao');
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Breadcrumb->add('Minha Unidade');
        $this->Auth->allow('validar');
        $this->Auth->allow('declaracao');
        $this->Auth->allow('inclusao');
        $this->Auth->allow('selecionar');
        $this->Auth->allow('listar');
   }
   
    public function index() {
        $this->redirect(array('controller' => 'unidades', 'action' => 'cadastro'));
    }
    
    public function cadastro() {
        $this->gravaAcesso(Configure::read('Rotina.CadastroUnidade'));
        $this->Breadcrumb->add('Cadastro da Unidade: ' . $this->Session->read('UnidadeAtual.Unidade.unidade'));
    }
    
    public function taxas_abertas() {
        $this->gravaAcesso(Configure::read('Rotina.TaxasAbertoUnidade'));
        $this->Breadcrumb->add('Taxas Abertas');
    }
    
    public function taxas_quitadas($ano = 0, $lancamento = 0) {
        $this->gravaAcesso(Configure::read('Rotina.TaxasQuitadasUnidade'));
        if($ano == 0) {
            $ano = date('Y');
        }
        $this->Breadcrumb->add('Taxas Quitadas');
        
        $this->loadModel('UnidadeTaxa');
        $this->loadModel('Taxa');
        
        $condTaxas  = array('UnidadeTaxa.unidade_id' => $this->Session->read('UnidadeAtual.Unidade.id'), 'YEAR(UnidadeTaxa.pagamento)'=>$ano);
        
        if($lancamento > 0) {
            $condTaxas['UnidadeTaxa.taxa_id'] = $lancamento;
        }
        $this->set('taxas', 
            $this->UnidadeTaxa->find('all', array(
                    'conditions' => $condTaxas,
                    'order' => array('UnidadeTaxa.vencimento ASC')
            ))
        );
        
        $this->set('lancamentos', 
            $this->Taxa->find('list', array(
                    'conditions' => array('Taxa.id in( SELECT DISTINCT taxa_id from unidade_taxas where unidade_id = "' . $this->Session->read('UnidadeAtual.Unidade.id')  . '" and ano = ' . $ano . ' )'),
                    'order' => array('Taxa.nome ASC')
            ))
        );
        
        $this->set('anos', 
            $this->UnidadeTaxa->find('all', array(
                    'fields' => array('DISTINCT UnidadeTaxa.ano'),
                    'conditions' => array('UnidadeTaxa.unidade_id' => $this->Session->read('UnidadeAtual.Unidade.id')),
                    'order' => array('UnidadeTaxa.ano DESC')
            ))
        );
        
        $this->set(compact('ano'));
        $this->set(compact('lancamento'));
        
    }
    
    public function atualizacao() {
        $this->gravaAcesso(Configure::read('Rotina.AtualizacaoCadastral'));
        $this->Breadcrumb->add('Cadastro da Unidade', '/unidades/cadastro');
        $this->Breadcrumb->add('Alteração Cadastral: ' . $this->Session->read('UnidadeAtual.Unidade.unidade'));
    }
    
    public function inclusao() {
        $this->Breadcrumb->add('Inclusão Cadastral');
    }
    
    public function boleto() {
        $this->gravaAcesso(Configure::read('Rotina.BoletoDeCobranca'));
        $this->Breadcrumb->add('Emissão de Guia (Boleto)');
        
        $view   = new View($this);
        $formatacao = $view->loadHelper('CakePtbr.Formatacao');
      
        $this->loadModel('Boleto');
        
        $exibirGuia = true;
        
        $boleto = $this->Boleto->findByUnidadeId( $this->Session->read('UnidadeAtual.Unidade.id') );
        
        if(empty($boleto['Boleto']) || $boleto['Boleto']['vencimento'] == null || $boleto['Boleto']['validade'] == null) {
            $exibirGuia = false;
            $this->Flash->set('A Guia de Cobrança não está disponível.<br>Motivo: Não existem Dados para a Geração. Verifique a próxima data prevista.');
        } else {
            if($boleto['Boleto']['validade'] < date('Y-m-d')) {
                $exibirGuia = false;
                $this->Flash->set('A Guia de Cobrança não está disponível.<br>Motivo: A Guia venceu no dia ' . $formatacao->data($boleto['Boleto']['validade']) . '. Verifique a próxima data prevista.');
            }
        }
        
        if($this->Session->read('UnidadeAtual.Condominio.id') == 998) {
            $exibirGuia = false;
            $this->Flash->set('A Guia de Cobrança não está disponível.<br>Motivo: A Entrega é Feita Diretamente na Caixa de Correios.');
        }
        
        $this->set('boleto', $boleto);
        $this->set(compact('exibirGuia'));
    }
    
    public function guia() {
        
        if($this->Session->read('UnidadeAtual') == null ) {
            $this->redirect($this->Auth->redirect());
        }
        
        $this->layout   = '';
        
        $this->loadModel('Boleto');
        
        $this->set('boleto', 
            $this->Boleto->findByUnidadeId( $this->Session->read('UnidadeAtual.Unidade.id') )
        );
        
        // initializing mPDF
        $this->Mpdf->init();

        // setting filename of output pdf file
        $this->Mpdf->setFilename('guia.pdf');

        // setting output to I, D, F, S
        $this->Mpdf->setOutput('I');

        // you can call any mPDF method via component, for example:
        //$this->Mpdf->SetWatermarkText("Draft");
    }

    public function nada_consta() {
        $this->gravaAcesso(Configure::read('Rotina.NadaConsta'));
        $this->Breadcrumb->add('Declaração de Pagamentos da Unidade');
        
        $exibirNadaConsta   = true;
        if($this->Session->read('UnidadeAtual.Condominio.nada_consta') != 'S') {
            $exibirNadaConsta   = false;
            $this->Flash->set('O Nada-Consta está Bloqueado Temporariamente.<br>Motivo: As Taxas em Aberto estão em fase de cadastramento ou sob Auditoria de verificação.');
        }
        
        $this->set(compact('exibirNadaConsta'));
    }
    
    public function declaracao($chave = null) {
        $this->gravaAcesso(Configure::read('Rotina.NadaConstaGerado'));
        
        $this->loadModel('Chave');
        
        $unidade    = array();
        
        if(isset($chave)) {
            $chaveDb    = $this->Chave->findById($chave);
            if(isset($chaveDb['Chave']['unidade_id'])) {
                $chave      = $chaveDb['Chave']['id'];
                $this->Unidade->recursive = 2;
                $this->Unidade->id = $chaveDb['Chave']['unidade_id'];
                $unidade    = $this->Unidade->read();
                $emissor    = 'Anônimo (Validado pela Internet)';
            }
            else {
                $this->Flash->set('Código de Validação Incorreto.');
                $this->redirect(array('action' => 'validar'));
            }
        }
        else {
            if($this->Session->read('UnidadeAtual') == null ) {
                $this->redirect($this->Auth->redirect());
            }
            $chave      = $this->getChaveNadaConsta();
            $this->Chave->save(array('Chave'=>array('id'=>$chave, 'unidade_id'=>$this->Session->read('UnidadeAtual.Unidade.id'), 'usuario_id'=>$this->Auth->user('id'))));
            $unidade    = $this->Session->read('UnidadeAtual');
            $emissor    = AuthComponent::user('Cliente.nome');
        }
        
        $corSituacao    = "blue";
        $situacao       = "";
        $nuPendencias   = 0;
        
        if(Set::classicExtract($unidade, 'Unidade.taxas_nada_consta') > 0) {
            $situacao    = "06";
            $nuPendencias++;
        }
        if(Set::classicExtract($unidade, 'Unidade.ajuizada') == 'S') {
            $situacao   .= ( ($nuPendencias>0)?' - ':'' ) . "07";
            $nuPendencias++;
        }
        if(Set::classicExtract($unidade, 'Unidade.acordo_administrativo') > 0) {
            $situacao   .= ( ($nuPendencias>0)?' - ':'' ) . "08";
            $nuPendencias++;
        }
        if(Set::classicExtract($unidade, 'Unidade.acordor_judicial') > 0) {
            $situacao   .= ( ($nuPendencias>0)?' - ':'' ) . "09";
            $nuPendencias++;
        }
        
        if($nuPendencias == 0) {
            $situacao    = "Nada-Consta";
        } else {
            $corSituacao = "red";
            $situacao    = (($nuPendencias>1) ? 'Pendências nos Itens: ' : 'Pendência no Item: ') . $situacao;
        }
        
        $this->set(compact('chave'));
        $this->set(compact('corSituacao'));
        $this->set(compact('situacao'));
        $this->set(compact('unidade'));
        $this->set(compact('emissor'));
        
        $this->layout   = 'pdf';                
        $this->Mpdf->init();
        $this->Mpdf->setFilename('declaracao.pdf');
        $this->Mpdf->setOutput('I');
        
    }
    
    private function getChaveNadaConsta(){
        
        $unidade    = sprintf("%03d", $this->Session->read('UnidadeAtual.Condominio.id') ) . sprintf("%06d", $this->Session->read('UnidadeAtual.Unidade.unidade') );
        $a = hash( 'crc32' , time() );
        $b = hash( 'crc32' , sprintf( '%s%s' , md5( $unidade ) , md5( $a ) ) );
        $c = sscanf( sprintf( '%s%s' , $a , $b ) , '%4s%4s%4s%4s' );

        return vsprintf( '%s.%s.%s.%s' , $c );
    }
    
    public function validar() {
        $this->gravaAcesso(Configure::read('Rotina.NadaConstaValidado'));
        $this->Breadcrumb->add('Declaração de Pagamentos (Nada-Consta)');
        
        if(isset($this->data['Chave']['id'])) {
            $this->loadModel('Chave');
            $chave  = $this->Chave->findById($this->data['Chave']['id']);

            if(isset($chave['Chave']['unidade_id'])) {
                $this->redirect(array('action' => 'declaracao', $this->data['Chave']['id']));
            } else {
                $this->Flash->set('Código de Validação Incorreto.');
            }
        }
    }
    
    public function selecionar() {
        $this->Breadcrumb->add('Selecione uma Unidade');
        $this->set('condominios', $this->Session->read('condominios'));
    }
    
    public function listar() {
        $this->layout   = 'ajax';

        if ($this->request->is('post')) {
            
            $conditions = array('Unidade.condominio_id' => $this->request->data['id']);
            
            if($this->Auth->user('role') == '' || $this->Auth->user('role') == null) {
                $this->loadModel('Cliente');
                $clientes = $this->Cliente->find('list', array(
                        'fields' => array('Cliente.id'),
                        'conditions' => array('Cliente.documento'=>$this->Auth->user('username'))
                    ));
                $conditions['OR']   = array( array('Unidade.ocupante_id' => $clientes), array('Unidade.proprietario_id' => $clientes) );
            }
            
            $this->set('unidades', $this->Unidade->find('all', array('conditions' => $conditions)));
        }
    
    }
    
}

?>