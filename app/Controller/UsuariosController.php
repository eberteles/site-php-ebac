<?php

class UsuariosController extends AppController {
    
    var $helpers = array('CakePtbr.Formatacao');
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow('cadastro');
        $this->Auth->allow('esqueci-senha');
        $this->Auth->allow('sair');
   }
    
    public function login() {
        $this->Breadcrumb->add('Área Restrita');
        if(isset($this->request->data['Usuario']['password'])) {$this->request->data['Usuario']['password'] = strtolower($this->request->data['Usuario']['password']);}
        if ($this->Auth->login()) {
            
            if($this->Auth->user('externo') == 'N' && strpos($this->request->ClientIp(), "186.215.65.") === false ) {
                $this->Flash->error('Não é possível acessar desta Máquina.');
                $this->redirect($this->Auth->logout());
            }
            
            $agora  = new DateTime('now');
            $this->Usuario->save(array('Usuario'=>
                array('id'=>$this->Auth->user('id'), 'last_login'=>$agora->format('Y-m-d H:i:s'), 'last_ip'=>$this->request->ClientIp() )
            ));
            
            $this->loadModel('Unidade');
            $this->loadModel('Condominio');
            
            $condominios = array();
            $unidades    = array();
            
            switch ($this->Auth->user('role')) {
                
                case 'ADMINISTRADORA':
                    $condominios    = $this->Condominio->find('list', array(
                            'fields' => array('Condominio.id', 'exibe'),
                            'order' => array('Condominio.id ASC')
                        ));
                    break;
                
                case 'SINDICO':
                    $this->loadModel('Sindico');
                    $sindicos   = $this->Sindico->find('list', array(
                            'fields' => array('Sindico.id'),
                            'conditions' => array('Sindico.documento'=>$this->Auth->user('username'))
                        ));
                    $condominios    = $this->Condominio->find('list', array(
                            'fields' => array('Condominio.id', 'exibe'),
                            'conditions' => array('Condominio.sindico_id' => $sindicos),
                            'order' => array('Condominio.id ASC')
                        ));
                    
                    if(count($condominios) == 1) {
                        $unidades   = $this->Unidade->find('all', array(
                                'conditions' => array(
                                    'Unidade.condominio_id' => key($condominios),
                                ),
                                'recursive' => 2,
                                'order' => array('Unidade.condominio_id DESC')
                            ));
                        $this->Session->write('unidades', $unidades);
                    }
                    break;
                    
                case 'ENCARREGADO':
                case 'CONSELHO':
                case 'SUBSINDICO':
                    $condominios    = $this->Condominio->find('list', array(
                            'fields' => array('Condominio.id', 'exibe'),
                            'conditions' => array('Condominio.id' => $this->Auth->user('condominio_id'))
                        ));
                    $unidades   = $this->Unidade->find('all', array(
                            'conditions' => array(
                                'Unidade.condominio_id' => key($condominios),
                            ),
                            'recursive' => 2,
                            'order' => array('Unidade.condominio_id DESC')
                        ));
                    $this->Session->write('unidades', $unidades);
                    break;
                
                default:
                    $this->loadModel('Cliente');
                    $clientes = $this->Cliente->find('list', array(
                            'fields' => array('Cliente.id'),
                            'conditions' => array('Cliente.documento'=>$this->Auth->user('username'))
                        ));
                    
                    $condominios    = $this->Condominio->find('list', array(
                            'joins' => array(
                                array(
                                    'table' => 'unidades',
                                    'alias' => 'Unidade',
                                    'type' => 'INNER',
                                    'conditions' => array(
                                        'Condominio.id = Unidade.condominio_id'
                                    )
                                )
                            ),
                            'fields' => array('Condominio.id', 'exibe'),
                            'conditions' => array('OR' => array(
                                array('Unidade.ocupante_id' => $clientes),
                                array('Unidade.proprietario_id' => $clientes)
                            ))
                        ));
                                        
                    if(count($condominios) == 1) {
                        $unidades   = $this->Unidade->find('all', array(
                                'conditions' => array(
                                    'Unidade.condominio_id' => key($condominios),
                                    'OR' => array(
                                    array('Unidade.ocupante_id' => $clientes),
                                    array('Unidade.proprietario_id' => $clientes)
                                )),
                                'recursive' => 2,
                                'order' => array('Unidade.condominio_id DESC')
                            ));
                        
                        if(count($unidades) == 1) {
                            $this->setUnidadeAtual($unidades[0]);
                        } else {
                            $this->Session->write('unidades', $unidades);
                        }
                    }
                    break;
            }

            $this->Session->write('condominios', $condominios);
            
            if(count($condominios) == 0) {
                $this->Flash->error('Não existe nehuma Unidade vinculada a este usuário. Favor realizar a Inclusão Cadastral.');
                $this->redirect($this->Auth->logout());
            }
            
            if( (count($condominios) == 1 && count($unidades) > 1) || count($condominios) > 1) { // Escolher Unidade
                $this->redirect(array('controller' => 'unidades', 'action' => 'selecionar'));
            } else {
                $this->redirect($this->Auth->redirect());
            }
            
        } else {
            if($this->request->is('post')) {
                $this->Flash->error('Usuário ou senha inválido, tente novamente.');
            }
        }
    }
    
    private function setUnidadeAtual($unidade) {
        if($this->Auth->user('role') != '') {
            $this->Session->write('tipoUsuario', ucfirst( strtolower($this->Auth->user('role')) ) );
        } else {
            if($unidade['Proprietario']['documento'] == $this->Auth->user('username')) {
                $this->Session->write('tipoUsuario', 'Proprietario' );
            } else {
                $this->Session->write('tipoUsuario', 'Ocupante' );
            }
        }
        $this->Session->write('UnidadeAtual', $unidade);
    }

    public function sair() {
        $this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }
    
    public function trocar($unidade_id) {
        $this->loadModel('Unidade');
        $unidade    = $this->Unidade->find('first', array(
                'conditions' => array(
                    'Unidade.id'=>$unidade_id,
                )
            ));
        if(isset($unidade['Unidade']['id'])) {
            $this->setUnidadeAtual($unidade);
            $this->redirect($this->Auth->redirect());
        } else {
            $this->sair();
        }
    }
    
    public function cadastro() {
        $this->Breadcrumb->add('Área Restrita');
        $this->Breadcrumb->add('Solicitar Acesso');
        
        $this->Usuario->validator()->add('email', array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar um e-mail do cadastro.'
            )
        ));
        
        $this->Usuario->validator()->add('nascimento', array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a data.'
            )
        ));
        
        $this->Usuario->validator()->add('confirmacao', array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a confirmação da senha.'
            )
        ));
    }
    
}

?>