<?php

App::uses('CakeEmail', 'Network/Email');

class EnviaController extends AppController {
    
    var $destinatario = "eberteles@hotmail.com"; // "sistemas@ebac.com.br"
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow('cadastro');
        $this->Auth->allow('inclusao_cadastral');
        $this->Auth->allow('acesso');
   }
    
    public function fale_com_ebac() {
        $this->autoRender = false;
        
        $assunto    = "Fale com a Ebac:" . $this->data['cmbMotivo'];
        
        $corpo  = $assunto . "<br>" . 
            "==================================================" . "<br>" . 
            "Nome do Condomínio: " . $this->data['txtNomeCon']  . "<br>" .
            "Unidade: " . $this->data['txtUnidade']  . "<br>" . 
            "Remetente: " . $this->data['txtNome']  . "<br>" . 
            "Telefones: " . $this->data['txtFone1']  . "  " . $this->data['txtFone2']  . "<br>" . 
            "Email: " . $this->data['txtEmail']  . "<br>" . 
            "Texto: " . $this->data['txtTexto']  . "<br>" . 
            "==================================================";
        
        $tipoAssunto = substr($this->data['cmbMotivo'], 0, 2);
        if($tipoAssunto == "01" || $tipoAssunto == "02" || $tipoAssunto == "03" || $tipoAssunto == "04") {
            $this->destinatario = "gerentecomercial@ebac.com.br";
        }
        $this->enviar('EBAC', $assunto, $corpo);
    }
    
    public function inclusao_cadastral() {
        $this->autoRender = false;
        
        $assunto    = "EbacCadastro" . date("d/m/Y H:i:s");
        
        $corpo  = $assunto . "<br>" . 
            "==================================================" . "<br>" .
            "CEbac00-Tipo de Cadastro     :I" . "<br>" .
            "CEbac01-Codigo               :000" . "<br>" .
            "CEbac02-Condominio           :" . $this->data['txtNomeCon'] . "<br>" .
            "CEbac03-Unidade              :" . $this->data['txtUnidade'] . "<br>" .
            "CEbac04-Ocupante             :" . $this->data['txtOcupante'] . "<br>" .
            "CEbac05-CPF/CNPJ Ocupante    :" . $this->mostraIFederal( $this->data['txtCpfCnpjOcup'] ) . "<br>" .
            "CEbac06-Data Nascimento Ocup :" . $this->data['txtNascOcup'] . "<br>" .
            "CEbac07-Fone Ocupante        :" . $this->data['txtFoneOcup'] . "<br>" .
            "CEbac08-Email do Ocupante    :" . $this->data['txtEmailOcup'] . "<br>" .
            "CEbac09-Proprietario         :" . $this->data['txtProprietario'] . "<br>" .
            "CEbac10-CPF/CNPJ Proprietario:" . $this->mostraIFederal( $this->data['txtCpfCnpjProp']) . "<br>" .
            "CEbac11-Data Nascimento Prop :" . $this->data['txtNascProp'] . "<br>" .
            "CEbac12-Fone1 Proprietario   :" . $this->data['txtFoneProp1'] . "<br>" .
            "CEbac13-Fone2 Proprietario   :" . $this->data['txtFoneProp2'] . "<br>" .
            "CEbac14-Email do Proprietario:" . $this->data['txtEmailProp'] . "<br>" .
//            "CEbac21-Codigo Imobiliaria   :" . "<br>" . 
//            "CEbac22-Imobiliaria          :" . $this->data['txtImobiliaria'] . "<br>" . 
//            "CEbac23-Fone Imobiliaria     :" . $this->data['txtFoneImob'] . "<br>" . 
            "CEbac25-Email do Remetente   :" . $this->data['txtEmail'] . "<br>" .
            "==================================================";
        
        $this->enviar('EBAC', $assunto, $corpo);
    }
    
    public function alteracao_cadastral() {
        $this->autoRender = false;
        
        $assunto    = "EbacCadastro" . date("d/m/Y H:i:s");
        
        $corpo  = $assunto . "<br>" . 
            "==================================================" . "<br>" . 
            "CEbac00-Tipo de Cadastro     :P" . "<br>" . 
            "CEbac01-Codigo               :" . $this->Session->read('UnidadeAtual.Condominio.id') . "<br>" . 
            "CEbac02-Condominio           :" . $this->Session->read('UnidadeAtual.Condominio.nome') . "<br>" . 
            "CEbac03-Unidade              :" . $this->Session->read('UnidadeAtual.Unidade.unidade') . "<br>" . 
            "CEbac04-Ocupante             :" . $this->data['txtOcupante'] . "<br>" . 
            "CEbac05-CPF/CNPJ Ocupante    :" . $this->mostraIFederal( $this->data['txtCpfCnpjOcup']) . "<br>" . 
            "CEbac06-Data Nascimento Ocup :" . $this->data['txtNascOcup'] . "<br>" . 
            "CEbac07-Fone Ocupante        :" . $this->data['txtFoneOcup'] . "<br>" . 
            "CEbac08-CPF/CNPJ Ocupante    :" . $this->mostraIFederal( $this->data['txtCpfCnpjOcup']) . "<br>" . 
            "CEbac09-Email do Ocupante    :" . $this->data['txtEmailOcup'] . "<br>" . 
            "CEbac10-Proprietario         :" . $this->data['txtProprietario'] . "<br>" . 
            "CEbac11-CPF/CNPJ Proprietario:" . $this->mostraIFederal( $this->data['txtCpfCnpjProp']) . "<br>" . 
            "CEbac12-Data Nascimento Prop :" . $this->data['txtNascProp'] . "<br>" . 
            "CEbac13-Fone1 Proprietario   :" . $this->data['txtFoneProp1'] . "<br>" . 
            "CEbac14-Fone2 Proprietario   :" . $this->data['txtFoneProp2'] . "<br>" . 
            "CEbac15-Email do Proprietario:" . $this->data['txtEmailProp'] . "<br>" . 
            "CEbac16-Endereco             :" . $this->data['txtEndeProp'] . "<br>" . 
            "CEbac17-Bairro               :" . $this->data['txtBairProp'] . "<br>" . 
            "CEbac18-Cidade               :" . $this->data['txtCidaProp'] . "<br>" . 
            "CEbac19-UF                   :" . $this->data['txtUfProp'] . "<br>" . 
            "CEbac20-CEP                  :" . $this->data['txtCepProp1'] . "<br>" . 
            "CEbac21-Codigo Imobiliaria   :" . "<br>" . 
            "CEbac22-Imobiliaria          :" . $this->data['txtImobiliaria'] . "<br>" . 
            "CEbac23-Fone Imobiliaria     :" . $this->data['txtFoneImob'] . "<br>" . 
            "CEbac24-Endereco Cobranca    :" . $this->data['cmbTipoCobr'] . "<br>" . 
            "CEbac25-Email do Remetente   :" . $this->data['txtEmail'] . "<br>" . 
            "==================================================";
        
        $this->enviar('EBAC', $assunto, $corpo);
    }
    
    public function acesso() {
        $this->autoRender = false;
        
        $this->loadModel('Cliente');
        
        $cliente    = $this->Cliente->find('first', array(
                'conditions' => array(
                    'Cliente.documento'=>$this->data['Usuario']['username'],
                    'Cliente.nascimento'=>$this->dateDb($this->data['Usuario']['nascimento']),
                    'Cliente.email'=>$this->data['Usuario']['email']
                    )
            ));
        
        // CADASTRO ENCONTRADO
        if(isset($cliente['Cliente']['id']) && $cliente['Cliente']['id'] > 0) {
            
            $assunto    = "[EBAC Administração de Condomínios] - Acesso à área restrita autorizado!";
            
            $corpo      = "<img border='0' src='https://www.ebac.com.br/imagens/EbacLogo.jpg' width='111' height='93'><br><br><b>" . $cliente['Cliente']['nome'] . ",</b><br><br>" 
                        . "&nbsp;&nbsp;&nbsp;&nbsp;Para acessar os seviços que a EBAC disponibiliza, basta utilizar a sua senha abaixo: <br><br>" 
                        . "&nbsp;&nbsp;&nbsp;&nbsp;<b>Senha</b>: " . $this->data['Usuario']['password'] . "<br><br>"
                        . "Mensagem automática, favor não responder.<br><br>EBAC - Trasparência para o condômino, eficiência para o síndico";
            
            $this->loadModel('Usuario');
            $usuario    = $this->Usuario->find('first', array(
                    'conditions' => array(
                        'Usuario.username'=>$this->data['Usuario']['username'],
                        'Usuario.role'=>null
                        )
                ));
            
            // ATUALIZAR SENHA
            if(isset($usuario['Usuario']['id']) && $usuario['Usuario']['id'] > 0) {
                $this->Usuario->save(array('Usuario'=>
                    array('id'=>$usuario['Usuario']['id'], 'password'=>$this->data['Usuario']['password'], 'nome'=>$cliente['Cliente']['nome'] )
                ));                
            }
            else { // CRIAR USUÁRIO E SENHA
                $this->Usuario->create();
                $this->Usuario->save(array('Usuario'=>
                    array('username'=>$this->data['Usuario']['username'], 'password'=>$this->data['Usuario']['password'], 'nome'=>$cliente['Cliente']['nome'] )
                ));
            }
            
            $this->enviar('EBAC', $assunto, $corpo, array($this->data['Usuario']['email'] => $cliente['Cliente']['nome']), "MF007"); //AUTORIZADO

        } else {
            return "MF256";  //NÃO AUTORIZADO
        }
    }
    
    private function enviar($nomeDestinatario, $assunto, $mensagem, $destinatario = null, $retorno = "MF000") {
        $email = new CakeEmail();
        if($destinatario == null) {
            $email->to(array($this->destinatario => $nomeDestinatario));
        } else {
            $email->to($destinatario);
        }
        $email->subject($assunto);
        $email->send($mensagem);
        echo $retorno;
    }
    
    private function mostraIFederal($inscricao) {
        
        if(strlen($inscricao) == 11) { //CPF
            return $this->mask($inscricao, '###.###.###-##');
        }
        else if(strlen($inscricao) == 14) { //CNPF
            return $this->mask($inscricao, '##.###.###/####-##');
        } else {
            return "";
        }
        
    }
    
    private function mask($val, $mask) {
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++) {
            if($mask[$i] == '#') {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
    
    private function dateDb($date) {
        if($date != '' && strpos($date, '/') > 0) {
            list($dia, $mes, $ano) = explode('/', $date);
            if (strlen($ano) == 2) {
                    if ($ano > 50) $ano += 1900;
                    else $ano += 2000;
            }
            return "$ano-$mes-$dia";
        } else {
            return '';
        }
    }
}

?>