<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
    public $components = array(
        'CakeBreadcrumbs.Breadcrumb',
        'Flash',
        'Session',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'unidades', 'action' => 'cadastro'),
            'logoutRedirect' => array('controller' => 'usuarios', 'action' => 'login'),
            //'unauthorizedRedirect' => array('controller' => 'usuarios', 'action' => 'externo'),
            'authorize' => array('Controller')
        )
    );
    
    public $helpers = array('Html', 'Form', 'Session', 'CakeBreadcrumbs.Breadcrumb', 'CakePtbr.Formatacao');
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        $this->Auth->authenticate = array(AuthComponent::ALL => array('userModel' => 'Usuario'), 'Form' => array('recursive' => 0));
        
        $this->Breadcrumb->add('Início', '/');
        
    }
    
    public function isAuthorized($user) {
        //$this->Flash->error('Não é possível acessar desta Máquina.');
        //$this->redirect(array('controller' => 'usuarios', 'action' => 'sair'));
        return true;
    }
    
    public function gravaAcesso($rotina, $condominio = null, $unidade = null) {
        $acesso = array();
        
        $acesso['Acesso']['rotina_id']      = $rotina;
        $acesso['Acesso']['ip']             = $this->request->ClientIp();
        $acesso['Acesso']['condominio_id']  = ($condominio == null) ? $this->Session->read('UnidadeAtual.Condominio.id') : $condominio;
        $acesso['Acesso']['unidade_id']     = ($unidade == null) ? $this->Session->read('UnidadeAtual.Unidade.id') : $unidade;
        $acesso['Acesso']['usuario']        = ($this->Session->read('tipoUsuario') == null) ? 'Internet' : $this->Session->read('tipoUsuario');
        $acesso['Acesso']['responsavel']    = ($this->Auth->user('nome') == null) ? 'Anônimo' : $this->Auth->user('nome');

        $this->loadModel('Acesso');
        $this->Acesso->save($acesso);
    }
    
}
